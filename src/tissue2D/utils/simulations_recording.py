# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:

import os
import datetime as dt
import numpy as np
from cellcomplex.property_topomesh.utils.pandas_tools import topomesh_to_dataframe
from cellcomplex.property_topomesh.io import save_ply_property_topomesh
from tissue2D.visualization.tissue_visualization import draw_tissue

VISU_PARAM_DEFAULT = {'save_figure': True,
                      'visu_each_step': True,
                      'show_stress_orient': True,
                      'show_recent_div': False,
                      'back_ground_color': 'black',
                      'edge_color': 'r',
                      'tensor_color': 'g',
                      'field_of_view': 'automatic',
                      'cell_property': 'area',
                      'cell_colormap': 'viridis',
                      'cell_alpha': .0,
                      'edge_property': None,
                      'edge_property_2': None,
                      'edge_thickness': 2}


def save_data(simulation, time, mesh_name=None, folder_path=None):
    """Save logs, meshes and snapshots of simulations.

    Parameters
    ----------
    simulation : Tissue2DModel()
        The simulation we want to save.
    time : Float
        The current time step we want to save.
    mesh_name : Str
        Optional (default : None). A specific name to the mesh.
    folder_path : Str
        Optional (default : None). A specfici location where
        to save the simulation.

    Returns
    -------
    None.

    """
    now = dt.datetime.now()

    if not mesh_name:
        mesh_name = simulation._mesh_name

    step_number = int((simulation._t_max
                       - simulation._t_min)/float(simulation._dt)) + 1

    # -- Setting the parameters for the various plots
    if not hasattr(simulation, '_visu_parameters'):
        simulation._visu_parameters = VISU_PARAM_DEFAULT

    if not hasattr(simulation, '_recording_path'):
        simulation._recording_path = '/Users/oali/Documents/Work/' + \
                                     'Research/Devlp/tissue2D/data/results/'

    # -- Initiation of the saving procedure.
    if time == simulation._t_min:

        # --*-- Creating the recording folders.
        folder_name = now.strftime("%y%m%d") + '_'\
                    + now.strftime("%H%M%S") + '_'\
                    + mesh_name + '/'

        simulation._recording_path += folder_name
        snapshots_folder_path = simulation._recording_path + 'snapshots/'
        meshes_folder_path = simulation._recording_path + 'meshes/'
        dataframes_folder_path = simulation._recording_path + 'dataframes/'

        for fold_name in [snapshots_folder_path,
                          dataframes_folder_path,
                          meshes_folder_path]:
            if not os.path.exists(fold_name):
                os.makedirs(fold_name)

        # --*-- Writing the log file
        write_log_file(simulation,
                       mesh_name=mesh_name,
                       save_folder_path=simulation._recording_path)

    # -- Saving snapshots at each time step.
    if simulation._visu_parameters['visu_each_step']:
        snapshots_folder_path = simulation._recording_path + 'snapshots/'
        snapshots_recording_name = 'snapshot_' + mesh_name + '_step' \
                                               + str(int(time) + 1).zfill(3) \
                                               + '_over_' \
                                               + str(step_number).zfill(3) \
                                               + '.png'

        draw_tissue(simulation._mesh,
                    visu_param=simulation._visu_parameters,
                    show_stress_orientation=simulation._visu_parameters['show_stress_orient'],
                    show_recent_divisions=simulation._visu_parameters['show_recent_div'],
                    save_figure=simulation._visu_parameters['save_figure'],
                    saving_path=snapshots_folder_path+snapshots_recording_name)

    # -- Saving the property_topomesh at each simulation step.
    if simulation._save_simulation:
        meshes_folder_path = simulation._recording_path + 'meshes/'
        recording_name = mesh_name + '_step' + str(int(time) + 1).zfill(3) \
                                   + '_over_' + str(step_number).zfill(3) \
                                   + '.ply'

        computable_topomesh_properties = {0: ['barycenter'],
                                          1: ['length', 'borders', 'vertices'],
                                          2: ['barycenter',
                                              'vertices',
                                              'oriented_borders',
                                              'oriented_vertices',
                                              'area']}

        properties_2_save = {degree: [] for degree in range(4)}
        for degree in range(4):
            properties = simulation._mesh.wisp_properties(degree)
            for mesh_property in properties.keys():
                if mesh_property not in computable_topomesh_properties[degree]:
                    properties_2_save[degree].append(mesh_property)

        save_ply_property_topomesh(simulation._mesh,
                                   meshes_folder_path+recording_name,
                                   properties_2_save)

        # -- Saving also the cell, edge and vertices properties as dataframes
        dataframes_folder_path = simulation._recording_path + 'dataframes/'

        for degree, name in enumerate(['vrtcs', 'walls', 'cells']):
            print("Saving "+name+" properties as a dataframe")
            dataframe = topomesh_to_dataframe(simulation._mesh,
                                              degree=degree)
            dtfr_name = mesh_name + '_step' + str(int(time) + 1).zfill(3) \
                                  + '_over_' + str(step_number).zfill(3) \
                                  + "_" + name + '.csv'

            dataframe.to_csv(dataframes_folder_path+dtfr_name, index=False)


def write_log_file(simulation, save_folder_path, mesh_name=None):
    """Generate a .txt file containing the parameters of the simulation.

    Parameters
    ----------
    simulation : Tissue2D()
        The simulation we want to describe.
    save_folder_path : str
        The path to the recording folder for the simulation.
    mesh_name : str
        Optional (Default : None). A possible new name for the mesh. By
        default the one given in the simulation is used.

    Returns
    -------
    None.

    """

    today = dt.date.today()

    # -- Mesh used within the simulations
    mesh = simulation._mesh
    if not mesh_name:
        mesh_name = simulation._mesh_name

    # -- Gathering the parameters of the various used models.
    if simulation._simulation_type == 'master':
        used_modules = {'mechanics':            simulation._mechanics,
                        'growth':               simulation._growth,
                        'cell division':        simulation._division,
                        'cell wall stiffening': simulation._stiffening}

    elif simulation._simulation_type == 'mechanics':
        used_modules = {'mechanics': True}

    elif simulation._simulation_type == 'growth':
        used_modules = {'growth': True}

    elif simulation._simulation_type == 'division':
        used_modules = {'division': True}

    elif simulation._simulation_type == 'stiffening':
        used_modules = {'cell wall stiffening': True}

    module_parameters = {}

    for module_name, module in used_modules.items():
        if (module_name == 'mechanics' and module):
            module_parameters[module_name] = {'pressure':
                                              simulation._mechanical_parameters['pressure'],
                                              'cwll_stiffness_ratio':
                                              simulation._mechanical_parameters['cwll_stiffness_ratio'],
                                              'initial expansion':
                                              simulation._factor}

        if (module_name == 'growth' and module):
            module_parameters[module_name] = {'growth rate':
                                              simulation._growth_rate,
                                              'growth threshold':
                                              simulation._growth_threshold,
                                              'growth type':
                                              simulation._growth_type}

            if simulation._growth_type == 'hill':
                hill_properties = {'hill function exponent':
                                   simulation._growth_hill_exponent,
                                   'hill function amplitude':
                                   simulation._growth_hill_amplitude}

                module_parameters[module_name].update(hill_properties)

        if (module_name == 'cell division' and module):
            module_parameters[module_name] = {'cell division threshold':
                                              simulation._division_threshold,
                                              'cell division orientation':
                                              simulation._orientation_method,
                                              'New walls loading coefficient':
                                              simulation._loading_coeff}

        if (module_name == 'cell wall stiffening' and module):
            module_parameters[module_name] = {'stiffening rate':
                                              simulation._stiffening_rate,
                                              'stiffening method':
                                              simulation._stiffening_method,
                                              'edge type to stiffen':
                                              simulation._edge_type_2_stiffen,
                                              'hill function threshold':
                                              simulation._stiffening_hill_threshold,
                                              'hill function exponent':
                                              simulation._stiffening_hill_exponent,
                                              'hill function amplitude':
                                              simulation._stiffening_hill_amplitude,
                                              'cell wall memory considered':
                                              simulation._cwll_memory}

    # -- Creating the log file
    simu_info_txt_file = os.path.join(save_folder_path, 'simulation_log.txt')

    with open(simu_info_txt_file, 'w') as text_file:
        text_file.write('Log file from tissue2D simulations. \n')
        text_file.write('Generated the '+str(today)+'\n')
        text_file.write('\n')
        text_file.write('---------------------------- \n')
        text_file.write('Author: Olivier Ali \n')
        text_file.write('Mail..: olivier.ali@inria.fr \n')
        text_file.write('---------------------------- \n')
        text_file.write('\n')

        # -- Mesh properties
        text_file.write('=============================\n')
        text_file.write('   TISSUE  CHARACTERISTICS   \n')
        text_file.write('\n')
        text_file.write(' * mesh name: ' + mesh_name + '\n')
        text_file.write('   - number of cells.....: '
                        + str(mesh.nb_wisps(2)) + '\n')
        text_file.write('   - number of edges.....: '
                        + str(mesh.nb_wisps(1)) + '\n')
        text_file.write('   - number of vertices..: '
                        + str(mesh.nb_wisps(0)) + '\n')
        text_file.write('\n')
        text_file.write(' * mesh dimensions:\n')
        mesh_width  = (np.max(mesh.wisp_property('barycenter',
                                                 0).values()[:, 0])
                       - np.min(mesh.wisp_property('barycenter',
                                                   0).values()[:, 0]))
        mesh_height = (np.max(mesh.wisp_property('barycenter',
                                                 0).values()[:, 1])
                       - np.min(mesh.wisp_property('barycenter',
                                                   0).values()[:, 1]))
        text_file.write('   - width...............: '
                        + str(mesh_width)[: 5] + '\n')
        text_file.write('   - height..............: '
                        + str(mesh_height)[: 5] + '\n')
        text_file.write('   - initial aspect ratio: '
                        + str(mesh_width/mesh_height)[: 5] + '\n')
        text_file.write('\n')
        text_file.write(' * mesh properties:' + '\n')
        for degree, wisp_name in enumerate(['vertices', 'edges', 'cells']):
            text_file.write('   - on '+wisp_name+': '+'\n')
            for prop_name in mesh.wisp_properties(degree).keys():
                text_file.write('     > '+prop_name+'\n')
        text_file.write('\n')

        # -- Simulation properties
        text_file.write('=============================\n')
        text_file.write(' SIMULATION  CHARACTERISTICS \n')
        text_file.write(' * initial time..........: '
                        + str(simulation._t_min) + ' \n')
        text_file.write(' * final time............: '
                        + str(simulation._t_max) + ' \n')
        text_file.write(' * time increment........: '
                        + str(simulation._dt) + ' \n')
        text_file.write(' * Used modules: \n')
        for module_name, module in used_modules.items():
            if module:
                text_file.write('    - '+module_name+' \n')

        text_file.write('\n')
        text_file.write(' * Parameters of the used modules: \n')
        for module_name, module in used_modules.items():
            if module:
                text_file.write('    - '+module_name+': \n')
                for param_name, param_values in module_parameters[module_name].items():
                    text_file.write('       > ' + param_name
                                    + ': ' + str(param_values) + '\n')


# simulations_recording.py ends here.
