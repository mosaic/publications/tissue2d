# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:
import numpy as np
import scipy.ndimage as nd
from imageio import imread
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.creation import dual_topomesh, quad_topomesh
from cellcomplex.property_topomesh.topological_operations import topomesh_remove_boundary_vertex
from cellcomplex.property_topomesh.example_topomesh import circle_voronoi_topomesh, hexagon_topomesh, hexagonal_grid_topomesh

try:
    from artificial_tissue.cellular_mesh_2D.image2cvt import image_voronoi_tissue_2d
except ImportError:
    print('No library artificial tissue, method generate_tissue_from_image cannot be used.')


from tissue2D.utils.tools import (orienting_vertices,
                                  define_edge_type,
                                  compute_cellwall_stiffness,
                                  compute_cell_center,
                                  deform_mesh,
                                  compute_epidermis)


def generate_some_tissues(add_properties=False):
    """Generate example meshes to simulate 2D tissues.

    Parameters
    ----------
    add_properties: bool
        Optional (default: False). If True some useful properties are
        automatically computed and added to the meshes.

    Returns
    -------
    Tuple of five PropertyTopomesh:
        - A square single cell.
        - An hexagonal single cell.
        - A grid composed of hexagonal cells.
        - A disk tilled with random cells.
        - An ellipse tilled with random cells.

    """

    # -- One square cell
    square_cell_mesh = square_topomesh(side_length=7)

    # -- One hexagonal cell
    hexa_cell_mesh = dual_topomesh(hexagon_topomesh(side_length=7))

    # -- Multicellular tissues
    # --*-- several hexagonal cells
    tissue_size = 2
    hexa_grid_mesh = hexagonal_grid_topomesh(size=tissue_size, voxelsize=.5)

    # --*-- 2D circular Voronoi
    circle_mesh = circle_voronoi_topomesh(tissue_size)

    # --*-- 2D ellipsoidal Voronoi
    ellipse_mesh = deform_mesh(circle_mesh, angle=0, aspect_ratio=2)
    # remove_outermost_vertices(ellipse_mesh)

    # -- Adding useful properties
    if add_properties:
        for mesh in [square_cell_mesh,
                     hexa_cell_mesh,
                     hexa_grid_mesh,
                     circle_mesh,
                     ellipse_mesh]:
            mesh.update_wisp_property('resting_length',
                                      1,
                                      np.ones(mesh.nb_wisps(1)))
            formatting_mesh(mesh)

    return square_cell_mesh, hexa_cell_mesh, hexa_grid_mesh, circle_mesh, ellipse_mesh


# -- Formatting mesh properly for the specificities of the tissue2D package

def formatting_mesh(mesh, cwll_stiffness_ratio=3, custom_properties=None):
    """Adds useful and custom properties to 2D meshes.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue srtucture to consider.
    cwll_stiffness_ratio : Float
        Value of the stiffness ratio between outermost and inner edges.
    custom_properties : Dict
        - keys : tuple(str, int) = property name, wisp degree
        - values : dict(int, float): id of the wisps, value of the property.

    Returns
    -------
    None.
    """

    # -- Adding barycenters
    compute_topomesh_property(mesh, 'barycenter', 0)

    # --*-- putting the barycenter of the tissue at the origin
    vtx_pos = mesh.wisp_property('barycenter', 0)
    tis_ctr = np.mean(vtx_pos.values(), axis=0)
    new_vtx_pos = {vid: pos - tis_ctr for vid, pos in vtx_pos.items()}

    mesh.update_wisp_property('barycenter', 0, new_vtx_pos)

    # -- Orienting barycenters
    orienting_vertices(mesh)

    # -- Orienting vertices and edges around cells
    for prop_name in ['oriented_vertices', 'oriented_borders']:
        if prop_name not in mesh.wisp_properties(2).keys():
            compute_topomesh_property(mesh, prop_name, 2)

    compute_topomesh_property(mesh, 'vertices', 1)
    compute_topomesh_property(mesh, 'vertices', 2)

    compute_topomesh_property(mesh, 'barycenter', 2)

    # -- Computing edge lengths
    if 'length' not in mesh.wisp_properties(1).keys():
        compute_topomesh_property(mesh, 'length', 1)

    # -- Computing cell areas
    if 'area' not in mesh.wisp_properties(2).keys():
        compute_topomesh_property(mesh, 'area', 2)

    # # -- Defining the epidermis layer
    # epidermis = compute_epidermis(mesh)
    # mesh.update_wisp_property('epidermis', 2, epidermis)

    # -- Defining the outermost edges
    define_edge_type(mesh)

    # -- Adding custom properties
    # --*-- Copmuting cell wall stiffness:
    if not custom_properties:
        custom_properties = {('pressure', 2):
                             {cid: .01 for cid in mesh.wisps(2)},
                             ('resting_length', 1):
                             {eid: len for eid, len
                              in mesh.wisp_property('length', 1).items()},
                             ('cwll_stiffness', 1):
                             compute_cellwall_stiffness(mesh, stiffness_ratio_out_in=cwll_stiffness_ratio)}

    for (property_name, degree), property in custom_properties.items():
        mesh.update_wisp_property(property_name, degree, property)


def formatting_routine(mesh):
    """Formats properly the mesh to run mechanical simulations.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue srtucture to consider.

    Returns
    -------
    None.

    Note
    ----
    This function has been specifically designed for the tutorial.
    """
    for prop_name in ['oriented_vertices', 'oriented_borders']:
        if prop_name not in mesh.wisp_properties(2).keys():
            compute_topomesh_property(mesh, prop_name, 2)

        compute_topomesh_property(mesh, 'vertices', 1)
        compute_topomesh_property(mesh, 'vertices', 2)
        formatting_mesh(mesh)
        compute_topomesh_property(mesh, 'length', 1)
        compute_topomesh_property(mesh, 'vertices', 1)
        compute_topomesh_property(mesh, 'area', 2)
        compute_topomesh_property(mesh, 'barycenter', 2)



# -- A method to generate a simple square_topomesh
def square_topomesh(side_length = 1):
    position = side_length/2.
    points = {}
    points[0] = [-position, -position, 0]
    points[1] = [position, -position, 0]
    points[2] = [position,  position, 0]
    points[3] = [-position,  position, 0]

    squares = [[0, 1, 2, 3]]

    return quad_topomesh(squares, points, faces_as_cells=True)


def remove_outermost_vertices(mesh):
    """Remove the edges and vertices on the outermost walls.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we want to simplify.

    Returns
    -------
    None.

    """
    vtx_ids = list(mesh.wisps(0))

    boundary_edges = dict(zip(mesh.wisps(1), [mesh.nb_regions(1, e) < 2
                                              for e in mesh.wisps(1)]))
    mesh.update_wisp_property('boundary', 1, boundary_edges)

    compute_topomesh_property(mesh, 'edges', 0)

    boundary_pts= dict(zip(mesh.wisps(0), map(np.all, mesh.wisp_property('boundary', 1).values(mesh.wisp_property('edges', 0).values(vtx_ids)))))
    mesh.update_wisp_property('boundary', 0, boundary_pts)

    boundary_vertices = np.array(vtx_ids)[mesh.wisp_property('boundary',
                                                             0).values(vtx_ids)]
    for v in boundary_vertices:
        topomesh_remove_boundary_vertex(mesh, v)

    compute_topomesh_property(mesh, 'edges', 0)
    compute_topomesh_property(mesh, 'vertices', 2)
    compute_topomesh_property(mesh, 'oriented_vertices', 2)


# -- A method to generate a 2D topomesh from a black and white profile
def generate_tissue_from_image(image_path, cell_nbr=50,
                               sampling_precision=100,
                               cell_regularity_coef=50):
    """Generates a property topomesh from a mask image.

    Parameters
    ----------
    image_path : str
        Path to the image to use as a mask.
    cell_nbr : int
        Optional (default : 50). Number of cell to put in the tissue.
    sampling_precision : int
        Optional (default : 100). Number of points around
        the border of the tissue.
    cell_regularity_coef : int
        Optional (default : 50). A variable that tunes the regularity of cells
        within the tissue. The bigger, the more regular the cells will be. In
        the asymptotic case, cells are hexagons.

    Returns
    -------
    mesh : PropertyTopomesh
        The corresponding mesh

    """

    img = imread(image_path)
    img = 255*nd.binary_opening(img > 128).astype(img.dtype)

    mesh = image_voronoi_tissue_2d(img,
                                   n_cells=cell_nbr,
                                   contour_size=sampling_precision,
                                   mode='cvt',
                                   lloyd_coef=1.,
                                   n_iterations=cell_regularity_coef)[0]

    mesh.update_wisp_property('cell', 2,
                              dict(zip(mesh.wisps(2),
                                       [list(mesh.regions(2, f))[0]
                                        for f in mesh.wisps(2)])))

    return mesh


# mesh_generation.py ends here.
