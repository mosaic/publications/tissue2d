# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:

import copy
import random
import logging
import numpy as np
import numpy.linalg as lng
from collections.abc import Iterable

from cellcomplex.property_topomesh.analysis import compute_topomesh_property


def orienting_vertices(mesh):
    """Orienting vertices properly around each cell of a property_topomesh.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to compute pressure forces on.

    Returns
    -------
    None
    """

    if 'barycenter' not in mesh.wisp_properties(2).keys():
        compute_topomesh_property(mesh, 'barycenter', 2)

    cell_centers = mesh.wisp_property('barycenter', 2)

    compute_topomesh_property(mesh, 'oriented_vertices', 2)

    new_oriented_vids = {}
    for cid, oriented_vids in mesh.wisp_property('oriented_vertices',
                                                 2).items():

        assert(set(oriented_vids) == set(list(mesh.borders(2, cid, 2))))

        center = cell_centers[cid]

        vect1 = mesh.wisp_property('barycenter', 0)[oriented_vids[0]] - center
        vect2 = mesh.wisp_property('barycenter', 0)[oriented_vids[1]] - center

        assert(np.cross(vect1, vect2)[2] != 0)

        if np.cross(vect1, vect2)[2] < 0:
            new_oriented_vids[cid] = oriented_vids
        else:
            new_oriented_vids[cid] = list(reversed(oriented_vids))

    mesh.update_wisp_property('oriented_vertices', 2, new_oriented_vids)


def deform_mesh(mesh, angle=0., aspect_ratio=1.):
    """Shrink or expand a property topomesh along a given direction.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we want to shrink or expand.
    angle : Float
        Optional (default : 0). the direction (starting from
        the abscissae axis) of the shrinkage. The angle are assumed in
        degrees.
    aspect_ratio : Float
        Optional (default : 1.). The deformation coefficient.
        If > 1: The mesh is expanded along the considered axis.
        if < 1: The mesh is shrinked along the considered axis.

    Returns
    -------
    deformed_mesh: PropertyTopomesh
        The deformed version of our property topomesh.

    """
    deformed_mesh = copy.deepcopy(mesh)
    Id = np.identity(3)

    # -- We convert the angle in radians.
    angle *= np.pi/180

    exp_unit_dir_vect = np.array([np.cos(angle), np.sin(angle), 0.])
    deformation_matrix = Id + (aspect_ratio - 1) * np.outer(exp_unit_dir_vect,
                                                            exp_unit_dir_vect)

    new_positions = {vid: np.dot(deformation_matrix, pos) for vid, pos
                     in mesh.wisp_property('barycenter', 0).items()}

    deformed_mesh.update_wisp_property('barycenter', 0, new_positions)

    return deformed_mesh


def compute_cell_center(mesh, cid):
    """Compute the cell centers not as a barycenter.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue to work on.
    cid : Int.
        The index of the cell we are interested in.

    Returns
    -------
    cell_center : NDarray(Float)
        the 3D position vector (third coord.=0)

    """
    compute_topomesh_property(mesh, 'barycenter', 1)
    compute_topomesh_property(mesh, 'length', 1)

    eids = list(mesh.borders(2, cid))

    edge_centers = mesh.wisp_property('barycenter', 1).values(eids)
    edge_lengths = mesh.wisp_property('length', 1).values(eids)
    cell_perimtr = sum(edge_lengths)

    cell_center = np.sum(list(map(lambda x, y: x * y / cell_perimtr,
                                  edge_centers, edge_lengths)), axis=0)

    return cell_center


def compute_position_array_from_arraydict(position_dict, key_values=None):
    """Concatenates a dict of position into an array.

    Parameters
    ----------
    position_dict : ArrayDict
        - keys : Int, vertex indexes within a topomesh.
        - values : Ndarray(Float), 3D vectors.

    key_values : List(Int)
        Optional, a subset of keys to consider.

    Returns
    -------
    vertex_positions : NDarray(float)
        a 2*nD vector (n=number of vertices) containing
        the vertices (x, y) coordinates.
    vid_2_idx : Dict(Int)
        - keys : indices (vid) of the vertex positions
                 within the PropertyTopomesh.
        - values : index (idx) of the  vertex positions
                   within the reshaped array.
    idx_2_vid : Dict(Int)
        - keys : index (idx) of the  vertex pos within the reshaped array.
        - values : indices (vid) of the vertex pos within the PropertyTopomesh.
    """

    if not key_values:
        key_values = list(position_dict.keys())

    vtx_pos_3nD = np.concatenate(position_dict.values(key_values))

    # -- Removing the 3rd component (always egal to zero)
    vtx_pos_2nD = np.array([element for id, element in enumerate(vtx_pos_3nD)
                            if (id + 1) % 3 != 0])
    assert len(vtx_pos_2nD) % 2 == 0

    vertex_number = int(len(vtx_pos_2nD) / 2)
    assert vertex_number == len(key_values)

    # -- Relating vids to index within the array
    vid_2_idx = {vid: idx for idx, vid in enumerate(key_values)}
    idx_2_vid = {}
    for idx, vid in enumerate(key_values):
        idx_2_vid[2*idx] = vid
        idx_2_vid[2*idx+1] = vid

    # -- Controling the order of the vector components
    vtx_pos_reshaped = vtx_pos_2nD.reshape(vertex_number, 2)

    for vid in key_values:
        assert vtx_pos_reshaped[vid_2_idx[vid]][0] == position_dict[vid][0]
        assert vtx_pos_reshaped[vid_2_idx[vid]][1] == position_dict[vid][1]

    return vtx_pos_2nD, vid_2_idx, idx_2_vid


def compute_position_array_from_mesh(mesh):
    """reshape the positions of all vertices of a propTopomesh into an array.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to compute stres field forces on.

    Returns
    -------
    vtx_pos_2nD : NDarray(float)
        a 2*nD vector (n=number of vertices) containing
        the vertices (x, y) coordinates.

    vid_2_idx : Dict(Int)
        - keys : indices (vid) of the vertex positions
                 within the PropertyTopomesh.
        - values : index (idx) of the  vertex positions
                   within the reshaped array.

    idx_2_vid : Dict(Int)
        - keys : index (idx) of the  vertex pos within the reshaped array.
        - values : indices (vid) of the vertex pos within the PropertyTopomesh.

    """
    vtx_pos_dict = mesh.wisp_property('barycenter', 0)
    vtx_ids = list(mesh.wisps(0))

    vtx_pos_2nD, vid_2_idx, idx_2_vid = compute_position_array_from_arraydict(vtx_pos_dict, key_values=vtx_ids)

    return vtx_pos_2nD, vid_2_idx, idx_2_vid


def compute_intersection_line_segment(unit_vector, initial_position, segment):
    """Computes the position of the intersection between a segment and a line.

    Parameters
    ----------
    unit_vector : ndarray(float)
        The unit vector (3D) giving the direction of the line
    initial_position : ndarray(float)
        A point belonging to the line.
    segment : tuple(ndarray(float))
        The extremities of the segment given as 3D vectors

    Returns
    -------
    ndarray(float)
        The vector position of the intersection if it exists,
        the null vector otherwise.

    Notes
    -----
        This algorithm works for lines and segments in the same plane
        but parametrized as 3D vectors with null third components.
    """

    # -- segment extreminties
    pos0 = segment[0]
    pos1 = segment[1]

    # -- we are looking for the interection point expressed as
    #    x = pos0 + relative_position * (pos1 - pos0)
    if np.cross(unit_vector, (pos1 - pos0))[2] == 0:
        relative_position = 1.5
    else:
        relative_position = - np.cross(unit_vector,
                                       (pos0 - initial_position))[2] \
                            / np.cross(unit_vector, (pos1 - pos0))[2]

    if 0 <= relative_position <= 1:
        return pos0 + relative_position * (pos1 - pos0)
    else:
        return np.array([0., 0., 0.])


def compute_intersection_line_cell(mesh, cid, unit_vector, initial_pos):
    """Computes the 2 intersections between a line and a closed cell.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue the considered cell is part of.
    cid : Int
        id of the considered cell within the mesh.
    unit_vector : ndarray(float)
        The unit vector (3D) giving the direction of the line
    initial_pos : ndarray(float)
        A point belonging to the line.

    Returns
    -------
    Dict(ndarray(float))
        - keys  : edge ids (where the intersection points are).
        - values: 3D vectors (ndarray(float)) giving the positions
                  of the intersection point.
    """

    intersection_positions = {}
    for eid in mesh.borders(2, cid):
        extremities_ids = list(mesh.borders(1, eid))
        segment = mesh.wisp_property('barycenter', 0).values(extremities_ids)
        putative_intersection = compute_intersection_line_segment(unit_vector,
                                                                  initial_pos,
                                                                  segment)

        # -- Useful to check is the intersection is not already counted
        #    (it can happen if the intersection is a vertex)
        existing_intersection = np.ones(3)
        if len(list(intersection_positions.values())) > 0:
            existing_intersection = list(intersection_positions.values())[0]

        if (lng.norm(putative_intersection) > 0
            and not (putative_intersection == existing_intersection).all()
                and len(list(intersection_positions.values())) < 2):
            intersection_positions[eid] = putative_intersection

    return intersection_positions


def compute_intersection_multiple_lines_cell(mesh, cid, unit_vector, density):
    """Computes the intersections between parallel lines and a cell.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue the considered cell is part of.
    cid : Int
        id of the considered cell within the mesh.
    unit_vector : ndarray(float)
        The unit vector (3D) giving the direction of the line
    density : float
        The number of lines we want to consider across a unit circle.

    Returns
    -------
    List(Dict(ndarray(float)))
        a list of dictionaries (one dict = one line) with:
                - keys  : edge ids (where the intersection points are).
                - values: 3D vectors (ndarray(float)) giving the positions
                          of the intersection point.
    """

    vtx_ids = list(mesh.borders(2, cid, 2))
    vtx_pos = mesh.wisp_property('barycenter', 0).values(vtx_ids)
    cell_center = mesh.wisp_property('barycenter', 2)[cid]

    # -- Computing the number of lines to consider
    cell_radius = max(map(lambda x: lng.norm(x), vtx_pos - cell_center))
    lines_nmber = int(cell_radius * density) + 1

    # -- Computing the initial positions of each line
    orthogonal_direction = np.array([unit_vector[1], -unit_vector[0], 0.])
    spacing = 2. / density
    unit_displacement = spacing * orthogonal_direction

    initial_positions = [cell_center + (l - (lines_nmber - 1) / 2)
                         * unit_displacement for l in range(lines_nmber)]

    # -- Computing the intersections between the lines and the cell periphery
    intersect_pos = list((map(lambda init_pos:
                              compute_intersection_line_cell(mesh,
                                                             cid,
                                                             unit_vector,
                                                             init_pos),
                              initial_positions)))

    # -- Removing the empty elements
    intersection_positions = [intersection for intersection
                              in intersect_pos if len(intersection) > 0]

    return intersection_positions


def compute_current_edge_lengths(mesh):
    """Computes the edges length knowing the positions of the vertices.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue to consider.

    Returns
    -------
    current_edges_length : Dict(float)
        - keys : Int. index of the edges
        - values : float. The edge current length.

    """

    current_edges_length = {}

    for eid in mesh.wisps(1):
        vtx_ids = list(mesh.borders(1, eid))
        vtx_pos0, vtx_pos1 = mesh.wisp_property('barycenter',
                                                0).values(vtx_ids)

        current_edges_length[eid] = lng.norm(vtx_pos1 - vtx_pos0)

    return current_edges_length


def inflate_structure(mesh, factor=.1):
    """displace vertices in the outward direction by a given amount.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to compute pressure forces on.
    factor : Float
        The percentage of displacement to apply.
        Should be within [0, 1] (default : 0.1)

    Returns
    -------
    new_vtx_pos : Dict
        - keys : Int, the vertex indices
        - values : NDarray(Float), the new vertex position vectors.
    """

    vtx_pos = mesh.wisp_property('barycenter', 0)
    vtx_ctr = np.mean(vtx_pos.values(), axis=0)

    dsplcmt = {vid: factor * (vpos - vtx_ctr) for vid, vpos in vtx_pos.items()}

    new_vtx_pos = {vid: vpos + dsplcmt[vid] for vid, vpos in vtx_pos.items()}

    return new_vtx_pos


def define_edge_type(mesh):
    """Defines the kind of edge (inner or outer) of a given edge within a mesh.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to compute edge types on.

    Returns
    -------
    None.

    """
    edge_type = {}

    # -- First version
    for eid in mesh.wisps(1):
        if mesh.nb_regions(1, eid) == 1:
            edge_type[eid] = 'outer'
        else:
            edge_type[eid] = 'inner'

    # # -- Second version
    # for eid in mesh.wisps(1):
    #     cids = list(mesh.regions(1, eid))
    #     layer = mesh.wisp_property('epidermis', 2).values(cids)
    #
    #     if any(layer == 1):
    #         edge_type[eid] = 'outer'
    #     else:
    #         edge_type[eid] = 'inner'

    mesh.update_wisp_property('edge_type', 1, edge_type)


def compute_epidermis(mesh):
    """Computes if a cell is in the epidermis or not.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to compute edge types on.

    Returns
    -------
    epidermis = dict()
        - keys : cell ids
        - values : 1 if cell in epidermis 0 otherwise.

    """
    epidermis = {cid: 0 for cid in mesh.wisps(2)}
    for cid in mesh.wisps(2):
        for eid in mesh.borders(2, cid):
            if mesh.nb_regions(1, eid) == 1:
                epidermis[cid] = 1
                break

    return epidermis


def compute_direction(mesh, method='random', cids_2_consider='all'):
    """Computes the division directions for a list of cells.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we are working on.
    method : str
        Optional (default : 'random'). The method to compute the direction.
        Should be chosen among:
            - 'random'
            - 'along_max_stress'
            - 'errera'
            - 'radial'
    cids_2_consider : str or int or list(int)
        Optional (default : 'all'). The list of cells to consider.

    Returns
    -------
    direction : dict
        - keys : int. The ids of the considered cells.
        - values : ndarray(float) the 3D unit vector giving the direction. The
                   3rd component is null.

    """

    # -- Listing the cells where the orientation needs to be computed
    if cids_2_consider == 'all':
        cids = list(mesh.wisps(2))
    elif (cids_2_consider != 'all'
          and not isinstance(cids_2_consider, Iterable)):
        cids = [cids_2_consider]
    else:
        cids = cids_2_consider

    # -- Defining the various division methods
    if method == 'along_max_stress':
        if 'stress_tensor' in mesh.wisp_properties(2).keys():
            cll_strss = mesh.wisp_property('stress_tensor', 2).values(cids)

            # -- Building up the direction dict
            direction = {cid: np.concatenate([max_eigvect(stress),
                                              np.zeros(1)])
                         for cid, stress in zip(cids, cll_strss)}

        else:
            logging.warnings('WARNING: no stress recorder within the tissue.')

    elif method == 'random':
        random_angles = [random.uniform(0, np.pi) for i in cids]

        direction = {cid: np.array([np.cos(angle), np.sin(angle), 0.])
                     for cid, angle in zip(cids, random_angles)}
    elif method == 'errera':
        cll_shape = {cid: compute_shape_descriptor(mesh, cid) for cid in cids}

        direction = {cid: np.concatenate([min_eigvect(shape), np.zeros(1)])
                     for cid, shape in cll_shape.items()}
    elif method == 'radial':
        direction = {cid: compute_apico_basal_direction(mesh, cid)
                     for cid in cids}
    elif method == 'ortho_growth':
        grth_tsr = mesh.wisp_property('growth_tensor', 2)

        direction = {cid: min_eigvect(grth_tsr[cid]) for cid in cids}

    return direction


def compute_apico_basal_direction(mesh, cid):
    """Computes the apico-basal direction of an epidermal cell within a tissue.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we work on.
    cid: Int
        The index of the cell we work on.

    Returns
    -------
    drection = np.array(Float)
        2D unit vector (written in 3D as usual).

    """
    vtx_pos = mesh.wisp_property('barycenter', 0)
    epidermis = compute_epidermis(mesh)
    assert epidermis[cid] == 1

    edge_eids = list(mesh.borders(2, cid))
    neighb_cids = [ncid for ncid in mesh.border_neighbors(2, cid)
                   if epidermis[ncid] == 1]
    assert len(neighb_cids) == 2

    edge_vectors = []
    for ncid in neighb_cids:
        # -- Detect the anticlinal edges (between epidermal neighbor cells)
        neighb_borders_eid = list(mesh.borders(2, ncid))
        common_edges = list(set(edge_eids) & set(neighb_borders_eid))
        assert len(common_edges) == 1

        # -- Compute the corresponding directional vector
        edge_end_pids = list(mesh.borders(1, common_edges[0]))
        ptx_0, ptx_1 = vtx_pos.values(edge_end_pids)
        edge_vect = ptx_1 - ptx_0
        edge_vect /= np.linalg.norm(edge_vect)
        edge_vectors.append(edge_vect)

    # -- Checking that the edge vectors are aligned in the same direction.
    if np.dot(edge_vectors[0], edge_vectors[1]) < 0:
        edge_vectors[1] *= -1

    direction = np.mean(edge_vectors, axis=0)

    return direction


def compute_cellwall_stiffness(mesh, stiffness_ratio_out_in=1):
    """Defines stiffness depending on the position of the walls (in/out).

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to compute cell wall stiffness on.
    stiffness_ratio_out_in : Float
        Optional (default : 3). Relative stiffness value of outer walls
        compared to inner ones.

    Returns
    -------
    Dict(Float)
        - keys : Int. Index of the edges.
        - values : Float. stiffness values.

    Note:
    -----
    Should be run after the 'define_edge_type' methods. Or, at least, the
    property 'edge_type' has to be defined on the wisps(1)
    of the considered mesh.
    """
    cwll_stiffness = {eid: 1 for eid in mesh.wisps(1)}

    for eid, edge_type in mesh.wisp_property('edge_type', 1).items():
        if edge_type == 'outer':
            cwll_stiffness[eid] *= stiffness_ratio_out_in

    return cwll_stiffness


def compute_cellwall_lengths(mesh):
    """Computes the lengths of edges, given the positions of their borders.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we are working on.

    Returns
    -------
    cwll_length : Dict(NDarray(Float))
        - keys : Int. edge ids
        - values : Float. the length of the considered edge.

    """

    cwll_length = {}
    for eid in mesh.wisps(1):
        vids = list(mesh.borders(1, eid))
        end_pos = mesh.wisp_property('barycenter', 0).values(vids)
        cwll_length[eid] = lng.norm(end_pos[1] - end_pos[0])

    return cwll_length


def compute_shape_descriptor(mesh, cids_2_consider='all', current=True):
    """Computes the 2nd moment of area of a point cloud.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we work on.
    cids_2_consider : list(Int)
        Optional (default : 'all'). The list of cell ids to consider in the
        mesh. If =='all' -> the mesh in its globality is considered.
    current : Bool
        Opitional (default : True). If True, the used positions are the
        current ones. Else, the used positions should be recorded as the
        property 'previous_barycenter'.

    Returns
    -------
    shape_descriptor : NDarray(Float)
        A 2D 2nd order symmetric tensor describing the shape as an ellipse.

    """

    if cids_2_consider == 'all':
        outer_eids = [eid for eid, type
                      in mesh.wisp_property('edge_type', 1).items()
                      if type == 'outer']
        outer_vids = list(set(np.concatenate(list(map(lambda x:
                                                      list(mesh.borders(1, x)),
                                                      outer_eids)))))

    elif isinstance(cids_2_consider, (int, np.int32, np.int64)):
        outer_vids = list(mesh.borders(2, cids_2_consider, 2))
    else:
        print("Not implemented yet! Sorry :)")

    if current:
        vtx_pos = list(mesh.wisp_property('barycenter',
                                          0).values(outer_vids))
    else:
        try:
            vtx_pos = list(mesh.wisp_property('previous_barycenter',
                                              0).values(outer_vids))
        except KeyError:
            print('WARNING: the property previous_barycenter \
                   is not defined on vertices.')

    vtx_ctr = np.mean(vtx_pos, axis=0)
    vtx_pos = list(map(lambda x: x - vtx_ctr, vtx_pos))

    shape_descriptor = np.mean(list(map(lambda x: np.outer(x[:2], x[:2]),
                                        vtx_pos)), axis=0)

    return shape_descriptor


def hill_function(var, thr, expo, amp=1.):
    """Implementation of the Hill function.

    Parameters
    ----------
    var : float
        The input variable. The signal to amplify.
    thr : float
        The threshold value above which the variable is amplified.
    expo : int
        The steepness of the non-linearity. Note: Should be positive.
    amp : float
        Optional (default : 1). Max value of the amplified signal

    Returns
    -------
    res : float
        The output, the amplified signal.

    """
    var /= thr

    res = 1/(1 + var**(-expo))

    res *= amp

    return res


# ######################### #
# -- Tensor-related methods
# ######################### #
def compute_tensor_from_vectors(mesh, force_field, cid_2_consider='all'):
    """Computes the stress within a cell, given the forces on its vertices.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to compute stres field forces on.
    force_field : Dict(ndarray(float))
        - keys  : vertex ids
        - values: 3D vector representing the force at the considered
                  vertex.
    cid_2_consider : str or int
        Optional (default : "all"). If a cell id is given the forces are
        computed only for this cell.

    Returns
    -------
    Dict(ndarray(float))
        - keys  : cell ids
        - values: a 2x2 symmetric matrix representing
                  the tensor field within each cell.

    """

    orienting_vertices(mesh)

    if cid_2_consider == 'all':
        cll_ids = list(mesh.wisps(2))
        vtx_ids = list(mesh.wisps(0))

    elif cid_2_consider in mesh.wisps(2):
        cll_ids = [cid_2_consider]
        vtx_ids = list(mesh.borders(2, cid_2_consider, 2))

        if set(vtx_ids) != set(force_field.keys()):
            logging.warning('vtx_ids: ', vtx_ids)
            logging.warning('Force_field.keys(): ', force_field.keys())

        assert set(vtx_ids) == set(force_field.keys())

    vtx_pos = mesh.wisp_property('barycenter', 0)

    if not mesh.has_wisp_property('area', 2, is_computed=True):
        compute_topomesh_property(mesh, 'area', 2)

    cell_surf = mesh.wisp_property('area', 2)

    stress_field = {}
    if not mesh.has_wisp_property('oriented_vertices', 2, is_computed=True):
        orienting_vertices(mesh)

    for cid, vtx_ids in zip(cll_ids, mesh.wisp_property('oriented_vertices',
                                                        2).values(cll_ids)):
        next_vtx_ids = np.concatenate((vtx_ids[1:], vtx_ids[:1]), axis=0)

        stress_xx, stress_yy, stress_xy = 0., 0., 0.
        for vid, nvid in zip(vtx_ids, next_vtx_ids):
            stress_xx += (((force_field[vid][0] * vtx_pos[vid][0]
                            + force_field[nvid][0] * vtx_pos[nvid][0])
                           + (force_field[vid][0] * vtx_pos[nvid][0]
                              + force_field[nvid][0] * vtx_pos[vid][0]) / 2)
                          / (3 * cell_surf[cid]))

            stress_yy += (((force_field[vid][1] * vtx_pos[vid][1]
                            + force_field[nvid][1] * vtx_pos[nvid][1])
                           + (force_field[vid][1] * vtx_pos[nvid][1]
                              + force_field[nvid][1] * vtx_pos[vid][1]) / 2)
                          / (3 * cell_surf[cid]))

            stress_xy += (((force_field[vid][0] * vtx_pos[vid][1]
                            + force_field[vid][1] * vtx_pos[vid][0]
                            + force_field[nvid][0] * vtx_pos[nvid][1]
                            + force_field[nvid][1] * vtx_pos[nvid][0])
                           + (force_field[vid][0] * vtx_pos[nvid][1]
                              + force_field[vid][1] * vtx_pos[nvid][0]
                              + force_field[nvid][0] * vtx_pos[vid][1]
                              + force_field[nvid][1] * vtx_pos[vid][0]) / 2)
                          / (6 * cell_surf[cid]))

        stress_field[cid] = np.array([[stress_xx, stress_xy],
                                      [stress_xy, stress_yy]])

    if len(cll_ids) == 1:
        return stress_field[cid]
    else:
        return stress_field


def compute_tension_from_stress(mesh, eid_2_consider):
    """computes the (positive only) tension experienced by an loaded edge.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The structure we are working on. Should feature a 'stress_tensor'
        property on its wisps(2).
    eid_2_consider : int
        The index of the wisps(1) we are considering.

    Returns
    -------
    edge_tension : float
        The value of the tension we are looking for.

    """
    # -- Compute the edge vector and its normal
    bvids = list(mesh.borders(1, eid_2_consider))
    ends = mesh.wisp_property('barycenter', 0).values(bvids)

    edge_vect = ends[1] - ends[0]
    edge_nrml = np.cross(np.array([0, 0, 1]),  edge_vect / lng.norm(edge_vect))

    # -- Compute the tension (scalar) experienced by the considered edge
    ncids = list(mesh.regions(1, eid_2_consider))
    stress = np.sum(mesh.wisp_property('stress_tensor',
                                       2).values(ncids), axis=0)

    assert np.sign(lng.det(stress)) == 1

    edge_tension = np.abs(np.tensordot(stress,
                                       np.outer(edge_nrml,
                                                edge_vect)[:2, :2], axes=2))

    return edge_tension


def compute_strain_from_stress(mesh, cid_2_consider='all'):
    """Compute the 2nd order strain tensor from the stress tensor for cells.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we are working on. It should feature a 'stress_tensor'
        property on its wisps(2) and a 'cwll_stiffness' on its wisps(1).
    cid_2_consider : list(Int)
        Optional (default : 'all'). The list of cell ids we want to compute
        strain on.

    Returns
    -------
    strain : dict
        - keys : Int. The ids of the considered cells
        - values : ndarray(float). The corresponding 2x2 symmetric matrix.

    """
    if cid_2_consider == 'all':
        cids = list(mesh.wisps(2))

    stresses = mesh.wisp_property('stress_tensor', 2).values(cids)

    strain = {}
    for cid, stress in zip(cids, stresses):
        # -- Compute the compliance tensor
        edge_projectors = []
        for eid in list(mesh.borders(2, cid)):
            cwl_stf = mesh.wisp_property('cwll_stiffness', 1)[eid]

            vids = list(mesh.borders(1, eid))
            end_pos = mesh.wisp_property('barycenter', 0).values(vids)
            edg_vec = end_pos[1] - end_pos[0]
            edg_vec /= lng.norm(edg_vec)
            edg_vec = edg_vec[:2]

            edge_projectors.append(lng.norm(edg_vec) / cwl_stf
                                   * np.tensordot(np.outer(edg_vec, edg_vec),
                                                  np.outer(edg_vec, edg_vec),
                                                  axes=0))

        compliance = np.sum(edge_projectors, axis=0)

        # -- Compute the strain tensor
        strain[cid] = np.tensordot(stress, compliance,  axes=2)

    return strain


# --*-- Definition of a projector
def proj(theta):
    """Computes a projector along the direction given by theta.

    Parameters
    ----------
    T : NDarray(Float)
        the tensor in question.

    Returns
    -------
    NDarray(Float)
        the expected projector.

    """
    unit_vector = np.array([np.cos(theta),
                            np.sin(theta)])

    return np.outer(unit_vector, unit_vector)


# --*-- Tensor decomposition
def sym(T):
    """Returns the symmetric part of a matrix.

    Parameters
    ----------
    T : NDarray(Float)
        the tensor in question.

    Returns
    -------
    NDarray(Float)
        the expected symmetric part.

    """
    assert T.shape[0] == T.shape[1]
    T = T.astype(float)
    return (T + T.transpose()) / 2.


def asy(T):
    """Returns the asymmetric part of a matrix.

    Parameters
    ----------
    T : NDarray(Float)
        the tensor in question.

    Returns
    -------
    NDarray(Float)
        the expected asymmetric part.

    """

    assert T.shape[0] == T.shape[1]
    T = T.astype(float)
    return T - sym(T)


def iso(T):
    """Returns the isotropic component of a 2D matrix.

    Parameters
    ----------
    T : NDarray(Float)
        the tensor in question.

    Returns
    -------
    NDarray(Float)
        the expected isotropic part.

    """
    assert T.shape[0] == T.shape[1]
    assert T.shape[0] == 2
    T = T.astype(float)
    Id = np.array([[1., 0.],
                   [0., 1.]])
    return np.trace(T) / 2 * Id


def dev(T):
    """Returns the deviatoric component of a 2D matrix.

    Parameters
    ----------
    T : NDarray(Float)
        The tensor in question.

    Returns
    -------
    NDarray(Float)
        The expected deviatoric part.

    """
    assert T.shape[0] == T.shape[1]
    assert T.shape[0] == 2
    T = T.astype(float)
    return T - iso(T)


# --*-- Tensor characteristics

def norm(T):
    """Returns the norm of a matrix.

    Parameters
    ----------
    T : NDarray(Float)
        The tensor in question.

    Returns
    -------
    Float
        The norm defined as sqrt(tr(T^tT)/dim).

    """
    assert T.shape[0] == T.shape[1]
    dim = T.shape[0]
    T = T.astype(float)
    T2 = np.dot(T, T)
    return np.sqrt(np.trace(T2) / dim)


def intensity(T):
    """Returns the intensity of a 2D 2nd order symmetric tensor.

    Parameters
    ----------
    T : NDarray(Float)
        The tensor in question.

    Returns
    -------
    Float
        The norm defined as sqrt(tr(T^tT)/dim).

    """
    assert T.shape[0] == T.shape[1]
    T = T.astype(float)
    return norm(iso(T))


def anisotropy(T):
    """Returns the anisotropy of a 2D 2nd order symmetric tensor.

    Parameters
    ----------
    T : NDarray(Float)
        The tensor in question.

    Returns
    -------
    Float
        The anisotropy measure.

    """
    assert T.shape[0] == T.shape[1]
    assert T.shape[0] == 2
    T = T.astype(float)
    D = dev(T)
    J = iso(T)
    if intensity(T) == 0:
        return 0
    else:
        return norm(D) / norm(J)


def max_eigvect(T):
    """
    Returns the eigvec associated with the highest eigval of a 2D matrix.

    Parameters
    ----------
    T : NDarray(Float)
        The tensor in question.

    Returns
    -------
    NDarray(Float)
        The eigen vector you dreamed of.

    """
    assert T.shape[0] == 2
    T = T.astype(float)

    eigen_values, eigen_vectors = lng.eigh(T)

    # -- Just to be sure that the eigen values are stored in the proper order.
    assert eigen_values[-1] >= eigen_values[0]

    return eigen_vectors[-1]


def min_eigvect(T):
    """Returns the eigvec associated with the smallest eigval of a 2D matrix.

    Parameters
    ----------
    T : NDarray(Float)
        The tensor in question.

    Returns
    -------
    NDarray
        the eigen vector.

    Note:
    -----
    The amplitude of the vector corresponds to the anisotropy of the tensor.

    """
    assert T.shape[0] == 2
    T = T.astype(float)

    eigen_values, eigen_vectors = lng.eigh(T)

    # -- Just to be sure that the eigen values are stored in the proper order.
    assert eigen_values[-1] >= eigen_values[0]

    return eigen_vectors[0]


def eig_dir_angle(T):
    """Returns the orientation angle (in rad) of the eigen direction.

    Parameters
    ----------
    T : NDarray(Float)
        The tensor in question.

    Returns
    -------
    Float
        The angle of the max (to check) eigen direction
        with respect to the x axis.

    """
    assert T.shape[0] == 2
    T = T.astype(float)

    return .5 * np.arctan(2 * T[0, 1] / (T[0, 0] - T[1, 1]))


def max_eigen_vector(T):
    """Computes the max eigenvector of a 2D symmetric 2nd order tensor.

    Parameters
    ----------
    T : NDarray(Float)
        The tensor in question.

    Returns
    -------
    max_eigen_vect : NDarray(Float)
        The eigen vector you dreamed of in 3D (with O as third coordinate).

    Note
    ----
    This function is an alternative to the max_eigvect() one and makes use
    of the eig_dir_angle() function based on the co-authors implementation.

    """

    assert T.shape[0] == 2
    T = T.astype(float)

    angle = eig_dir_angle(T)
    ortho = angle + np.pi/2

    if np.tensordot(T, proj(angle)) > np.tensordot(T, proj(ortho)):
        max_eigen_vect = np.array([np.cos(angle), np.sin(angle), 0.])
    else:
        max_eigen_vect = np.array([np.cos(ortho), np.sin(ortho), 0.])
    return max_eigen_vect


# tools.py ends here.
