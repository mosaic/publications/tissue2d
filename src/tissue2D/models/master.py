# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:


__all__ = ['Tissue2DModel']

import logging
import numpy as np

from tissue2D.models.mechanics import MechanicalModel, MECHA_PARAM_DEFAULT
from tissue2D.models.division import CellDivisionModel
from tissue2D.models.growth import GrowthModel
from tissue2D.models.stiffening import StiffeningModel
from tissue2D.utils.simulations_recording import save_data


# -- Useful visualization parameters
VISU_PARAM_DEFLT = {'back_ground_color': 'black',
                    'field_of_view': 10,
                    'cell_property': 'area',
                    'cell_colormap': 'viridis',
                    'cell_alpha': .0,
                    'edge_property': "cwll_stiffness",
                    'edge_thickness': 2,
                    'edge_color': 'r'}

# ------------------------------------------------------------------------------
# Master model class:
# One model to rule  them all, one model to find them,
# One model to bring them all and in the darkness bind them.
# ------------------------------------------------------------------------------


class Tissue2DModel():
    """Master model to simulate morphogenesis of a 2 dimensional model.

    Parameters
    ----------


    Attributes
    ----------
    _parameters : type
        Description of attribute `_parameters`.
    mesh : type
        Description of attribute `mesh`.

    """

    def __init__(self):
        # -- Parameters defining what model(s) we include within the master

        # -- Visualization Parameters
        self._visu_each_step = None
        self._visu_parameters = None
        self._show_stress_orient = None
        self._show_recent_div = None
        self._save_figure = None
        self._save_simulation = None
        self._recording_path = None

        # -- Parameters of the master model only
        self._simulation_type = 'master'
        self._mechanics = None
        self._growth = None
        self._division = None
        self._stiffnening = None
        self._t_min = None
        self._t_max = None
        self._dt = None

        # -- Input parameters of the mechanical model
        self._mechanical_model = MechanicalModel()
        self._mechanical_parameters = None
        self._pressure = None
        self._cwll_stiffness_ratio = None
        self._factor = None

        # -- Input parameters of the growth model
        self._growth_model = GrowthModel()
        self._growth_type = None
        self._growth_rate = None
        self._growth_threshold = None
        self._growth_hill_exponent = None
        self._growth_hill_amplitude = None

        # -- Input parameters of the division model
        self._division_model = CellDivisionModel()
        self._division_threshold = None
        self._orientation_method = None
        self._loading_coeff = None

        # -- Input parameters of the stiffening model
        self._stiffening_model = StiffeningModel()
        self._stiffening_rate = None
        self._stiffening_method = None
        self._edge_type_2_stiffen = None
        self._stiffening_hill_threshold = None
        self._stiffening_hill_exponent = None
        self._stiffening_hill_amplitude = None
        self._cwll_memory = None

        # -- Inital mesh
        self._mesh = None
        self._mesh_name = None

        # -- Outputs of the mechanical equilibrium estimation
        self.mech_equi_mecha = None
        self.stress_field = None
        self.vtx_dsplcmnt_field = None

    def set_mesh(self, mesh, mesh_name='2Dmesh'):
        self._mesh = mesh
        self._mesh_name = mesh_name

    def set_processes(self,
                      mechanics=True,
                      growth=False,
                      division=False,
                      stiffening=False,
                      recording=False,
                      recording_path=None):

        self._mechanics = mechanics
        self._growth = growth
        self._division = division
        self._stiffening = stiffening

        self._save_simulation = recording
        self._recording_path = recording_path

    def set_visualization_parameters(self,
                                     visu_each_step=False,
                                     show_stress_orient=False,
                                     show_recent_div=False,
                                     save_fig=False,
                                     visu_params=VISU_PARAM_DEFLT):

        self._visu_each_step = visu_each_step
        self._show_stress_orient = show_stress_orient
        self._show_recent_div = show_recent_div
        self._save_figure = save_fig

        if self._visu_each_step:
            self._visu_parameters = visu_params

    def set_mechanical_parameters(self,
                                  pressure=MECHA_PARAM_DEFAULT['pressure'],
                                  stiffness_ratio=MECHA_PARAM_DEFAULT['cwll_stiffness_ratio'],
                                  factor=.25):
        self._pressure = pressure
        self._cwll_stiffness_ratio = stiffness_ratio
        self._mechanical_parameters = {'pressure':
                                       self._pressure,
                                       'cwll_stiffness_ratio':
                                       self._cwll_stiffness_ratio}
        self._factor = factor


        self._mechanical_model.set_parameters(self._mechanical_parameters,
                                              self._factor)

    def set_growth_parameters(self,
                              rate=.5,
                              threshold=0,
                              type='hill',
                              exponent=5,
                              amplitude=1.):
        self._growth_type = type
        self._growth_rate = rate
        self._growth_threshold = threshold
        self._growth_hill_exponent = exponent
        self._growth_hill_amplitude = amplitude

        self._growth_model.set_parameters(self._growth_rate,
                                          self._growth_threshold,
                                          self._growth_type,
                                          self._growth_hill_exponent,
                                          self._growth_hill_amplitude)

    def set_division_parameters(self,
                                threshold=0,
                                method='random',
                                loading_coeff=0.):
        self._division_threshold = threshold
        self._orientation_method = method
        self._loading_coeff = loading_coeff

        self._division_model.set_parameters(self._division_threshold,
                                            self._orientation_method,
                                            self._loading_coeff)

    def set_stiffening_parameters(self,
                                  rate=.1,
                                  cwll_stiffness_ratio=3.,
                                  method='along_max_stress',
                                  edges='all',
                                  threshold=.5,
                                  exponent=5,
                                  amplitude=1.,
                                  cwll_memory=False):

        self._cwll_memory = cwll_memory
        self._stiffening_rate = rate
        self._stiffening_method = method
        self._edge_type_2_stiffen = edges
        self._stiffening_hill_threshold = threshold
        self._stiffening_hill_exponent = exponent
        self._stiffening_hill_amplitude = amplitude

        if cwll_stiffness_ratio:
            self._cwll_stiffness_ratio = cwll_stiffness_ratio

        self._stiffening_model.set_parameters(self._stiffening_rate,
                                              self._cwll_stiffness_ratio,
                                              self._stiffening_method,
                                              self._edge_type_2_stiffen,
                                              self._stiffening_hill_threshold,
                                              self._stiffening_hill_exponent,
                                              self._stiffening_hill_amplitude,
                                              self._cwll_memory)

    def set_log(self):

        logging.info('\n')
        logging.info('=========================')
        logging.info('== LOADING  SIMULATION ==')
        logging.info('=========================\n')
        logging.info('== Simulation Settings ==')
        logging.info('-+ Number of simulation steps: '
                     + str((self._t_max - self._t_min)/float(self._dt) + 1))
        logging.info(' |')
        logging.info('-+   Tissue characteristics:')
        logging.info(' |')
        logging.info(' +-> Name: ' + self._mesh_name)
        logging.info(' |')
        logging.info(' +-> Number of: ')
        logging.info(' |   * Cells: ' + str(self._mesh.nb_wisps(2)))
        logging.info(' |   * Walls: ' + str(self._mesh.nb_wisps(1)))
        logging.info(' |   * Vertices: ' + str(self._mesh.nb_wisps(0)))
        logging.info(' |')

        mesh_width = (np.max(self._mesh.wisp_property('barycenter',
                                                      0).values()[:, 0])
                      - np.min(self._mesh.wisp_property('barycenter',
                                                        0).values()[:, 0]))
        mesh_height = (np.max(self._mesh.wisp_property('barycenter',
                                                       0).values()[:, 1])
                       - np.min(self._mesh.wisp_property('barycenter',
                                                         0).values()[:, 1]))

        logging.info(' +-> Initial dimensions: ')
        logging.info('     * Height: '+str(mesh_height)[: 5])
        logging.info('     * Width: '+str(mesh_width)[: 5])
        logging.info('     * Aspect ratio: '+str(mesh_width/mesh_height)[: 5])
        logging.info('')

        logging.info('-+  Included processes:')
        if self._mechanics:
            logging.info(' |')
            self._mechanical_model.set_log()
        if self._growth:
            logging.info(' |')
            self._growth_model.set_log()
        if self._division:
            logging.info(' |')
            self._division_model.set_log()
        if self._stiffening:
            logging.info(' |')
            self._stiffening_model.set_log()
        if self._save_simulation:
            logging.info('')
            logging.info('-+  Simulation will be recorded')
            logging.info(' |')
            logging.info(' +-> Location: '+self._recording_path)
        logging.info('')

        if self._visu_each_step:
            logging.info('-+  Snapshots will be displayed at each step')

            if self._show_stress_orient:
                logging.info(' |')
                logging.info(' +-> with stress orientation represented')

            if self._show_recent_div:
                logging.info(' |')
                logging.info(' +-> with recent division represented')

            if self._save_figure:
                logging.info(' |')
                logging.info(' +-> These Snapshots will be recorded')

        logging.info('\n')
        logging.info('=========================')
        logging.info('== STARTING SIMULATION ==')
        logging.info('=========================')

    def reset(self, mesh):
        self.set_mesh(mesh)

    def step(self, time, dt_growth=1, dt_stiffening=1):
        logging.info('\n')
        logging.info('-------------------------')
        logging.info('-+  ' + self._simulation_type + ' Step # ' + str(time))

        if self._mechanics:
            self._mechanical_model.set_mesh(self._mesh)
            self._mechanical_model.set_parameters(self._mechanical_parameters,
                                                  self._factor)
            self._mechanical_model.step(time)
            self._mesh = self._mechanical_model._mesh
        if self._growth:
            self.set_growth_parameters(self._growth_rate,
                                       self._growth_threshold,
                                       self._growth_type,
                                       self._growth_hill_exponent,
                                       self._growth_hill_amplitude)
            self._growth_model.set_mesh(self._mesh)
            self._growth_model._t_min = self._t_min
            logging.debug('growth t_min: ' + str(self._growth_model._t_min))
            self._growth_model._t_max = self._t_max
            self._growth_model.step(time, dt_growth)
            self._mesh = self._growth_model._mesh
        if self._stiffening:
            self.set_stiffening_parameters(self._stiffening_rate,
                                           self._cwll_stiffness_ratio,
                                           self._stiffening_method,
                                           self._edge_type_2_stiffen,
                                           self._stiffening_hill_threshold,
                                           self._stiffening_hill_exponent,
                                           self._stiffening_hill_amplitude,
                                           self._cwll_memory)
            self._stiffening_model.set_mesh(self._mesh)
            self._stiffening_model.run(time, dt_stiffening)
            self._mesh = self._stiffening_model._mesh
        if self._division:
            self.set_division_parameters(self._division_threshold,
                                         self._orientation_method,
                                         self._loading_coeff)
            self._division_model.set_mesh(self._mesh)
            self._division_model.step(time)
            self._mesh = self._division_model._mesh

    def run(self, t_min=0, t_max=1, dt=1):
        self._t_min = t_min
        self._t_max = t_max
        self._dt = dt

        print(t_min, t_max, dt, (t_max - t_min)/float(dt) + 1)

        self.set_log()

        for t in np.linspace(t_min, t_max, int((t_max - t_min)/float(dt)) + 1):
            self.step(t, dt_growth=dt, dt_stiffening=dt)

            if self._save_simulation:
                save_data(self, t)




# master.py ends here.
