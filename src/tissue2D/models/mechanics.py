# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:


__all__ = ['MechanicalModel']
import logging
import numpy as np
from time import time
import numpy.linalg as lng
from scipy import optimize as opt

from cellcomplex.utils.array_dict import array_dict
from cellcomplex.property_topomesh.analysis import compute_topomesh_property

from tissue2D.utils.simulations_recording import save_data
from tissue2D.utils.tools import (inflate_structure,
                                  compute_position_array_from_mesh,
                                  compute_position_array_from_arraydict,
                                  compute_tensor_from_vectors,
                                  intensity,
                                  anisotropy, orienting_vertices)

# -- Default parameter values
MECHA_PARAM_DEFAULT = {'pressure': .1, 'cwll_stiffness_ratio': 3}


# ------------------------------------------------------------------------------
# Mechanical model class
# ------------------------------------------------------------------------------
class MechanicalModel(object):
    """Mechanical model for 2D propertyTopomesh.

    Parameters
    ----------


    Attributes
    ----------
    _mechanical_parameters : type
        Description of attribute `_mechanical_parameters`.
    _include_cmf_forces : type
        Description of attribute `_include_cmf_forces`.
    _factor : type
        Description of attribute `_factor`.
    mesh : type
        Description of attribute `mesh`.
    mech_equi_mecha : type
        Description of attribute `mech_equi_mecha`.
    stress_field : type
        Description of attribute `stress_field`.
    vtx_dsplcmnt_field : type
        Description of attribute `vtx_dsplcmnt_field`.

    """

    def __init__(self):
        # -- Parameters of the model
        self._simulation_type = 'mechanics'
        self._t_min = None
        self._t_max = None
        self._dt = None
        self._save_simulation = None
        self._recording_path = None

        # -- Input parameters
        self._mechanical_parameters = None
        self._factor = None

        # -- Inital mesh
        self._mesh = None
        self._mesh_name = None

    def set_mesh(self, mesh, mesh_name='mesh2D'):
        self._mesh = mesh
        self._mesh_name = mesh_name

    def set_parameters(self,
                       mechanical_parameters=MECHA_PARAM_DEFAULT,
                       factor=.1,
                       save_simulation=False,
                       recording_path=None):

        self._mechanical_parameters = mechanical_parameters
        self._factor = factor
        self._save_simulation = save_simulation
        self._recording_path = recording_path

    def set_log(self):
        logging.info(' +-> ' + self._simulation_type)

        for param_name, param_vals in self._mechanical_parameters.items():
            logging.info(' |   * ' + param_name + ': ' + str(param_vals))

    def reset(self,
              mesh,
              mechanical_parameters=MECHA_PARAM_DEFAULT,
              factor=.1):
        self.set_mesh(mesh)
        self.set_parameter(mechanical_parameters, factor)

    def step(self, time):
        logging.info(' |')
        logging.info(' +-> ' + self._simulation_type + ' Step # ' + str(time))

        compute_mechanical_equilibrium(self._mesh,
                                       factor=self._factor,
                                       mechanical_parameters=self._mechanical_parameters)

    def run(self, t_min=0, t_max=1, dt=1):
        self._t_min = t_min
        self._t_max = t_max
        self._dt = dt

        for t in np.linspace(t_min, t_max, (t_max - t_min)/float(dt)):
            self.step(t)

            if self._save_simulation:
                print('   |')
                print('   +-> Recording step #', t)
                save_data(self, t, folder_path=self._recording_path)


# ------------------------------------------------------------------------------
# Methods to compute forces
# ------------------------------------------------------------------------------
def compute_pressure_force(vertex_positions,
                           mesh, vid_2_idx,
                           pressure_value=None):
    """Computes a pressure force on the vertices of a mesh.

    Parameters
    ----------
    vertex_positions : NDarray(float)
        A (2*n)D vector containing the positions of all the vertices

    mesh : PropertyTopomesh
        The tissue we want to compute pressure forces on.

    vid_2_idx : Dict(Int)
        - keys : indices (vid) of the vertex positions within the mesh.
        - values : index (idx) of the vertex positions within the array.

    pressure_value : Float
        Optional. A custom value for the pressure within the tissue.

    Returns
    -------
    pressure_forces : Dict(ndarray(float))
        - keys  : vertex ids
        - values: 2D vector representing the pressure force at the considered
                  vertex.
    """

    # -- Reshaping the position vector
    vertex_number = len(list(mesh.wisps(0)))
    vertex_positions = vertex_positions.reshape(vertex_number, 2)

    # -- Initializing pressure forces
    pressure_forces = {vid: np.array([0., 0., 0.]) for vid in mesh.wisps(0)}

    if pressure_value:
        pressure = array_dict({cid: pressure_value for cid in mesh.wisps(2)})

    elif 'pressure' in mesh.wisp_properties(2).keys():
        pressure = mesh.wisp_property('pressure', 2)
    else:
        print('L.174 - mechanics - WARNING: Turgor pressure not defined.')

    # -- Computing forces on each vertex
    for cid in mesh.wisps(2):
        vtx_ids = mesh.wisp_property('oriented_vertices', 2)[cid]

        # -- Lists of vtx ids +/- 1.
        prev_vtx_ids = np.concatenate((vtx_ids[-1:], vtx_ids[:-1]), axis=0)
        next_vtx_ids = np.concatenate((vtx_ids[1:], vtx_ids[:1]), axis=0)

        for prev_vid, vid, next_vid in zip(prev_vtx_ids,
                                           vtx_ids,
                                           next_vtx_ids):

            prev_vtx_pos = vertex_positions[vid_2_idx[prev_vid]]
            next_vtx_pos = vertex_positions[vid_2_idx[next_vid]]

            pressure_forces[vid] += .5 * pressure[cid] *\
                (np.array([prev_vtx_pos[1], -prev_vtx_pos[0], 0.])
                 + np.array([-next_vtx_pos[1], next_vtx_pos[0], 0.]))

    return pressure_forces


def compute_cellwall_force(vertex_positions,
                           mesh,
                           vid_2_idx,
                           cwll_stiffness_value=None,
                           cw_resting_length_value=None,
                           cid_2_consider='all'):
    """Computes cell wall elastic forces on the vertices of a mesh.

    Parameters
    ----------
    vertex_positions : NDarray(float)
        A (2*n)D vector containing the positions of all the vertices
    mesh : PropertyTopomesh
        The tissue we want to compute pressure forces on.
    vid_2_idx : Dict(Int)
        - keys : indices (vid) of the vertex positions within the mesh.
        - values : index (idx) of the vertex positions within the array.
    cwll_stiffness_value: Dict(Float)
        Optional.
        - keys : edge ids
        - values : custom values for the cell wall stiffness.
    cw_resting_length_value Float
        Optional. A custom value for the walls resting length.
    cid_2_consider : str or int
        Optional (default : "all"). If a cell id is given the forces are
        computed only for this cell.

    Returns
    -------
    cw_response_force : Dict(ndarray(float))
        - keys  : vertex ids
        - values: 2D vector representing the elastic response force
                  of the cell walls at the considered vertex.
    """
    # -- Reshaping the position vector
    if cid_2_consider == 'all':
        vtx_ids = list(mesh.wisps(0))
        edg_ids = list(mesh.wisps(1))

    elif cid_2_consider in mesh.wisps(2):
        vtx_ids = list(mesh.borders(2, cid_2_consider, 2))
        edg_ids = list(mesh.borders(2, cid_2_consider, 1))

    vertex_number = len(vtx_ids)
    vertex_positions = vertex_positions.reshape(vertex_number, 2)

    # -- Initialization of elastic forces
    cw_response_forces = {vid: np.array([0., 0., 0.]) for vid in vtx_ids}

    # --*-- Setting the resting length
    if (cw_resting_length_value and not isinstance(cw_resting_length_value,
                                                   dict)):
        resting_length = array_dict({eid: cw_resting_length_value
                                     for eid in edg_ids})

    elif (cw_resting_length_value and isinstance(cw_resting_length_value,
                                                 dict)):
        resting_length = array_dict(cw_resting_length_value)
    elif (not cw_resting_length_value
          and 'resting_length' in mesh.wisp_properties(1).keys()):
        resting_length = {eid: mesh.wisp_property('resting_length', 1)[eid]
                          for eid in edg_ids}

    elif (not cw_resting_length_value
          and 'resting_length' not in mesh.wisp_properties(1).keys()):
        compute_topomesh_property(mesh, 'length', 1)
        resting_length = {eid: mesh.wisp_property('length', 1)[eid]
                          for eid in edg_ids}

    else:
        print('WARNING: Cell wall resting length not defined.')

    # --*-- Setting the stiffness coefficients
    if cwll_stiffness_value:
        stiffness = array_dict(cwll_stiffness_value)

    elif (not cwll_stiffness_value
          and 'cwll_stiffness' in mesh.wisp_properties(1).keys()):
        stiffness = mesh.wisp_property('cwll_stiffness', 1)
    else:
        print('WARNING: Cell wall stiffness not defined.')

    # -- Computing the forces for each vertex
    for eid, rest_lngth in resting_length.items():
        vtx_ids = list(mesh.borders(1, eid))
        pos1_2D, pos2_2D = list(map(lambda x: vertex_positions[vid_2_idx[x]],
                                    vtx_ids))
        pos1 = np.concatenate((pos1_2D, np.array([0.])), axis=0)
        pos2 = np.concatenate((pos2_2D, np.array([0.])), axis=0)
        curr_lngth = lng.norm(pos1 - pos2)

        cw_response_forces[vtx_ids[0]] += - stiffness[eid] / curr_lngth *\
            (curr_lngth / rest_lngth - 1) * (pos1 - pos2)
        cw_response_forces[vtx_ids[1]] += + stiffness[eid] / curr_lngth *\
            (curr_lngth / rest_lngth - 1) * (pos1 - pos2)

    return cw_response_forces


def pressure_force_one_cell(mesh, cid, pressure_value=None):
    """Computes the pressure forces on all the vertices surrounding a cell.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we are working on.
    cid : Int
        The index of the cell we are working on within the tissue.
    pressure_value : Float
        Optional (default : None). A custom value for the pressure to
        consider.

    Returns
    -------
    cell_pressure_forces : Dict(NDarray(Float))
        - keys : vertex ids.
        - values : NDarray(Float). 3D force vector (third component=0).

    """
    vtx_ids = mesh.wisp_property('oriented_vertices', 2)[cid]
    vtx_pos = mesh.wisp_property('barycenter', 0)

    # -- defining the pressure
    if pressure_value:
        pressure = array_dict({cid: pressure_value for cid in mesh.wisps(2)})

    elif (not pressure_value and 'pressure' in mesh.wisp_properties(2).keys()):
        pressure = mesh.wisp_property('pressure', 2)

    else:
        print('WARNING: Turgor pressure not defined.')

    # -- Lists of vtx ids +/- 1.
    prev_vtx_ids = np.concatenate((vtx_ids[-1:], vtx_ids[:-1]), axis=0)
    next_vtx_ids = np.concatenate((vtx_ids[1:], vtx_ids[:1]), axis=0)

    # -- Computing the forces
    cell_pressure_forces = {vid: np.array([0., 0., 0.])
                            for vid in vtx_ids}

    for prev_vid, vid, next_vid in zip(prev_vtx_ids,
                                       vtx_ids,
                                       next_vtx_ids):

        prev_vtx_pos = vtx_pos[prev_vid]
        next_vtx_pos = vtx_pos[next_vid]

        cell_pressure_forces[vid] += pressure[cid] / 2 *\
            (np.array([prev_vtx_pos[1], -prev_vtx_pos[0], 0.])
             + np.array([-next_vtx_pos[1], next_vtx_pos[0], 0.]))

    return cell_pressure_forces


def elastic_force_one_cell(mesh, cid):
    """Compute the elastic forces generated by the edges around a cell.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we work on.
    cid : type
        the index of the considered cell within this mesh.

    Returns
    -------
    elastic_force : Dict(NDarray(Float))
        - keys : index of vertices around the cell.
        - values : 3D force vector (NDarray with null third component)

    """

    vtx_ids = mesh.wisp_property('oriented_vertices', 2)[cid]

    # -- Lists of vtx ids +/- 1.
    prev_vtx_ids = np.concatenate((vtx_ids[-1:], vtx_ids[:-1]), axis=0)
    next_vtx_ids = np.concatenate((vtx_ids[1:], vtx_ids[:1]), axis=0)

    vtx_pos = mesh.wisp_property('barycenter', 0)

    elastic_force = {}
    for prev_vid, vid, next_vid in zip(prev_vtx_ids,
                                       vtx_ids,
                                       next_vtx_ids):

        elastic_force[vid] = np.zeros(3)

        # -- Getting the edges around the considered vertex in the proper order
        prev_eid = list(set(mesh.regions(0, prev_vid))
                        & set(mesh.regions(0, vid)))[0]
        next_eid = list(set(mesh.regions(0, next_vid))
                        & set(mesh.regions(0, vid)))[0]

        # -- Computing the elastic force for each edge around the vertex.
        for neighb_vid, neighb_eid in zip([prev_vid, next_vid],
                                          [prev_eid, next_eid]):
            edge_vector = vtx_pos[neighb_vid] - vtx_pos[vid]
            current_length = lng.norm(edge_vector)
            current_direct = edge_vector / current_length

            if 'resting_length' in mesh.wisp_properties(1).keys():
                resting_length = mesh.wisp_property('resting_length',
                                                    1)[neighb_eid]
            else:
                resting_length = mesh.wisp_property('length', 1)[neighb_eid]

            if mesh.wisp_property('edge_type', 1)[neighb_eid] == 'outer':
                cwll_stiffness = mesh.wisp_property('cwll_stiffness',
                                                    1)[neighb_eid]
            else:
                cwll_stiffness = .5 * mesh.wisp_property('cwll_stiffness',
                                                         1)[neighb_eid]

            elastic_force[vid] += - cwll_stiffness *\
                (current_length / resting_length - 1) * current_direct

    return elastic_force


def compute_external_forces(mesh, cid, global_forces):

    inner_pressure_forces = pressure_force_one_cell(mesh,
                                                    cid,
                                                    pressure_value=None)

    external_forces = {vid: global_forces[vid] - inner_pressure_force
                       for vid, inner_pressure_force
                       in inner_pressure_forces.items()}

    return external_forces


def compute_residual_forces(vtx_pos_2nD_final,
                            mesh,
                            vid_2_idx,
                            mechanical_parameters=None):
    """Computes the total force at each vertex.

    Parameters
    ----------
    vtx_pos_2nD_final : NDarray(float)
        A (2*n)D vector containing the positions of all the vertices
    mesh : PropertyTopomesh
        The tissue we want to compute pressure forces on.
    vid_2_idx : Dict(Int)
        - keys : indices (vid) of the vertex positions
                 within the PropertyTopomesh.
        - values : index (idx) of the  vertex positions
                   within the reshaped array.
    mechanical_parameters : dict(float)
        - keys : str, mechanical parameter names amongst 'pressure'
                'cwll_stiffness_ratio', 'cmf_relative_stiffness'
        - values : float, the values to use.

    Returns
    -------
    norm_residual_forces : NDarray(float)
        a vector containing the norm of the final force at each vertex.

    """
    # -- Checking for custom mechanical parameters
    mecha_param_custom_vals = {}

    if mechanical_parameters:
        for param_name in ['pressure',
                           'cwll_stiffness_ratio',
                           'cmf_relative_stiffness',
                           'cmf_orientation_method']:
            if param_name in mechanical_parameters.keys():
                mecha_param_custom_vals[param_name] = mechanical_parameters[param_name]

    # -- Computing the edges stiffness dict from the stiffness_ratio value.
    cwll_stiffness = mesh.wisp_property('cwll_stiffness', 1)

    # -- Computing the forces
    pressure_forces_final = compute_pressure_force(vtx_pos_2nD_final,
                                                   mesh,
                                                   vid_2_idx)#,
                                                   #pressure_value=mecha_param_custom_vals['pressure'])

    cellwall_forces_final = compute_cellwall_force(vtx_pos_2nD_final,
                                                   mesh,
                                                   vid_2_idx,
                                                   cwll_stiffness_value=cwll_stiffness)

    total_force_field_final = [pressure_forces_final[vid]
                               + cellwall_forces_final[vid]
                               for vid in mesh.wisps(0)]

    norm_residual_forces = [lng.norm(force)
                            for force in total_force_field_final]

    return norm_residual_forces


def compute_mechanical_equilibrium(mesh,
                                   factor=.2,
                                   mechanical_parameters=MECHA_PARAM_DEFAULT):
    """Computes the mechanical equilibrium positions of a PropertyTopomesh.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to compute pressure forces on.
    factor : Float
        Optional (default=.1). An inflation factor used to generate
        an initial guess for the root finding algorithm.
    mechanical_parameters : Dict(NDarray(Float))
        Optional (default=None). Putative custom values to use for
        the mechanical parameters of the model.

    Returns
    -------
    None.
    But updates the considered PropertyTopomesh with the following properties:
    degree 0:
        barycenter (updated with the equilibrium positions).
        previous_barycenter (initial position)
    degree 1:
        edge_stretch (length increase of the edges)
        edge_strain (stretch divided by the initial length)
    degree 2:
        stress_tensor (the 2x2 stress tensor at mechanical equilibrium)

    Notes
    -----
    stress_field : Dict(NDarray)
        - keys : Int. The cell ids
        - values : NDarray(float). 2x2 symmetric tensor of stress per cell.
    vtx_displacement_field : Dict(NDarray)
        - keys : Int. The vertices ids
        - values : NDarray(float). The 3D vectors of displacement between
        the initial structure and the one at mechanical equilibrium
        (thrid component = 0).

    """
    vtx_nbr = mesh.nb_wisps(0)
    vtx_pos_init = mesh.wisp_property('barycenter', 0)
    vtx_pos_2nD, vid_2_idx, idx_2_vid = compute_position_array_from_mesh(mesh)

    # -- Checking for custom mechanical parameters
    mecha_param_custom_vals = {}

    for param_name in ['pressure', 'cwll_stiffness']:
        if (mechanical_parameters
                and param_name in mechanical_parameters.keys()):
            mecha_param_custom_vals[param_name] =\
                mechanical_parameters[param_name]

        elif param_name in mesh.wisp_properties(2).keys():
            mecha_param_custom_vals[param_name] =\
                mesh.wisp_property(param_name, 2)

        elif param_name in mesh.wisp_properties(1).keys():
            mecha_param_custom_vals[param_name] =\
                mesh.wisp_property(param_name, 1)
        else:
            mecha_param_custom_vals[param_name] = None

    # -- Definition the force balance to solve
    def force_balance(vtx_pos_2nD):
        pressure_forces = compute_pressure_force(vtx_pos_2nD,
                                                 mesh,
                                                 vid_2_idx,
                                                 pressure_value=mecha_param_custom_vals['pressure'])

        cellwall_forces = compute_cellwall_force(vtx_pos_2nD,
                                                 mesh,
                                                 vid_2_idx,
                                                 cwll_stiffness_value=mecha_param_custom_vals['cwll_stiffness'])

        total_force_field = [pressure_forces[vid]
                             + cellwall_forces[vid] for vid in mesh.wisps(0)]

        total_force_vector_3nD = np.concatenate(total_force_field)
        total_force_vector_2nD = np.array([element for id, element
                                           in enumerate(total_force_vector_3nD)
                                           if (id+1) % 3 != 0])

        return total_force_vector_2nD

    # -- Computing an initial guess for the solution
    inflated_positions = array_dict(inflate_structure(mesh, factor=factor))
    vtx_pos_2nD_init, _, _ = compute_position_array_from_arraydict(inflated_positions)

    # -- Finding the root
    start_time = time()
    sol = opt.root(force_balance, vtx_pos_2nD_init, method='hybr')
    end_time = time()

    convergence = sol.success
    convergence_time = end_time - start_time

    # -- Computing the equilibrium positions of the vertices
    vtx_pos_2nD_final = sol.x
    vtx_pos_2nD_final_reshaped = vtx_pos_2nD_final.reshape(vtx_nbr, 2)
    vtx_pos_almost_final = {idx_2_vid[idx]:
                            np.concatenate((vtx_pos_2nD_final_reshaped[idx//2],
                                            np.zeros(1)))
                            for idx in range(len(vtx_pos_2nD_final))}

    new_center = np.mean(list(vtx_pos_almost_final.values()), axis=0)

    vtx_pos_final = {vid: pos - new_center for vid, pos
                     in vtx_pos_almost_final.items()}
    vtx_pos_final = array_dict(vtx_pos_final)

    # --*-- updating the mesh with the new positions
    mesh.update_wisp_property('barycenter', 0, vtx_pos_final)
    mesh.update_wisp_property('previous_barycenter', 0, vtx_pos_init)

    # -- Compute properties to update after vertices displacement
    computable_properties = {0: [],
                             1: ['length', 'barycenter'],
                             2: ['barycenter', 'area'],
                             3: []}
    for degree in range(4):
        for prop_name in computable_properties[degree]:
            # -- Storing the previous values.
            previous_property = mesh.wisp_property(prop_name, degree)
            mesh.update_wisp_property('previous_'+prop_name,
                                      degree,
                                      previous_property)
            compute_topomesh_property(mesh, prop_name, degree)

    # --*-- Edges stretch and strain
    eids = list(mesh.wisps(1))
    curr_lngth = mesh.wisp_property('length', 1).values(eids)
    rest_lngth = mesh.wisp_property('resting_length', 1).values(eids)

    edge_stretch = {eid: curl - rest
                    for eid, curl, rest
                    in zip(eids, curr_lngth, rest_lngth)}
    edge_strain = {eid: curl / rest - 1
                   for eid, curl, rest
                   in zip(eids, curr_lngth, rest_lngth)}

    mesh.update_wisp_property('edge_stretch', 1, edge_stretch)
    mesh.update_wisp_property('edge_strain', 1, edge_strain)

    # # --*-- Computing the elastic forces within the walls
    # stiffness = mesh.wisp_property('cwll_stiffness', 1)
    # elastic_force = {eid: stiffness[eid] * strain
    #                  for eid, strain in edge_strain.items()}
    #
    # mesh.update_wisp_property('elastic_force', 1, elastic_force)

    # --*-- Cell stress tensor from forces
    stress = {}
    for cid in mesh.wisps(2):
        # --*--*-- Debugging
        logging.debug('Working on cell #' + str(cid))
        logging.debug('    - Edge borders: ' + str(list(mesh.borders(2, cid))))
        logging.debug('    - Corresponding vertices: '
                      + str(list(mesh.borders(2, cid))))

        ext_forces = elastic_force_one_cell(mesh, cid)
        stress[cid] = compute_tensor_from_vectors(mesh, ext_forces, cid)

    mesh.update_wisp_property('stress_tensor', 2, stress)

    # -- Logging
    generate_mechanical_simulation_log(mesh,
                            mecha_param_custom_vals,
                            vtx_pos_2nD_final,
                            vid_2_idx,
                            convergence,
                            convergence_time)


def generate_mechanical_simulation_log(mesh,
                                       mechanical_parameters,
                                       vtx_pos_2nD_final,
                                       vid_2_idx,
                                       convergence,
                                       convergence_time):
    """Generates logs from the mechanical simulation outputs.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we are working on.
    mechanical_parameters : dict
        - keys : str. The name of the mechanical parameters.
        - values : ndarray(float). The values of these parameters.
    vtx_pos_2nD_final : ndarray
        The positions of the mesh vertices once
        mechanical equilibrium is reached.
    vid_2_idx : ndarray
        The indices of the vertices in the topomesh data structure.
    convergence : bool
        True if an equilibrium has been reached, false otherwise.
    convergence_time : float
        The time the algorithm needed to reach equilibrium (in sec.).

    Returns
    -------
    None.

    """
    stress = mesh.wisp_property('stress_tensor', 2)
    strain = mesh.wisp_property('edge_strain', 1)
    stretch = mesh.wisp_property('edge_stretch', 1)

    strs_int = list(map(lambda x: intensity(x), list(stress.values())))
    strs_ani = list(map(lambda x: anisotropy(x), list(stress.values())))

    # -- Computing the residual forces
    norm_residual_forces = [0.]
    norm_residual_forces = compute_residual_forces(vtx_pos_2nD_final,
                                                   mesh,
                                                   vid_2_idx,
                                                   mechanical_parameters=mechanical_parameters)

    # -- Computing the tissue aspect ratio
    vrtx_posi = mesh.wisp_property('barycenter', 0).values()
    tiss_hght = vrtx_posi[:, 1].max() - vrtx_posi[:, 1].min()
    tiss_wdth = vrtx_posi[:, 0].max() - vrtx_posi[:, 0].min()

    # -- Logging post-simulation info
    logging.info(' |   * Solver: Scipy.optimize.root with hybrid method.')

    if convergence:
        logging.info(' |   * Convergence successful :)')
    else:
        logging.info(' |   * Convergence failed :(')

    logging.info(' |   * Convergence time: '
                 + str(int(convergence_time)) + ' sec.')
    logging.info(' |   * Max of resid. force norm: '
                 + '{:.2e}'.format(np.max(norm_residual_forces)) + ' N')
    logging.debug('|   * Tissue aspect ratio (w/h): '
                  + '{:.2e}'.format(tiss_wdth / tiss_hght))
    logging.debug('|   * Cell wall relative stiffness range: '
                  + '{:.2e}'.format(mesh.wisp_property('cwll_stiffness',
                                                       1).values().min())
                  + '{:.2e}'.format(mesh.wisp_property('cwll_stiffness',
                                                       1).values().max()))
    logging.debug('|   * Edge stretch (min, max): '
                  + '({:.2e}, '.format(np.min(list(stretch.values())))
                  + '{:.2e})'.format(np.max(list(stretch.values()))))
    logging.info(' |   * Edge strain (min, max): '
                 + '({:.2e}, '.format(np.min(list(strain.values())))
                 + '{:.2e})'.format(np.max(list(strain.values()))))
    logging.info(' |   * Cell stress (mean +/- std): ')
    logging.info(' |       - Intensity: ' + '{:.2e}'.format(np.mean(strs_int))
                 + ' +/- ' + '{:.2e}'.format(np.std(strs_int)))
    logging.info(' |       - Anisotropy: ' + '{:.2e}'.format(np.mean(strs_ani))
                 + ' +/- ' + '{:.2e}'.format(np.std(strs_ani)))


# mechanics.py ends here.
