# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:


__all__ = ['CellDivisionModel']

import logging
import copy
import numpy as np
import numpy.linalg as lng

from cellcomplex.property_topomesh.analysis import compute_topomesh_property

from tissue2D.utils.tools import (compute_intersection_line_cell,
                                  compute_epidermis,
                                  orienting_vertices,
                                  compute_cell_center,
                                  compute_direction, max_eigvect)


# ------------------------------------------------------------------------------
# Cellular division model class
# ------------------------------------------------------------------------------

class CellDivisionModel:
    """Cell division model for 2D propertyTopomesh.

    Parameters
    ----------


    Attributes
    ----------
    _parameters : type
        Description of attribute `_parameters`.
    mesh : type
        Description of attribute `mesh`.

    """

    def __init__(self):
        # -- Simuation Parameters
        self._simulation_type = 'division'

        # -- Input parameters
        self._division_threshold = None
        self._orientation_method = None

        # -- Initial mesh
        self._mesh = None

    def set_mesh(self, mesh):
        self._mesh = mesh

    def set_parameters(self,
                       threshold_value=0,
                       orientation_method='random',
                       loading_coeff=.1):
        self._division_threshold = threshold_value
        self._orientation_method = orientation_method
        self._loading_coeff = loading_coeff

    def set_log(self):
        logging.info(' +-> ' + self._simulation_type)

        for param_name, param_vals in {'Division threshold':
                                       self._division_threshold,
                                       'Orienation method':
                                       self._orientation_method,
                                       'Loading coefficient':
                                       self._loading_coeff}.items():
            logging.info(' |   * ' + param_name + ': ' + str(param_vals))

    def reset(self,
              mesh,
              threshold_value=0,
              orientation_method='random',
              loading_coeff=.1):
        self.set_mesh(mesh)
        self.set_parameter(threshold_value, orientation_method, loading_coeff)

    def step(self, time):
        logging.info(' |')
        logging.info(' +-> ' + self._simulation_type + ' Step # ' + str(time))

        self._mesh = divide_several_cells(self._mesh,
                                          self._division_threshold,
                                          self._orientation_method,
                                          self._loading_coeff)

        generate_division_simulation_log(self._mesh)

    def run(self):
        self.step()


# ------------------------------------------------------------------------------
# Methods to compute forces
# ------------------------------------------------------------------------------

def divide_several_cells(old_mesh,
                         threshold,
                         orientation_method='random',
                         loading_coeff=.1):
    """Divide cells within a tissue according to a given orientation rule.

    Parameters
    ----------
    old_mesh : PropertyTopomesh
        The tissue where the cell divisions are going to happen.
    threshold : Float
        The target area above which cells divide
    orientation_method : str
        Optional (default : 'random'). The name of the considered
        orientation rule (see Note below).
    loading_coeff : float
        Optional (default : .1). Should be between 0 and 1.
        Correspond to the putative intrinsic strain of the newly formed walls.

    Returns
    -------
    new_mesh : PropertyTopomesh
        An updated version of the tissue old_mesh where cells that met the
        division threshold have been split according to the orientation rule.

    Note
    ----
        So far consider only two orientation rules:
            * 'random' : The new edge orientation is picked at random in the
              [0, pi] range.
            * 'along_max_stress': The new edge is added along the direction
              associated with the max eigenvalue of the local stress tensor.
            * errera: The new edge is added perpendicular to the most elongated
              direction of the cell.
            * out_radial_in_random: for epidermal cells, the new wall is added
              in the apico-basal direction. For inner cells, its orientation is
              random.
            * out_radial_in_errera: for epidermal cells, the new wall is added
              in the apico-basal direction. For inner cells, its orientation
              follows the errera rule.

    """
    available_methods = ['along_max_stress', 'random', 'errera',
                         'radial', 'ortho_growth']

    new_mesh = copy.deepcopy(old_mesh)
    # -- Defining the orientations of cell divisions.
    if orientation_method in available_methods:
        direction = compute_direction(old_mesh, method=orientation_method)

    # --*-- If the division rule varies between inner tissues and epidermis.
    elif orientation_method[:10] == 'out_radial':
        inner_meth = orientation_method[14:]
        epidermis = compute_epidermis(old_mesh)

        outer_cids = [cid for cid, epiderm
                      in epidermis.items() if epiderm == 1]
        inner_cids = [cid for cid, epiderm
                      in epidermis.items() if epiderm == 0]

        direction = compute_direction(old_mesh,
                                      method='radial',
                                      cids_2_consider=outer_cids)
        inner_dir = compute_direction(old_mesh,
                                      method=inner_meth,
                                      cids_2_consider=inner_cids)

        direction.update(inner_dir)

    # -- Dividing cells
    non_dividing_cids = []
    for cid in old_mesh.wisps(2):
        non_dividing_cids = divide_one_cell(old_mesh,
                                            new_mesh,
                                            cid,
                                            non_dividing_cids,
                                            threshold,
                                            direction[cid],
                                            loading_coeff)

        old_mesh = copy.deepcopy(new_mesh)

    return new_mesh


def divide_one_cell(old_mesh,
                    new_mesh,
                    cid,
                    non_dividing_cids,
                    threshold,
                    direction=np.array([1., 0., 0.]),
                    loading_coeff=.1):
    """Divide a given cell into two daughters within a tissue.

    Parameters
    ----------
    old_mesh : PropertyTopomesh
        The tissue where the cell division is going to happen.
    new_mesh : PropertyTopomesh
        A deepcopy of mesh, where the tissue topological
        modifications will be stored.
    cid : Int
        The index of the cell to divide within the tissue.
    non_dividing_cid : list(int)
        List of indices of cells that should not be allowed to divide.
        Because they are neighbors of a cell that has already been divided
        during the same division round.
    threshold : Float
        A threshold value on cell surface area.
        If cell area > threshold -> division is initiated.
    direction : NDarray(Float)
        A 3D unit vector giving the orientation of the new edge to add.
    loading_coeff : float
        Optional (default : .1). Should be between 0 and 1.
        Correspond to the putative intrinsic strain of the newly formed walls.

    Returns
    -------
    None.

    Note:
    -----
        * This method returns nothing but updates the new_mesh structure.
        * This method also propagates the properties from mother to daughters.
        * This method add a 'lineage_mother_id' property to track cell
          (and edge) lineage between daughters in the new_mesh
          and their mother in mesh.
    """

    # -- Computing cell surf. area and orientation of vertices around the cell
    for property_name in ['area', 'oriented_vertices']:
        compute_topomesh_property(old_mesh, property_name, 2)

    assert 'oriented_borders' in old_mesh.wisp_properties(2).keys()

    cell_surf = old_mesh.wisp_property('area', 2)[cid]

    # -- Computing edge and cell lineage
    mother_id = {}
    for degree in [1, 2]:
        if 'lineage_mother_id' in old_mesh.wisp_properties(degree).keys():
            mother_id[degree] = old_mesh.wisp_property('lineage_mother_id',
                                                       degree)
        else:
            mother_id[degree] = {wid: -1 for wid in old_mesh.wisps(degree)}

    # -- The actual division loop
    if cell_surf > threshold and cid not in non_dividing_cids:

        logging.debug('Dividing cell #' + str(cid))

        # -- Updating the list of cells that should not divide
        # non_dividing_cids += list(old_mesh.border_neighbors(2, cid))
        # non_dividing_cids = list(set(non_dividing_cids))

        # -- Computing the biggest ids for cells and walls
        max_cid = max(new_mesh.wisps(2))
        max_eid = max(new_mesh.wisps(1))

        # -- Computing the division plane position
        cell_cntr = compute_cell_center(old_mesh, cid)
        intersection_positions = compute_intersection_line_cell(old_mesh,
                                                                cid,
                                                                direction,
                                                                cell_cntr)
        eids_2_remove = intersection_positions.keys()
        vtx_pos_2_add = intersection_positions.values()

        # -- Removing the hyper-cell (wisps(3)) linked to the dividing 2D one.
        new_mesh.unlink(3, cid, cid)
        new_mesh.remove_wisp(3, cid)

        # -- Updating the mesh topology
        # --*-- Unlinking the old cell and its edges
        old_oriented_edge_ids = old_mesh.wisp_property('oriented_borders',
                                                       2)[cid][0]
        for eid in old_oriented_edge_ids:
            # -- A little complication in the case of multiple cell divisions
            if (eid not in old_mesh.borders(2, cid)
                    and eid in list(mother_id[1].values())):
                new_eids = [new_eid for new_eid, old_eid
                            in mother_id[1].items() if old_eid == eid]

                eid = new_eids[0]
            if eid in new_mesh.borders(2, cid):
                new_mesh.unlink(2, cid, eid)

        small_new_eids = {eid: [] for eid in eids_2_remove}

        # --*-- Creating dict to track cell and edge lineage
        new_lineage = {degree: {} for degree in [1, 2]}

        # --*-- Adding a new edge in the middle of the cell
        big_new_eid = max_eid + 1
        new_mesh.add_wisp(1, big_new_eid)
        new_lineage[1][big_new_eid] = -1

        small_new_eid = big_new_eid
        for eid, pts_2_add in zip(eids_2_remove, vtx_pos_2_add):
            # --*-- Adding a vertex (in the middle of the old edge)
            new_vid = new_mesh.add_wisp(0)
            new_mesh.wisp_property('barycenter', 0)[new_vid] = pts_2_add
            new_mesh.link(1, big_new_eid, new_vid)

            # --*-- Unlinking the old edge to its other neighboring cells
            neighbor_cell_ids = list(old_mesh.regions(1, eid))
            for nghbr_cid in neighbor_cell_ids:
                if nghbr_cid != cid:
                    new_mesh.unlink(2, nghbr_cid, eid)

            # --*-- Unlinking the old edge and its end vertices
            old_edge_end_ids = list(old_mesh.borders(1, eid))

            for end_vid in old_edge_end_ids:
                new_mesh.unlink(1, eid, end_vid)

                # --*-- Adding a new edge between one end and the new vertex
                small_new_eid += 1
                new_mesh.add_wisp(1, small_new_eid)
                new_mesh.link(1, small_new_eid, end_vid)
                new_mesh.link(1, small_new_eid, new_vid)

                small_new_eids[eid].append(small_new_eid)
                new_lineage[1][small_new_eid] = eid

                # --*-- Linking the new edge to the neighboring cell
                for nghbr_cid in neighbor_cell_ids:
                    if nghbr_cid != cid:
                        new_mesh.link(2, nghbr_cid, small_new_eid)

        # --*-- Defining the two new daughter cells
        new_cid0 = max_cid + 1
        new_cid1 = max_cid + 2
        new_mesh.add_wisp(2, new_cid0)
        new_mesh.add_wisp(2, new_cid1)
        new_lineage[2][new_cid0] = cid
        new_lineage[2][new_cid1] = cid

        # --*-- Defining the new daughter hyper-cells
        new_mesh.add_wisp(3, new_cid0)
        new_mesh.add_wisp(3, new_cid1)
        new_mesh.link(3, new_cid0, new_cid0)
        new_mesh.link(3, new_cid1, new_cid1)

        # --*-- Remove the mother cell
        new_mesh.remove_wisp(2, cid)

        # --*-- Remove the old edges
        for eid in eids_2_remove:
            new_mesh.remove_wisp(1, eid)

        # --*-- building up the lists of edge ids around both new cells
        new_edge_ids_cells = [[big_new_eid],
                              [big_new_eid]]
        idx = 0

        # --*--*-- Checking that the list of ordered eids
        #          does not start with an edge to remove
        if old_oriented_edge_ids[0] in eids_2_remove:
            old_oriented_edge_ids = np.concatenate([old_oriented_edge_ids[1:],
                                                    old_oriented_edge_ids[:1]])

        for eid in old_oriented_edge_ids:
            if eid not in eids_2_remove:
                new_edge_ids_cells[idx].append(eid)

            else:
                new_eid0, new_eid1 = small_new_eids[eid]
                if (new_edge_ids_cells[idx][-1]
                        in new_mesh.border_neighbors(1, new_eid0)):
                    new_edge_ids_cells[idx].append(new_eid0)
                    new_edge_ids_cells[(idx + 1) % 2].append(new_eid1)
                else:
                    new_edge_ids_cells[idx].append(new_eid1)
                    new_edge_ids_cells[(idx + 1) % 2].append(new_eid0)

                idx += 1
                idx %= 2

        # --*-- Linking the daughter cells to their edges
        for cid, new_edge_ids_cells in zip([new_cid0, new_cid1],
                                           new_edge_ids_cells):
            for eid in new_edge_ids_cells:
                new_mesh.link(2, cid, eid)

        # --*-- Defining the cells and edges lineage
        update_lineage(new_mesh, old_mesh, new_lineage)

        # --*-- Transmitting the varios cell & edge properties
        #       from the old to the new mesh.
        inherite_properties(new_mesh, old_mesh, loading_coeff)

    return non_dividing_cids


def update_lineage(new_mesh, old_mesh, new_lineage):
    """Update the lineage on the new divided mesh.

    Parameters
    ----------
    new_mesh : PropertyTopomesh
        The new structure containing the daughter cells and edges.
    old_mesh : PropertyTopomesh
        The previous versions of the structure.
    new_lineage : Dict(Dict(Int))
        - keys : Int. degrees of the wisps to consider (usually 1 and 2)
        - values : dict( keys : new wisps ids, values : mother wisps ids)

    Returns
    -------
    None

    """

    for degree in new_lineage.keys():
        mother_id = {}
        for new_wid in new_mesh.wisps(degree):
            if new_wid in old_mesh.wisps(degree):
                mother_id[new_wid] = new_wid
            elif new_wid in new_lineage[degree].keys():
                mother_id[new_wid] = new_lineage[degree][new_wid]
            else:
                logging.warning('Elment #' + str(new_wid) + ' of degree '
                                + str(degree) + ' has no mother.')

        new_mesh.update_wisp_property('lineage_mother_id', degree, mother_id)


def inherite_properties(new_mesh, old_mesh, loading_coeff=.1):
    """Transmits a property from a mother mesh to a daughter one.

    Parameters
    ----------
    new_mesh : PropertyTopomesh
        The new structure containing the daughter cells and edges.
    old_mesh : PropertyTopomesh
        The previous versions of the structure.
    loading_coeff : float
        Optional (default : .1). Should be between 0 and 1.
        Correspond to the putative intrinsic strain of the newly formed walls.

    Returns
    -------
    None.

    Note:
    -----
    the update_lineage method should be run prior to this one. Or, at least,
    the property 'lineage_mother_id' should be defined on the considered
    daughter mesh.

    """
    # -- Sorting properties according to their computability
    computable_topomesh_properties = {0: ['barycenter'],
                                      1: ['barycenter', 'length', 'borders',
                                          'vertices'],
                                      2: ['barycenter', 'vertices',
                                          'oriented_borders',
                                          'oriented_vertices',
                                          'area', 'edges']}

    heritable_properties = {0: [],
                            1: ['edge_type', 'cwll_stiffness'],
                            2: ['pressure', 'cmf_density',
                                'cmf_relative_stiffness', 'cmf_resting_length',
                                'cmf_orientation', 'stress_tensor',
                                'growth_tensor']}

    mechanical_model_properties = {0: ['displacement'],
                                   1: ['resting_length',
                                       'edge_stretch',
                                       'edge_strain'],
                                   2: []}

    # --*-- Asserting that all properties are being taken care of
    for degree in range(3):
        for prop_name in new_mesh.wisp_properties(degree).keys():
            if (prop_name not in computable_topomesh_properties[degree]
                and prop_name not in heritable_properties[degree]
                    and prop_name not in mechanical_model_properties[degree]):

                assert prop_name in ['lineage_mother_id',
                                     'previous_barycenter',
                                     'previous_length',
                                     'previous_area']

    # --*-- Useful for latter
    cwll_stfnss_values = list(old_mesh.wisp_property('cwll_stiffness',
                                                     1).values())

    # -- Computing topomesh related properties
    for degree, property_names in computable_topomesh_properties.items():
        for prop_name in property_names:
            compute_topomesh_property(new_mesh, prop_name, degree)

    # --*-- Personal pass on the vertices orientation
    orienting_vertices(new_mesh)

    # -- Passing heritable properties from the old mesh to the new one
    for degree, property_names in heritable_properties.items():
        if degree > 0:
            mother_wid = new_mesh.wisp_property('lineage_mother_id', degree)

        for prop_name in property_names:
            if prop_name in old_mesh.wisp_properties(degree).keys():
                old_property = old_mesh.wisp_property(prop_name, degree)

                new_property = {}

                for wid in new_mesh.wisps(degree):

                    # --*-- The wid was already in the old_mesh
                    if wid in old_mesh.wisps(degree):
                        new_property[wid] = old_property[wid]

                    # --*-- The wid is new but has a mother in the old_mesh

                    elif mother_wid[wid] > -1:
                        new_property[wid] = old_property[mother_wid[wid]]

                    # --*-- The wid is new and created ex nihilo
                    #       (=new edge between 2 new cells -> only 2
                    #        possible properties: edge_type or cwll_stiffness)
                    elif (mother_wid[wid] == -1 and degree == 1):
                        if new_mesh.nb_regions(1, wid) == 1:
                            if prop_name == 'edge_type':
                                new_property[wid] = 'outer'

                            elif prop_name == 'cwll_stiffness':
                                new_property[wid] = np.max(cwll_stfnss_values)

                            else:
                                logging.warning('Undefined property on edges: '
                                                + prop_name)
                        else:
                            if prop_name == 'edge_type':
                                new_property[wid] = 'inner'

                            elif prop_name == 'cwll_stiffness':
                                new_property[wid] = np.min(cwll_stfnss_values)

                            else:
                                logging.warning('Undefined property on edges: '
                                                + prop_name)
                    else:
                        logging.warning('Lineage problem with element #'
                                        + str(wid) + ' (degree ' + str(degree)
                                        + ') about property: ' + prop_name)

                # --*-- Recording the new property on the new mesh
                new_mesh.update_wisp_property(prop_name, degree, new_property)

    # -- Passing the mechanical model properties (displacement and edge_stretch)

    if 'previous_barycenter' in old_mesh.wisp_properties(0).keys():
        old_prev_bary = old_mesh.wisp_property('previous_barycenter', 0)
        new_prev_bary = {}

        for vid in new_mesh.wisps(0):
            if vid in old_mesh.wisps(0):
                new_prev_bary[vid] = old_prev_bary[vid]

            else:
                # -- Getting the ids of the neighboring vertices in the old mesh
                new_neighb_vids = set(new_mesh.region_neighbors(0, vid))
                all_old_vids    = set(old_mesh.wisps(0))
                old_neighb_vids = list(new_neighb_vids & all_old_vids)

                assert len(old_neighb_vids)==2

                # -- Computing the weighted average of the previous barycenters
                vtx_pos = new_mesh.wisp_property('barycenter', 0)[vid]

                neighb_vtx_pos = new_mesh.wisp_property('barycenter', 0).values(old_neighb_vids)
                edge_lngth     = lng.norm(neighb_vtx_pos[1] -
                                          neighb_vtx_pos[0])

                interpol_prev_bary = np.zeros(3)
                for nvid, nvtx_pos in zip(old_neighb_vids, neighb_vtx_pos):
                    interpol_coef       = lng.norm(vtx_pos -
                                                  nvtx_pos) / edge_lngth
                    interpol_prev_bary += interpol_coef * old_prev_bary[nvid]

                new_prev_bary[vid] = interpol_prev_bary

        new_mesh.update_wisp_property('previous_barycenter', 0, new_prev_bary)


    if 'edge_stretch' in old_mesh.wisp_properties(1).keys():
        mother_eid = new_mesh.wisp_property('lineage_mother_id', 1)
        old_stretch = old_mesh.wisp_property('edge_stretch', 1)
        new_stretch = {}

        for eid in new_mesh.wisps(1):
            if eid in old_mesh.wisps(1):
                new_stretch[eid] = old_stretch[eid]

            elif mother_eid[eid]>-1:
                new_stretch[eid] = old_stretch[mother_eid[eid]]

            else:
                new_stretch[eid] = 0

        new_mesh.update_wisp_property('edge_stretch', 1, new_stretch)


    if 'edge_strain' in old_mesh.wisp_properties(1).keys():
        mother_eid = new_mesh.wisp_property('lineage_mother_id', 1)
        old_strain = old_mesh.wisp_property('edge_strain', 1)
        new_strain = {}

        for eid in new_mesh.wisps(1):
            if eid in old_mesh.wisps(1):
                new_strain[eid] = old_strain[eid]

            elif mother_eid[eid]>-1:
                new_strain[eid] = old_strain[mother_eid[eid]]

            else:
                new_strain[eid] = 0

        new_mesh.update_wisp_property('edge_strain', 1, new_strain)


    if 'resting_length' in old_mesh.wisp_properties(1).keys():
        mother_eid  = new_mesh.wisp_property('lineage_mother_id', 1)
        old_rst_len = old_mesh.wisp_property('resting_length', 1)
        old_cur_len = old_mesh.wisp_property('length', 1)
        new_cur_len = new_mesh.wisp_property('length', 1)
        new_rst_len = {}

        for eid in new_mesh.wisps(1):
            if eid in old_mesh.wisps(1):
                new_rst_len[eid] = old_rst_len[eid]

            elif mother_eid[eid]>-1:
                new_rst_len[eid] = new_cur_len[eid] * (old_rst_len[mother_eid[eid]]
                                                    / old_cur_len[mother_eid[eid]])

            else:
                new_rst_len[eid] = (1. - loading_coeff) * new_cur_len[eid]

        new_mesh.update_wisp_property('resting_length', 1, new_rst_len)

    # -- WARNING: Maybe this variable should not be stored as a property in the first place.
    #             It seems we need it l.666 of mechanics.py to compute some stuff.
    #             Future me will have to check that !
    if 'previous_length' in old_mesh.wisp_properties(1).keys():
        mother_eid   = new_mesh.wisp_property('lineage_mother_id', 1)
        old_prev_len = old_mesh.wisp_property('previous_length', 1)
        old_cur_len  = old_mesh.wisp_property('length', 1)
        new_cur_len  = new_mesh.wisp_property('length', 1)
        new_prev_len = {}

        for eid in new_mesh.wisps(1):
            if eid in old_mesh.wisps(1):
                new_prev_len[eid] = old_prev_len[eid]

            elif mother_eid[eid]>-1:
                new_prev_len[eid] = new_cur_len[eid] * (old_prev_len[mother_eid[eid]]
                                                    / old_cur_len[mother_eid[eid]])

            else:
                new_prev_len[eid] = 0

        new_mesh.update_wisp_property('previous_length', 1, new_prev_len)


    # -- WARNING: This is a patch for I don't remember the use of this variable !!!!
    # --          I don't know if it is really needed and I am in a hurry right now....
    if 'previous_area' in old_mesh.wisp_properties(2).keys():
        mother_eid    = new_mesh.wisp_property('lineage_mother_id', 2)
        old_prev_area = old_mesh.wisp_property('previous_area', 2)
        new_prev_area = {}

        for wid in new_mesh.wisps(2):
            if wid in old_mesh.wisps(2):
                new_prev_area[wid] = old_prev_area[wid]

            else:
                new_prev_area[wid] = 0

        new_mesh.update_wisp_property('previous_area', 2, new_prev_area)


def generate_division_simulation_log(mesh):
    """Generates logs from the division simulation outputs.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we are working on.

    Returns
    -------
    None.

    """
    logging.info(' |   * New tissue statistics: ')
    for name, degree in zip(['Vertices', 'Edges', 'Cells'], range(3)):
        logging.info(' |       - ' + name
                     + ' number: ' + str(mesh.nb_wisps(degree)))


# division.py ends here.
