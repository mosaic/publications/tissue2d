# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T09:08:13+01:00
# @Email:  olivier.ali@inria.fr
# @Filename: __init__.py
# @Last modified by:   oali
# @Last modified time:

from .mechanics import MechanicalModel
