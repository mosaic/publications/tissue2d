# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:


__all__ = ['StiffeningModel']


import random
import logging
import numpy as np
import numpy.linalg as lng

from cellcomplex.utils import array_dict
from tissue2D.utils.tools import max_eigvect, norm, hill_function, compute_epidermis


# ------------------------------------------------------------------------------
# Stiffening model class
# ------------------------------------------------------------------------------
class StiffeningModel():
    """growth model for 2D propertyTopomesh.

    Parameters
    ----------


    Attributes
    ----------
    _parameters : type
        Description of attribute `_parameters`.
    mesh : type
        Description of attribute `mesh`.

    """

    def __init__(self):
        # -- Simuation Parameters
        self._simulation_type = 'stiffening'

        # -- Stiffening parameters
        self._cwll_stiffness_ratio = None
        self._stiffening_rate = None
        self._stiffening_method = None
        self._edge_type_2_stiffen = None
        self._hill_threshold = None
        self._hill_exponent = None
        self._hill_amplitude = None
        self._cwll_memory = None

        self._mesh = None

    def set_mesh(self, mesh):
        self._mesh = mesh

    def set_parameters(self,
                       stiffening_rate=.5,
                       cwll_stiffness_ratio=3.,
                       stiffening_method='along_max_stress',
                       edge_type_2_stiffen='all',
                       hill_threshold=.5,
                       hill_exponent=5,
                       hill_amplitude=1.,
                       cwll_memory=False):

        self._cwll_stiffness_ratio = cwll_stiffness_ratio
        self._stiffening_rate = stiffening_rate
        self._stiffening_method = stiffening_method
        self._edge_type_2_stiffen = edge_type_2_stiffen
        self._hill_threshold = hill_threshold
        self._hill_exponent = hill_exponent
        self._hill_amplitude = hill_amplitude
        self._cwll_memory = cwll_memory

    def set_log(self):
        logging.info(' +-> ' + self._simulation_type)

        for param_name, param_vals in {'Stiffening rate':
                                       str(self._stiffening_rate)[:5],
                                       'Stiffening method':
                                       self._stiffening_method,
                                       'Edges to stiffen':
                                       self._edge_type_2_stiffen,
                                       'Hill function threshold':
                                       self._hill_threshold,
                                       'Hill function exponent':
                                       self._hill_exponent,
                                       'Hill function amplitude':
                                       self._hill_amplitude,
                                       'Cell wall memory':
                                       self._cwll_memory}.items():
            logging.info(' |   * ' + param_name + ': ' + str(param_vals))

    def reset(self,
              mesh,
              stiffening_rate=.5,
              cwll_stiffness_ratio=3.,
              stiffening_method='along_max_stress',
              edge_type_2_stiffen='all',
              hill_threshold=.5,
              hill_exponent=5,
              hill_amplitude=1.,
              cwll_memory=False):

        self.set_mesh(mesh)
        self.set_parameter(stiffening_rate,
                           cwll_stiffness_ratio,
                           stiffening_method,
                           edge_type_2_stiffen,
                           hill_threshold,
                           hill_exponent,
                           hill_amplitude,
                           cwll_memory)

    def step(self, time, dt):
        logging.info(' |')
        logging.info(' +-> ' + self._simulation_type + ' Step # ' + str(time))

        new_cwll_stiff = compute_whole_tissue_stiffening(self._mesh,
                            stiffening_coefficient=self._stiffening_rate * dt,
                            cwll_stiffness_ratio=self._cwll_stiffness_ratio,
                            stiffening_method=self._stiffening_method,
                            edge_type_2_stiffen=self._edge_type_2_stiffen,
                            hill_threshold=self._hill_threshold,
                            hill_exponent=self._hill_exponent,
                            hill_amplitude=self._hill_amplitude,
                            cwll_memory=self._cwll_memory)

        self._mesh.update_wisp_property('cwll_stiffness', 1, new_cwll_stiff)

        generate_stiffening_simulation_log(self._mesh)

    def run(self, time, dt=1):
        self.step(time, dt)


# ------------------------------------------------------------------------------
# Methods to compute the new resting configuration of the mesh
# ------------------------------------------------------------------------------
def compute_whole_tissue_stiffening(mesh,
                                    stiffening_coefficient=.1,
                                    cwll_stiffness_ratio=3.,
                                    stiffening_method='along_max_stress',
                                    edge_type_2_stiffen='all',
                                    hill_threshold=.5,
                                    hill_exponent=5,
                                    hill_amplitude=1.,
                                    cwll_memory=False):
    """Computes the stiffening of all edges within a mesh, given a rule.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we are working on.
    stiffening_coefficient : float.
        Optional (default : .1). An adimensional stiffening coefficient
    cwll_stiffness_ratio : float.
        Optional (default : 3.). If no history is assumed in the stiffening.
    stiffening_method : str
        Optional (default : 'along_max_stress'). Determines along which
        direction the stiffening process is maximal.
    edge_type_2_stiffen : str
        Optional (default : 'all'). Should be chosen among
        ['all', 'inner', 'outer']. Select the edge on which the stiffening
        should be apply.
    hill_threshold : float
        Optional (default : .5). Relative force value giving the mean between
        the min and max value of the hill function.
    hill_exponent : int
        Optional (default : 5). Value of the exponent used
        in the hill function.
    hill_amplitude : float
        Optional (default : 3/.9). Difference between the min and max of the
        hill function.
    cwll_memory : bool
        Optional (default : False). If True the stiffness computed at each
        step is based on the stiffness of the previous stemp. Otherwize, it
        is based on the initial value.

    Note
    ----
    available methods :
        - along_max_stress : the direction associated with the stress largest
                             eigenvalue
        - radial : the radial direction of the tissue. The feedback intensity
                   is proportional to the stress amplitude in this direction.
        - random : the orienation is arbitrarily chosen.
        - along_edge : the elastic force borne by each edge.

    Returns
    -------
    new_cwll_stiffness : dict(int, float)
        - keys : edge indices
        - values : new stiffness value.

    """

    new_cwll_stiffness = {}

    assert 'stress_tensor' in mesh.wisp_properties(2).keys()
    print(f"l.206 - stiffening.py: edge_type_2_stiffen: {edge_type_2_stiffen}")
    # -- edge to consider
    edge_type = mesh.wisp_property('edge_type', 1)

    if edge_type_2_stiffen == 'all':
        eids_to_consider = list(mesh.wisps(1))
    else:
        eids_to_consider = [eid for eid, type in edge_type.items()
                            if type == edge_type_2_stiffen]

    # -- Useful quantities
    edge_stffns = mesh.wisp_property('cwll_stiffness', 1)
    edge_stain = mesh.wisp_property('edge_strain', 1)
    strss_tnsrs = mesh.wisp_property('stress_tensor', 2)
    vrtx_posi = mesh.wisp_property('barycenter', 0)
    tiss_cntr = np.mean(list(vrtx_posi.values()), axis=0)[:2]
    edge_forces = np.array([edge_stffns[eid] * strain
                            for eid, strain in edge_stain.items()
                            if eid in eids_to_consider])
    cell_types = array_dict(compute_epidermis(mesh))

    # -- Stiffening loop over the edges.
    for eid in mesh.wisps(1):
        # -- Applying the stiffening to a selection of edges
        if eid in eids_to_consider:
            # -- Defining the edge direction vector and projector
            vids_ends = list(mesh.borders(1, eid))
            posi_ends = mesh.wisp_property('barycenter',
                                           0).values(vids_ends)
            edge_vect = posi_ends[1, :2] - posi_ends[0, :2]
            edge_vect /= lng.norm(edge_vect)
            edge_proj = np.tensordot(edge_vect, edge_vect, axes=0)

            # --*-- Computing the averaged stress tensor for an edge
            neighb_cids = list(mesh.regions(1, eid))

            edge_strss = np.mean(strss_tnsrs.values(neighb_cids), axis=0)

            # --*-- Computing the stiffening direction
            if stiffening_method == 'along_max_stress':
                strss_ampl = np.tensordot(edge_strss, edge_proj, axes=2)

                ampli = hill_function(strss_ampl, hill_threshold,
                                      hill_exponent, hill_amplitude)
                stffn_vect = ampli * edge_vect

            elif stiffening_method in ['random', 'radial']:
                if stiffening_method == 'random':
                    rndm_angle = random.uniform(0, np.pi)
                    stffn_vect = np.array([np.cos(rndm_angle),
                                           np.sin(rndm_angle)])

                elif stiffening_method == 'radial':
                    # --*-- Defining the barycenter of an edge...
                    vids_ends = list(mesh.borders(1, eid))
                    posi_ends = mesh.wisp_property('barycenter',
                                                   0).values(vids_ends)
                    edge_bary = np.mean(posi_ends, axis=0)[:2]

                    # --*-- ... And the direction between the edge & the center
                    stffn_vect = edge_bary - tiss_cntr
                    stffn_vect /= lng.norm(stffn_vect)

                # -- Getting the stress value in this edge - center direction
                stffn_proj = np.tensordot(stffn_vect, stffn_vect, axes=0)
                strss_ampl = np.tensordot(edge_strss, stffn_proj, axes=2)
                strss_norm = norm(edge_strss)

                stffn_vect *= strss_ampl / strss_norm

            elif stiffening_method == 'along_edge':
                # -- Computing the elastic force borne by the edge
                norm_force = edge_stffns[eid] * edge_stain[eid]
                # norm_force = ((norm_force - edge_forces.min())
                #               / (edge_forces.max() - edge_forces.min()))
                stffn_vect = hill_function(norm_force,
                                           hill_threshold,
                                           hill_exponent,
                                           hill_amplitude) * edge_vect

            elif stiffening_method == 'out_radial_in_along_max_stress':
                if set(cell_types.values(neighb_cids)) == {0, 1}:
                    # -- The feedback on L1L2 walls comes only from the L2...
                    neighb_cids = [cid for cid in list(mesh.regions(1, eid))
                                   if cell_types[cid] == 0]
                    edge_strss = np.mean(strss_tnsrs.values(neighb_cids),
                                         axis=0)

                    strss_ampl = np.tensordot(edge_strss, edge_proj, axes=2)

                    ampli = hill_function(strss_ampl, hill_threshold,
                                          hill_exponent, hill_amplitude)

                elif list(cell_types.values(neighb_cids)) == [1]:
                    ampli = 0
                elif list(cell_types.values(neighb_cids)) == [1, 1]:
                    ampli = hill_function(5*hill_threshold, hill_threshold,
                                          hill_exponent, 3*hill_amplitude)

                else:
                    strss_ampl = np.tensordot(edge_strss, edge_proj, axes=2)

                    ampli = hill_function(strss_ampl, hill_threshold,
                                          hill_exponent, hill_amplitude)

                stffn_vect = ampli * edge_vect

            # --*-- Updating the stiffness dictionary
            new_cwll_stiffness[eid] = sitffen_one_wall(mesh,
                                                       eid,
                                                       stffn_vect,
                                                       stiffening_coefficient=stiffening_coefficient,
                                                       cwll_stiffness_ratio=cwll_stiffness_ratio,
                                                       cwll_memory=cwll_memory)

            if new_cwll_stiffness[eid] > 10:
                print(f"l.322 - stiffness.py: norm_force: {norm_force}")
                print(f"l.322 - stiffness.py: hill_threshold: {hill_threshold}")
                print(f"l.322 - stiffness.py: hill_exponent: {hill_exponent}")
                print(f"l.322 - stiffness.py: hill_amplitude: {hill_amplitude}")
                print(f"l.322 - stiffness.py: norm(edge_vect): {lng.norm(edge_vect)}")

        else:
            new_cwll_stiffness[eid] = edge_stffns[eid]

        logging.debug('edge id: ' + str(eid) + ', edge type: '
                      + str(edge_type[eid]) + ', stiffness (old, new): '
                      + '({:.2e}, '.format(edge_stffns[eid])
                      + '{:.2e})'.format(new_cwll_stiffness[eid]))

    return new_cwll_stiffness


def sitffen_one_wall(mesh,
                     eid,
                     stiffening_direction,
                     stiffening_coefficient=.1,
                     cwll_stiffness_ratio=3.,
                     cwll_memory=False):
    """Computes the stiffening of one edge.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we are working on.
    eid : int
        The index of the edge of interest in the considered mesh.
    stiffening_direction : ndarray(float)
        2D (2 coeffs only) vector giving the direction to
        stiffen the most. Its norm can encode for the intensity of the
        feedback mechanism. (can only be positive at this point).
    stiffening_coefficient : Float.
        Optional (default : .1). An adimensional stiffening coefficient
    cwll_stiffness_ratio : Float.
        Optional (default : 3.). If no history is assumed in the stiffening.
    cwll_memory : bool
        Optional (default : False). If True the stiffness computed at each
        step is based on the stiffness of the previous stemp. Otherwize, it
        is based on the initial value.

    Note
    ----
    due to the absolute value around the scalar product, the feedback at this
    point can only be positive.

    Returns
    -------
    type
        Description of returned object.

    """

    # -- Defining the old stiffness value
    # --*-- If we want some history of the walls:
    if cwll_memory:
        old_cwll_stiff = mesh.wisp_property('cwll_stiffness', 1)[eid]

    else:
        # --*-- If we do not want history of the walls:
        edge_type = mesh.wisp_property('edge_type', 1)
        if edge_type[eid] == 'inner':
            old_cwll_stiff = 1

        elif edge_type[eid] == 'outer':
            old_cwll_stiff = cwll_stiffness_ratio
        else:
            print('l.130 - stiffening - WARNING weird edge type for eid#', eid)

    # -- Defining the edge direction
    vids_ends = list(mesh.borders(1, eid))
    posi_ends = mesh.wisp_property('barycenter', 0).values(vids_ends)
    edge_vect = posi_ends[1, :2] - posi_ends[0, :2]
    edge_vect /= lng.norm(edge_vect)

    new_cwll_stiff = old_cwll_stiff * (1 + stiffening_coefficient
                                       * np.abs(np.dot(edge_vect,
                                                       stiffening_direction)))
    if new_cwll_stiff > 10:
        print(f"l.403 - stiffening.py: stiffening_coefficient: {stiffening_coefficient}")
        print(f"l.403 - stiffening.py: norm(edge_vect): {lng.norm(edge_vect)}")
        print(f"l.403 - stiffening.py: norm(stiffening_direction): {lng.norm(stiffening_direction)}")
        print(f"l.403 - stiffening.py: new cwll stiffness: {new_cwll_stiff}")

    return new_cwll_stiff


def generate_stiffening_simulation_log(mesh):
    """Generates logs from the stiffening simulation outputs.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we are working on.

    Returns
    -------
    None.

    """
    cwll_stiff = {}
    for cwll_type in ['outer', 'inner']:
        eids = [eid for eid, type
                in mesh.wisp_property('edge_type', 1).items()
                if type == cwll_type]

        cwll_stiff[cwll_type] = mesh.wisp_property('cwll_stiffness',
                                                   1).values(eids)

    logging.info(' |   * Cell wall stiffness (min, max): ')
    for name, stiff in cwll_stiff.items():
        logging.info(' |       - ' + name + ' edges: '
                     + '({:.2e}, '.format(stiff.min())
                     + '{:.2e})'.format(stiff.max()))


# stiffening.py ends here.
