# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:


__all__ = ['GrowthModel']

import logging
import numpy as np
import numpy.linalg as lng

from tissue2D.utils.tools import (compute_shape_descriptor, hill_function, sym,
                                  anisotropy, intensity)
from tissue2D.utils.simulations_recording import save_data


# ------------------------------------------------------------------------------
# Growth model class
# ------------------------------------------------------------------------------
class GrowthModel():
    """growth model for 2D propertyTopomesh.

    Parameters
    ----------


    Attributes
    ----------
    _parameters : type
        Description of attribute `_parameters`.
    mesh : type
        Description of attribute `mesh`.

    """

    def __init__(self):
        # -- Simuation Parameters
        self._simulation_type = 'growth'
        self._t_min = None
        self._t_max = None
        self._dt = None
        self._save_simulation = None
        self._recording_path = None

        # -- Input parameters
        self._growth_type = None
        self._growth_rate = None
        self._growth_threshold = None

        # -- Initial mesh
        self._mesh = None
        self._mesh_name = None
        self._surface_initial = None

    def set_mesh(self, mesh):
        self._mesh = mesh

    def set_parameters(self,
                       growth_rate=.5,
                       growth_threshold=0,
                       growth_type='hill',
                       hill_exponent=5,
                       hill_amplitude=1.,
                       save_simulation=False,
                       recording_path=None):

        self._growth_rate = growth_rate
        self._growth_threshold = growth_threshold
        self._growth_type = growth_type
        self._hill_exponent = hill_exponent
        self._hill_amplitude = hill_amplitude
        self._save_simulation = save_simulation
        self._recording_path = recording_path

    def set_log(self):
        logging.info(' +-> ' + self._simulation_type)

        for param_name, param_vals in {'Growth law': self._growth_type,
                                       'Growth rate': self._growth_rate,
                                       'Growth threshold':
                                       self._growth_threshold}.items():
            logging.info(' |   * ' + param_name + ': ' + str(param_vals))

        if self._growth_type == 'hill':
            for param_name, param_vals in {'Hill function exponent':
                                           self._hill_exponent,
                                           'Hill function amplitude':
                                           self._hill_amplitude}.items():
                logging.info(' |   * ' + param_name + ': ' + str(param_vals))

    def reset(self,
              mesh,
              growth_rate=.5,
              growth_threshold=0,
              growth_type='linear',
              hill_exponent=5,
              hill_amplitude=1.):
        self.set_mesh(mesh)
        self.set_parameters(growth_rate,
                            growth_threshold,
                            growth_type,
                            hill_exponent,
                            hill_amplitude)

    def step(self, time, dt=1):
        logging.info(' |')
        logging.info(' +-> ' + self._simulation_type + ' Step # ' + str(time))

        grow_property_topomesh(self._mesh,
                               self._growth_rate * dt,
                               self._growth_threshold,
                               self._growth_type,
                               self._hill_exponent,
                               self._hill_amplitude)

        compute_growth_tensor(self._mesh)

        logging.debug('t_min: ' + str(self._t_min))

        # -- Compute the first surface area for reference.
        if time == self._t_min:
            pos = self._mesh.wisp_property('barycenter', 0).values()
            width = np.max(pos[:, 0]) - np.min(pos[:, 0])
            height = np.max(pos[:, 1]) - np.min(pos[:, 1])
            self._surface_initial = np.pi * width * height / 4
            logging.debug('Initial surface: ' + str(self._surface_initial))

        generate_growth_simulation_log(self._mesh, time, self._surface_initial)

    def run(self, t_min=0, t_max=10, dt=1):
        self._t_min = t_min
        self._t_max = t_max
        self._dt = dt

        for t in np.linspace(t_min, t_max, (t_max - t_min)/dt + 1):
            self.step(t, dt)

            if self._save_simulation:
                print('   |')
                print('   +-> Recording step #', t)
                save_data(self, t, folder_path=self._recording_path)


# ------------------------------------------------------------------------------
# Methods to compute the new resting configuration of the mesh
# ------------------------------------------------------------------------------

def compute_new_resting_lengths(mesh,
                                growth_factor,
                                growth_threshold,
                                growth_type='linear',
                                hill_exponent=5,
                                hill_amplitude=1.):
    """Computes the new resting lengths of each edge of the considered mesh.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to grow.
    growth_factor : float
        The expansion factor of the wall, should be between 0 (no growth)
        and 1 (the current length becomes the new reference).
    growth_threshold : float
        The strain value above which growth happens.
        Should be be between 0 and 1.
    growth_type : str
        Optional (default : 'linear'). Choice of the growth law. 'linear'
        corresponds to the usual ramp function. 'hill' corresponds to the hill
        function version.
    hill_exponent : int
        Opitional (default : 5). The exponent to use if the hill function is
        chosen as the growth type.
    hill_amplitude : Float
        Optional (default : .6). The difference between the max and min values
        of the hill function.

    Returns
    -------
    new_cwll_rst_lng : Dict(float)
        - keys : Int. index of the edges
        - values : float. the new value of the resting length.

    Note
    ----
    The property 'edge_strain' must be defined on the wisps(1) of the
    considered mesh in order to make this function work.
    """
    eids = list(mesh.wisps(1))
    cwll_rstlng = mesh.wisp_property('resting_length', 1).values(eids)
    cwll_strain = mesh.wisp_property('edge_strain', 1).values(eids)
    #
    # # -- Defining the L1-anticlinal walls
    # new_type = {eid: type for eid, type
    #             in mesh.wisp_property('edge_type', 1).items()}
    #
    # for eid, type in mesh.wisp_property('edge_type', 1).items():
    #     if type == 'outer':
    #         neids = list(mesh.border_neighbors(1, eid))
    #
    #         for neid in neids:
    #             if mesh.wisp_property('edge_type', 1)[neid] == 'inner':
    #                 new_type[neid] = 'L1anti'
    #
    # mesh.update_wisp_property('edge_type', 1, new_type)

    # -- Restricting growth of the L1anticlinal walls
    no_growing_eids = [eid for eid, type
                       in mesh.wisp_property('edge_type', 1).items()
                       if type == 'L1anti']

    # -- Updating the resting length
    new_cwll_rstlng = {}
    for eid, rst_lng, strain in zip(eids, cwll_rstlng, cwll_strain):
        new_cwll_rstlng[eid] = rst_lng

        if eid not in no_growing_eids:
            if growth_type == 'linear':
                if strain > growth_threshold:
                    new_cwll_rstlng[eid] += growth_factor * rst_lng * strain

            elif growth_type == 'hill':
                new_cwll_rstlng[eid] += growth_factor * rst_lng \
                                       * hill_function(strain,
                                                       growth_threshold,
                                                       hill_exponent,
                                                       hill_amplitude)

    return new_cwll_rstlng


def grow_property_topomesh(mesh,
                           growth_factor=.5,
                           growth_threshold=0,
                           growth_type='linear',
                           hill_exponent=5,
                           hill_amplitude=1.):
    """Updates a mesh given a displacement field and growth factor.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to grow.
    growth_factor : float
        Optional (default : .5). The expansion factor of the wall,
        should be between 0 (no growth) and 1
        (the current length becomes the new reference).
    growth_threshold : float
        Optional (default : 0). The strain value above which growth happens.
        Should be be between 0 and 1.
    growth_type : str
        Optional (default : 'linear'). Choice of the growth law. 'linear'
        corresponds to the usual ramp function. 'hill' corresponds to the hill
        function version.
    hill_exponent : int
        Opitional (default : 5). The exponent to use if the hill function is
        chosen as the growth type.
    hill_amplitude : Float
        Optional (default : .6). The difference between the max and min values
        of the hill function.

    Returns
    -------
    None.

    Note
    ----
    The property 'edge_strain' must be defined on the wisps(1) of the
    considered mesh in order to make this function work.
    """
    # -- Updating the resting length of the edges (in the edge-growth case)
    new_resting_lengths = compute_new_resting_lengths(mesh,
                                                      growth_factor,
                                                      growth_threshold,
                                                      growth_type,
                                                      hill_exponent,
                                                      hill_amplitude)

    mesh.update_wisp_property('resting_length', 1, new_resting_lengths)

    logging.debug('Property_topomesh updated')


def compute_growth_tensor(mesh):
    """Adds to the wisps(2) a growth tensor property.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to grow.

    Returns
    -------
    None.

    Notes
    -----
    This function need the property 'displacement' to be added to the
    wisps(0) of the considered mesh.

    """
    growth_tensor = {}
    for cid in mesh.wisps(2):
        identity = np.eye(2)

        cur_shape = compute_shape_descriptor(mesh, cid)
        ref_shape = compute_shape_descriptor(mesh, cid, current=False)

        inv_ref_shape = lng.inv(ref_shape)

        growth_tensor[cid] = sym(np.dot(cur_shape, inv_ref_shape)) - identity

    mesh.update_wisp_property('growth_tensor', 2, growth_tensor)


def generate_growth_simulation_log(mesh, time, initial_surface):
    """Generates logs from the growth simulation outputs.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we are working on.
    time : int
        The current time step.
    initial_surface : float
        The surface area of the tissue at the first time step.

    Returns
    -------
    None.

    """
    # -- Global shape of the tissue
    tissue_shape = compute_shape_descriptor(mesh)
    tissue_shape_aniso = anisotropy(tissue_shape)

    if tissue_shape_aniso != 1:
        tissue_aspect_ratio = np.sqrt((1 + tissue_shape_aniso)
                                      / (1 - tissue_shape_aniso))
    else:
        tissue_aspect_ratio = -100
        logging.warning('anisotropy of the whole tissue shape tensor egals 1')

    # --*-- Global extansion of the tissue
    pos = mesh.wisp_property('barycenter', 0).values()
    width = np.max(pos[:, 0]) - np.min(pos[:, 0])
    height = np.max(pos[:, 1]) - np.min(pos[:, 1])
    surf = np.pi * width * height / 4

    # -- Growth quantification
    growth_tensor = mesh.wisp_property('growth_tensor', 2).values()
    grth_inten = np.array([intensity(grw_tsr) for grw_tsr in growth_tensor])
    grth_aniso = np.array([anisotropy(grw_tsr) for grw_tsr in growth_tensor])

    # -- Logging post-simulation info
    logging.info(' |   * Tissue geometry:')
    logging.info(' |       - Width: ' + '{:.2e}'.format(width))
    logging.info(' |       - Height: ' + '{:.2e}'.format(height))
    logging.info(' |       - Aspect ratio (from vertices positions): '
                 + '{:.2e}'.format(width / height))
    logging.info(' |       - Aspect ratio (from shape tensor): '
                 + '{:.2e}'.format(tissue_aspect_ratio))
    logging.info(' |       - Relative surface (compared to initial time): '
                 + '{:.2e}'.format(surf / initial_surface))
    logging.info(' |   * Growth tensor (mean +/- std):')
    for name, mean_val, std_val in zip(['Intensity', 'Anisotropy'],
                                       [grth_inten.mean(), grth_aniso.mean()],
                                       [grth_inten.std(), grth_aniso.std()]):
        logging.info(' |       - ' + name + ': '
                     + '{:.2e}'.format(mean_val) + ' +/- '
                     + '{:.2e}'.format(std_val))


# growth.py ends here.
