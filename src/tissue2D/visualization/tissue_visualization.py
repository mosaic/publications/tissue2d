# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:
import os
import logging
import numpy as np
from time import time
import matplotlib.pylab as pl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
from collections.abc import Iterable

from cellcomplex.property_topomesh.utils.matplotlib_tools import mpl_draw_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property

from tissue2D.utils.tools import anisotropy, max_eigvect, min_eigvect


# -- Useful visualization paramters
VISU_PARAM_DEFAULT = {'back_ground_color': 'black',
                      'edge_color': 'r',
                      'image_aspect_ratio': 1,
                      'tensor_color': 'g',
                      'field_of_view': 'automatic',
                      'image_aspect_ratio': 1,
                      'cell_property': None,
                      'cell_colormap': 'viridis',
                      'cell_alpha': .5,
                      'edge_property': None,
                      'edge_property_2': None,
                      'edge_thickness': 2}

VISU_PARAM_DEFAULT_STACK = {'back_ground_color': 'black',
                            'edge_color': 'r',
                            'tensor_color': 'g',
                            'field_of_view': [-30, 30],
                            'cell_property': None,
                            'cell_colormap': 'viridis',
                            'cell_alpha': 0.,
                            'edge_property': None,
                            'edge_property_2': None,
                            'edge_thickness': 2}

SAVE_PATH_DEFAULT = '/Users/oali/Documents/Work/Research' +\
                    '/Devlp/tissue2D/data/results/'


def draw_tissue(meshes, visu_param=VISU_PARAM_DEFAULT,
                show_stress_orientation=False,
                show_recent_divisions=False,
                show_cell_and_edge_ids=False,
                fig=None,
                save_figure=False,
                saving_path=SAVE_PATH_DEFAULT):
    """Draws a mesh (or a list of mesh).

    Parameters
    ----------
    meshes : list(PropertyTopomesh)
        The tissues we want to plot.
    visu_param : Dict(str or float or int)
        Optional (default : VISU_PARAM_DEFAULT - defined above the method code)
        - keys : Str. Names of the various parameters.
        - values : Str or Float or Int. Values of the various parameters.
    show_stress_orientation: Bool.
            Optional (default : False). If true the main stress
            directions are ploted.
    show_recent_divisions: Bool.
            Optional (default : False). If true the newly added edge are
            plotted in a specific color.
    show_cell_and_edge_ids: Bool.
        Optional (default : False). If true the cell and edge ids
        are displayed on the drawn tissue.
    fig: matplotlib figure
        Optional (default : None). To add the figure to a bigger one.
    save_figure : Bool
        Optional (default : False). If true, the figure is recorded.
    saving_path : Str
        Opitional (default : a value given above). Where we want to
        record the snapshot.

    Returns
    -------
    None

    """
    # -- Parametrizing the visualization
    asp_rto = visu_param['image_aspect_ratio']

    if not fig:
        fig = plt.figure(facecolor=visu_param['back_ground_color'],
                         edgecolor='white')
    axes = fig.add_subplot(111)
    axes.patch.set_facecolor(visu_param['back_ground_color'])
    axes.xaxis.set_tick_params(color='white', labelcolor='white')
    axes.yaxis.set_tick_params(color='white', labelcolor='white')
    for spine in axes.spines.values():
        spine.set_color('white')
    axes.set_aspect('equal')

    if isinstance(meshes, dict):
        meshes = list(meshes.values())

    if not isinstance(meshes, Iterable):
        meshes = [meshes]

    for mesh in meshes:
        mpl_draw_topomesh(mesh,
                          fig,
                          property_name=visu_param['cell_property'],
                          property_degree=2,
                          colormap=visu_param['cell_colormap'],
                          alpha=visu_param['cell_alpha'],
                          cell_edges=True,
                          linewidth=10)

        draw_cell_walls(mesh,
                        cwll_property=visu_param['edge_property'],
                        cwll_property_2=visu_param['edge_property_2'],
                        thickness_coef=visu_param['edge_thickness'],
                        color=visu_param['edge_color'],
                        fig=fig)

        if show_stress_orientation:
            draw_tensor_main_directions(mesh,
                                        fig=fig,
                                        color=visu_param['tensor_color'])

        if show_recent_divisions:
            draw_recent_divisions(mesh, fig=fig)

        position_x = [pos[0] for pos in mesh.wisp_property('barycenter',
                                                           0).values()]
        mesh_width = np.max(position_x) - np.min(position_x)

        # -- Visualization of the cids and eids.
        if show_cell_and_edge_ids:
            for cid, pos in mesh.wisp_property('barycenter', 2).items():
                plt.text(pos[0],
                         pos[1],
                         cid,
                         color='w',
                         horizontalalignment='center',
                         verticalalignment='center',
                         zorder=10)

            for eid in mesh.wisps(1):
                bids = list(mesh.borders(1, eid))
                pos = mesh.wisp_property('barycenter',
                                         0).values(bids).mean(axis=0)

                plt.scatter(pos[0], pos[1], color='r', s=200)
                plt.text(pos[0],
                         pos[1],
                         eid,
                         color='w',
                         horizontalalignment='center',
                         verticalalignment='center',
                         zorder=10)

        # -- setting the field of view
        logging.debug('Starting the calculation of the field of view.')
        logging.debug('Field of view value: '+str(visu_param['field_of_view']))
        if visu_param['field_of_view'] == 'automatic':
            xint = [-(mesh_width/2 + 1), mesh_width/2 + 1]
            logging.debug('Automatic field_of_view used.')
        elif (visu_param['field_of_view'] != 'automatic'
              and not isinstance(visu_param['field_of_view'], Iterable)):
            xint = [-visu_param['field_of_view'], visu_param['field_of_view']]
        else:
            logging.debug('Custom field of view with scalar value: '
                          + str(visu_param['field_of_view']))
            xint = visu_param['field_of_view']

        fig.gca().set_xlim(xint)
        fig.gca().set_ylim([asp_rto * xint[0], asp_rto * xint[1]])
        fig.subplots_adjust(wspace=0, hspace=0)
        fig.tight_layout()
        fig.set_size_inches(10, asp_rto * 10)

    # -- Saving the figure
    if save_figure:
        if saving_path[-4:] == '.png':
            plt.savefig(saving_path, dpi=300)

        elif saving_path[-1] == '/':
            fig_name = str(int(time())) + '.png'
            plt.savefig(saving_path+fig_name, dpi=300)

    if not fig:
        fig.show()


def draw_vector_field(mesh, vector_field_name, fig=None):
    """Plots a vector field defined on the vertices of a given mesh.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to plot on.
    vector_field_name : str
        the vector field should be a property on the vertices of the considered
        mesh.
    fig : pyplot firgure
        The figure to plot in.

    Returns
    -------
    None

    """

    assert vector_field_name in mesh.wisp_properties(0).keys()

    if not fig:
        fig = plt.figure()
        mpl_draw_topomesh(mesh, fig)

    vids = list(mesh.wisps(0))
    pos_x = mesh.wisp_property('barycenter', 0).values(vids)[:, 0]
    pos_y = mesh.wisp_property('barycenter', 0).values(vids)[:, 1]

    vec_x = mesh.wisp_property(vector_field_name, 0).values(vids)[:, 0]
    vec_y = mesh.wisp_property(vector_field_name, 0).values(vids)[:, 1]

    fig.gca().quiver(pos_x, pos_y, vec_x, vec_y, scale_units='x')
    # fig.gca().set_xlim(-10, 10)
    # fig.gca().set_ylim(-10, 10)
    fig.show()


def draw_tensor_main_directions(mesh,
                                tensor_name='stress_tensor',
                                fig=None,
                                color='g'):
    """Display a crosshair representation of tensor eigen direction.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we want to display tensor on.
    tensor_name : Str
        Optional (default : 'stress_tensor'). The tensor quantity
        we want to visualize
    fig : pyplot graph
        Optional (default: None). The graph to pass on.
    color : pyplot color name
        Optional (default: 'g'). The color we want to use to see the tensors.

    Returns
    -------
    None.

    """
    if not fig:
        fig = plt.figure()
        # mpl_draw_topomesh(mesh, fig)

    if len(color) == 1:
        color_max = color
        color_min = color

    elif len(color) == 2:
        color_max = color[0]
        color_min = color[1]
    else:
        print('WARNING - Wrong number of colors.')

    # -- Checking that the needed properties do exist.
    assert tensor_name in mesh.wisp_properties(2).keys()

    # # -- In the case growth is implemented on the edges lenghts
    # compute_topomesh_property(mesh, 'barycenter', 2)

    # -- Building up the various vector fields to draw.
    cids = list(mesh.wisps(2))
    tsr = mesh.wisp_property(tensor_name, 2).values(cids)

    position = mesh.wisp_property('barycenter', 2).values(cids)

    max_vect = np.array(list(map(lambda x: (1 + anisotropy(x))
                                 / (1 - anisotropy(x))
                                 * max_eigvect(x), tsr)))
    min_vect = np.array(list(map(lambda x: min_eigvect(x), tsr)))

    # -- Generating the figure
    fig.gca().quiver(position[:, 0], position[:, 1],
                     max_vect[:, 0], max_vect[:, 1],
                     scale_units='x',
                     angles='uv',
                     scale=1,
                     color=color_max,
                     width=.004,
                     headaxislength=0,
                     headlength=0,
                     pivot='mid')

    fig.gca().quiver(position[:, 0], position[:, 1],
                     min_vect[:, 0], min_vect[:, 1],
                     scale_units='x',
                     angles='uv',
                     scale=1,
                     color=color_min,
                     width=.002,
                     headaxislength=0,
                     headlength=0,
                     pivot='mid')


def draw_tensor_directions_and_property(mesh,
                                        tensor_name,
                                        degree, property,
                                        colormap='inferno',
                                        color=['w', 'k'],
                                        alpha=1,
                                        fig=None,
                                        save_figure=False,
                                        save_path=None):
    """Display crosshair representation of tensor eigendirection on an heatmap.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we want to display tensor on.
    tensor_name : Str
        The tensor quantity we want to visualize. Note: The tensor should
        be recorded on the propertyTopomesh with a name following this
        convention: '****_tensor'.
    degree : int.
        The wisp degree the tensor is defined on.
    property : str
        Should be either 'intensity' or 'anisotropy'.
    fig : pyplot graph
        Optional (default: None). The graph to pass on.
    colormap : matplotlib colormap name
        Optional (default: 'inferno'). The colormap we want to use
        to display the scalar property.
    color : tuple of matplotlib color code
        Optional (default : ['w', 'k']). The color we want the crosshair.
        The first one will mark the direction associated with the largest
        eigenvalue.
    alpha : Float
        Optional (default : 1). The alpha value we want for the histogram.
    fig : matplotlib figure
        Optional (default : None). To include the histogram in a bigger figure.
    save_figure : bool
        Optional (default : False). Save the figure if true.
    save_path : str
        Optional (default : None). A custom path where to save the figure.

    Returns
    -------
    None.

    """
    if not fig:
        fig = plt.figure()

    # -- Defining the heatmaps boundaries
    vtx_pos = mesh.wisp_property('barycenter', 0).values()
    xlim_max = np.max(vtx_pos[:, 0]) + 1
    xlim_min = np.min(vtx_pos[:, 0]) - 1
    ylim_max = np.max(vtx_pos[:, 1]) + 1
    ylim_min = np.min(vtx_pos[:, 1]) - 1

    # -- Plotting the heatmap
    property_name = tensor_name[:-7] + '_' + property

    mpl_draw_topomesh(mesh,
                      fig,
                      property_name=property_name,
                      property_degree=degree,
                      colormap=colormap,
                      alpha=alpha)

    # -- Plotting the tensor directions
    draw_tensor_main_directions(mesh,
                                tensor_name=tensor_name,
                                fig=fig,
                                color=color)

    fig.gca().set_xlim(xlim_min, xlim_max)
    fig.gca().set_ylim(ylim_min, ylim_max)
    fig.gca().set_aspect('equal')
    fig.gca().set_xticks([])
    fig.gca().set_yticks([])
    fig.gca().xaxis.set_label_position('top')
    fig.gca().set_xlabel(tensor_name[:-7] + ' ' + property + ' heat map')

    # -- Saving the plot
    if save_figure:
        figure_name = 'heatmap_' + tensor_name[:-7] + '_' + property + '.png'
        plt.savefig(save_path+figure_name, dpi=300)


def draw_recent_divisions(mesh, fig=None):
    """Plot in green the edges added during the last round of cell division.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we are working on.
    fig : PyplotFigure
        Optional (default: None). Where we draw.

    Returns
    -------
    None

    """
    if not fig:
        fig = plt.figure()
        # mpl_draw_topomesh(mesh, fig)

    if 'vertices' not in mesh.wisp_properties(1).keys():
        compute_topomesh_property(mesh, 'vertices', 1)

    recent_edges = list(eid for eid, mother_id
                        in mesh.wisp_property('lineage_mother_id', 1).items()
                        if mother_id == -1)
    positions = mesh.wisp_property('barycenter', 0)
    edge_ends = positions.values(mesh.wisp_property('vertices',
                                                    1).values(recent_edges))

    for vtx_pos in edge_ends:
        fig.gca().plot(vtx_pos[:, 0],
                       vtx_pos[:, 1],
                       color='g',
                       linewidth=1.5,
                       alpha=1.,
                       zorder=5)


def draw_tissue_with_lines(mesh, intersection_positions, fig=None):
    """Plots lines intersecting a mesh.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to plot.
    intersection_positions : NDarray(Dict(NDarray))
        - keys : index of the intersected edges
        - values : positions of the intersections.
    fig : pyplot firgure
        The figure to plot in.

    Returns
    -------
    None

    """

    if not fig:
        fig = plt.figure()
        mpl_draw_topomesh(mesh, fig)

    for line_dict in intersection_positions:
        pos0, pos1 = list(line_dict.values())
        fig.gca().plot([pos0[0], pos1[0]], [pos0[1], pos1[1]])

    fig.show()


def draw_cell_walls(mesh,
                    cwll_property="length",
                    cwll_property_2=None,
                    color='r',
                    thickness_coef=1,
                    fig=None):
    """Plots the edges of a topomesh with a property coded as their thickness.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to plot.
    cwll_property : Str
        The name of the property to plot (coded as the edge thickness).
    cwll_property_2 : Str
        The name of the property to plot (coded as the edge color).
    color : Str
        The color to use on the edges.
    thickness_coef : Float
        A scaling coefficient to apply to the thickness of all edges.
    fig : pyplot figure
        The figure to draw in.

    Returns
    -------
    figure : matplolib figure.

    """
    if not fig:
        fig = plt.figure()
        # mpl_draw_topomesh(mesh, fig)

    if 'vertices' not in mesh.wisp_properties(1).keys():
        compute_topomesh_property(mesh, 'vertices', 1)

    edges_ids = list(mesh.wisps(1))
    positions = mesh.wisp_property('barycenter', 0)
    edge_ends = positions.values(mesh.wisp_property('vertices',
                                                    1).values(edges_ids))

    if cwll_property:
        property_2_plot = mesh.wisp_property(cwll_property,
                                             1).values(edges_ids)
    else:
        property_2_plot = np.ones(len(edges_ids))

    # -- If we want to visualize a property as a color code on the edges
    if cwll_property_2 is not None:
        sec_prop_2_plot = mesh.wisp_property(cwll_property_2,
                                             1).values(edges_ids)
        colors = pl.cm.inferno(np.linspace(0, 1, 101))

        for vtx_pos, property, sec_prop in zip(edge_ends,
                                               property_2_plot,
                                               sec_prop_2_plot):
            it = int(100 * (sec_prop - np.min(sec_prop_2_plot))
                     / (np.max(sec_prop_2_plot) - np.min(sec_prop_2_plot)))
            fig.gca().plot(vtx_pos[:, 0],
                           vtx_pos[:, 1],
                           color=colors[it],
                           linewidth=thickness_coef * property,
                           alpha=1.,
                           zorder=5)

    # -- If we want the regular visualization with a single color for the edges
    else:
        for vtx_pos, property in zip(edge_ends, property_2_plot):
            fig.gca().plot(vtx_pos[:, 0],
                           vtx_pos[:, 1],
                           color=color,
                           linewidth=thickness_coef * property,
                           alpha=1.,
                           zorder=5)


def generate_tissue_time_series(mesh_list,
                                visu_param=VISU_PARAM_DEFAULT_STACK,
                                frame_size=(40, 20),
                                fig_name='step_',
                                saving_path=SAVE_PATH_DEFAULT):
    """Generate a folder full of tissue snapshots at the same scale.

    Parameters
    ----------
    mesh_list : list(PropertyTopomesh)
        The list of the meshes we want to draw.
    visu_param : Dict(str or float or int)
        Optional (default : VISU_PARAM_DEFAULT - defined above the method code)
        - keys : Str. Names of the various parameters.
        - values : Str or Float or Int. Values of the various parameters.
    frame_size : tuple(float, float)
        The width (first) and heigth (second) of the snapshots.
    fig_name : str
        Optional (default : 'step_'). Common part of the recording names of
        the snapshots.
    saving_path : str
        Opitional (default : SAVE_PATH_DEFAULT). Where we want to
        record the snapshot.

    Returns
    -------
    None.

    """

    for nber, mesh in enumerate(mesh_list):
        print('Drawing tissue #', nber)
        draw_tissue(mesh, show_stress_orientation=False, visu_param=visu_param)

        # --*-- Recording figure
        fig_name = 'mesh_{:03d}.png'.format(nber)
        rec_path = saving_path + 'time_series_stack/'

        if not os.path.exists(rec_path):
            os.makedirs(rec_path)

        plt.savefig(rec_path+fig_name, dpi=300)


def draw_image_stack(image_loc_list,
                     column_number=4,
                     saving_path=SAVE_PATH_DEFAULT):
    """Short summary.

    Parameters
    ----------
    image_loc_list : list(str)
        List of the path to the images to plot.
    column_number : int
        Optional (default : 4). The number of column in the final figure.
    saving_path : str
        Opitional (default : SAVE_PATH_DEFAULT). Where we want to
        record the snapshot.

    Returns
    -------
    type
        Description of returned object.

    """

    # -- Setting the number of lines according to the number of columns
    #    and the number of elements to plot
    step_nbr = len(image_loc_list)
    n_col = column_number

    if step_nbr % n_col == 0:
        n_lin = step_nbr // n_col
    else:
        n_lin = step_nbr // n_col + 1

    # -- Setting the image size
    fig = plt.figure(figsize=(n_col*2*3, n_lin*3))
    fig.clf()

    grid = gs.GridSpec(n_lin, n_col)
    # fig.subplots_adjust(bottom=0.025, left=0.025, top=0.975, right=0.975)

    # -- Plotting all the subplots
    for it, img_path in enumerate(image_loc_list):

        axe = fig.add_subplot(grid[it // n_col, it % n_col])
        img = plt.imread(img_path)
        # -- Resizing the image:
        # x0, x1 = 384, 2692
        # y0, y1 = 184, 1336
        img = img[100: -100, 200: -200, :]
        axe.imshow(img)
        axe.axis('off')

        axe.set_aspect('equal')
        axe.set_xticks([])
        axe.set_yticks([])
        plt.text(100, 1200, 'step '+str(5 * it), color='w', fontsize=16)

    fig.subplots_adjust(wspace=0, hspace=0)
    fig.tight_layout()
    # plt.title(mesh_name + ' time evolution', fontsize=24)

    # --*-- Saving procedure
    figure_name = 'mesh_growth_frame_by_frame.png'
    plt.savefig(saving_path + figure_name, dpi=300)

    plt.show()


# tissue_visualization.py ends here.
