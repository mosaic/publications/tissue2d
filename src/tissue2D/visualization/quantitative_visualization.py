# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:
import os
import numpy as np
from matplotlib import cm, colors
import numpy.linalg as lng
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
from collections.abc import Iterable
from matplotlib.colors import Normalize
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

from cellcomplex.property_topomesh.property_topomesh import PropertyError
from cellcomplex.property_topomesh.utils.matplotlib_tools import mpl_draw_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property

from tissue2D.utils.tools import (max_eigvect, intensity, anisotropy,
                                  compute_shape_descriptor)
from tissue2D.visualization.tissue_visualization import (SAVE_PATH_DEFAULT,
                                                         draw_cell_walls,
                                                         draw_tensor_main_directions)

# -- Visualization parameters default Values
QPLOT_PARAM_DEFAULT = {'back_ground_color': 'black',
                       'field_of_view': 'automatic',
                       'tensor_1_name': 'stress_tensor',
                       'tensor_2_name': 'growth_tensor',
                       'cell_property': 'stress_anisotropy',
                       'cell_property_2': 'growth_intensity',
                       'cell_colormap': 'viridis',
                       'cell_colormap_2': 'inferno',
                       'cell_alpha': .75,
                       'cell_alpha_2': .75,
                       'edge_property': 'cwll_stiffness',
                       '2nd_edge_property': 'cwll_stiffness',
                       'edge_thickness': 2,
                       'edge_color': 'w'}

STANDARD_PROPERTIES = ['aspect_ratio',
                       'orientation',
                       'surface_area',
                       'cell_number']


# ####################################################################### #
# Functions using the PropertyTopomesh Class as the input data structure. #
# ####################################################################### #
def plot_simulation_characteristics(mesh,
                                    visu_param=QPLOT_PARAM_DEFAULT,
                                    mesh_name=None,
                                    save_figure=False,
                                    save_path=None):
    """Plots stress visualization and quantification.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The tissue we want to analyse. It should contain a 'stress_tensor'
        property on its elements of degree two.
    visu_param : Dict(Str or Float)
        Opitional (default : see above). A list of parameter to set the
        visualization.
    mesh_name : Str
        Optional (default : None). The name of the considered mesh.
    save_figure : Bool
        Optional (default : False). If True a .png of the graph is recorded.
    save_path : Str
        Optional (default : None). The location where we want to record
        the graphs.

    Returns
    -------
    None.

    """

    if save_figure and not mesh_name:
        mesh_name = 'mesh2D'

    if save_figure and not save_path:
        save_path = SAVE_PATH_DEFAULT

        save_path += 'plots/stack/'
        if not os.path.exists(save_path):
            os.makedirs(save_path)

    # -- Compute stress intensity and anisotropy
    tensor_1_intensity = {cid: intensity(tensor)
                          for cid, tensor in
                          mesh.wisp_property(visu_param['tensor_1_name'],
                                             2).items()}

    tensor_1_anisotropy = {cid: anisotropy(tensor)
                           for cid, tensor in
                           mesh.wisp_property('stress_tensor', 2).items()}

    mesh.update_wisp_property(visu_param['tensor_1_name'][:-7] + '_intensity',
                              2, tensor_1_intensity)
    mesh.update_wisp_property(visu_param['tensor_1_name'][:-7] + '_anisotropy',
                              2, tensor_1_anisotropy)

    # -- Multiple plots
    fig = plt.figure(figsize=(20, 8))
    grid = gs.GridSpec(2, 5)
    fig.subplots_adjust(bottom=0.025, left=0.025, top=0.975, right=0.975)

    # --*-- First plot: the tissue
    fig.add_subplot(grid[:, :-3])

    vtx_pos = mesh.wisp_property('barycenter', 0).values()
    xlim_max = np.max(vtx_pos[:, 0]) + 1
    xlim_min = np.min(vtx_pos[:, 0]) - 1
    ylim_max = np.max(vtx_pos[:, 1]) + 1
    ylim_min = np.min(vtx_pos[:, 1]) - 1

    tissue = mpl_draw_topomesh(mesh,
                               fig,
                               property_name=visu_param['cell_property'],
                               property_degree=2,
                               colormap=visu_param['cell_colormap'],
                               alpha=visu_param['cell_alpha'],
                               cell_edges=True,
                               linewidth=10)

    draw_cell_walls(mesh,
                    cwll_property=visu_param['edge_property'],
                    cwll_property_2=visu_param['2nd_edge_property'],
                    thickness_coef=visu_param['edge_thickness'],
                    color=visu_param['edge_color'],
                    fig=fig)

    draw_tensor_main_directions(mesh, fig=fig)

    fig.gca().set_xlim(xlim_min, xlim_max)
    fig.gca().set_ylim(ylim_min, ylim_max)
    fig.gca().set_aspect('equal')
    fig.gca().set_xticks([])
    fig.gca().set_yticks([])
    fig.gca().set_title(visu_param['cell_property'] + ' heat map (edges: '
                        + visu_param['2nd_edge_property'] + ')')

    # --*--*-- Plotting the colorbar of the heatmap
    cax0 = inset_axes(fig.gca(), width="3%", height="25%", loc='lower right')
    fig.colorbar(mappable=tissue, cax=cax0, pad=0.)
    cax0.yaxis.set_ticks_position('left')

    # --*-- tensor 1 intenstiy histogram
    fig.add_subplot(grid[0, -3])
    plot_property_histogram(mesh,
                            visu_param['tensor_1_name'][:-7] + '_intensity', 2,
                            color='b',
                            alpha=.5,
                            fig=fig,
                            save_figure=False,
                            save_path=None)

    # --*-- tensor 1 anisotropy histogram
    fig.add_subplot(grid[1, -3])
    plot_property_histogram(mesh,
                            visu_param['tensor_1_name'][:-7] + '_anisotropy',
                            2,
                            color='b',
                            alpha=.5,
                            fig=fig,
                            save_figure=False,
                            save_path=None)

    # --*-- edge stretch histogram
    fig.add_subplot(grid[0, -2])
    plot_property_histogram(mesh,
                            'edge_strain',
                            1,
                            color='g',
                            alpha=.5,
                            fig=fig,
                            save_figure=False,
                            save_path=None)

    # --*-- edge force intensity histogram
    fig.add_subplot(grid[1, -2])
    plot_property_histogram(mesh,
                            'cwll_force_intensity',
                            1,
                            color='y',
                            alpha=.5,
                            fig=fig,
                            save_figure=False,
                            save_path=None)

    # --*-- Tensor 2 heatmap
    fig.add_subplot(grid[0, -1])
    vtx_pos = mesh.wisp_property('barycenter', 0).values()
    xlim_max = np.max(vtx_pos[:, 0]) + 1
    xlim_min = np.min(vtx_pos[:, 0]) - 1
    ylim_max = np.max(vtx_pos[:, 1]) + 1
    ylim_min = np.min(vtx_pos[:, 1]) - 1

    tissue2 = mpl_draw_topomesh(mesh,
                                fig,
                                property_name=visu_param['cell_property_2'],
                                property_degree=2,
                                colormap=visu_param['cell_colormap_2'],
                                alpha=visu_param['cell_alpha_2'])

    draw_tensor_main_directions(mesh,
                                tensor_name=visu_param['tensor_2_name'],
                                fig=fig, color=visu_param['edge_color'])

    fig.gca().set_xlim(xlim_min, xlim_max)
    fig.gca().set_ylim(ylim_min, ylim_max)
    fig.gca().set_aspect('equal')
    fig.gca().set_xticks([])
    fig.gca().set_yticks([])
    fig.gca().set_title(visu_param['cell_property_2']+' heat map')

    # --*--*-- Plotting the colorbar of the heatmap
    cax = inset_axes(fig.gca(), width="3%", height="25%", loc='lower right')
    fig.colorbar(mappable=tissue2, cax=cax, pad=0.)
    cax.yaxis.set_ticks_position('left')

    # --*-- Correlation between tensors 1 and 2 eigendirections
    cllids = list(mesh.wisps(2))
    tsr_1 = mesh.wisp_property(visu_param['tensor_1_name'], 2).values(cllids)
    tsr_2 = mesh.wisp_property(visu_param['tensor_2_name'], 2).values(cllids)

    tsr1_2_correlation = [2 * np.abs(np.dot(max_eigvect(tsr1),
                                            max_eigvect(tsr2))) - 1
                          for tsr1, tsr2 in zip(tsr_1, tsr_2)]

    fig.add_subplot(grid[1, -1])
    nmbr_cells_per_bin, bins, patches = plt.hist(tsr1_2_correlation,
                                                 color='k',
                                                 alpha=.5)

    xlim_min = -1
    xlim_max = 1.
    ylim_min = 0.
    ylim_max = 1.33 * np.max(nmbr_cells_per_bin)

    fig.gca().set_xlim(xlim_min, xlim_max)
    fig.gca().set_ylim(ylim_min, ylim_max)
    fig.gca().set_title('Correlation ' + visu_param['tensor_1_name'][:-7]
                        + ' & ' + visu_param['tensor_2_name'][:-7]
                        + ' directions')

    # --*-- Saving procedure
    if save_figure:
        figure_name = mesh_name + '_analysis.png'
        plt.savefig(save_path+figure_name, dpi=300)

    fig.show()


def plot_tissue_property_time_evolution(meshes,
                                        property='aspect_ratio',
                                        meshes_name=None,
                                        yrange='automatic',
                                        color='k',
                                        marker='o',
                                        linestyle='-',
                                        legend_location='upper left',
                                        fig=None,
                                        save_figure=False,
                                        save_path=None):
    """follow the time evolution of a property of a growing 2D mesh.

    Parameters
    ----------
    meshes : list(PropertyTopomesh)
        The time squence of meshes to follow.
    property : Str
        Optional (default: aspect_ratio). Name of the property to draw.
        Possibilities: aspect_ratio, surface_area, cell_number
    meshes_name : Str
        Optional (default : None). Name of the followed line.
    color : Matplotlib color
        Optional (default : 'k'). Color of the drawn line.
    marker : Matplotlib marker.
        Optional (default : 'o'). Shape of the marker to use.
    linestyle : Str
        Optional (default : '-'). Style for the line.
    legend_location: Str
        Optional (default : 'upper left'). Position of the legend
        within the graph.
    fig : Matplotlib Figure.
        Optional (default : None). The graphic element to draw on.
    save_figure : Bool
        Optional (default : False). If True a .png of the graph is recorded.
    save_path : Str
        Optional (default : None). The location where we want to record
        the graphs.

    Returns
    -------
    None.

    """

    if not fig:
        fig = plt.figure()

    if not meshes_name:
        meshes_name = 'meshes2D'

    if save_figure and not save_path:
        save_path = SAVE_PATH_DEFAULT

    # -- Computing the aspect ratio
    property_2_plot = []
    if property == 'aspect_ratio':
        y_label = 'Aspect ratio (W/T)'
        for mesh in meshes:
            mesh_width = (mesh.wisp_property('barycenter',
                                             0).values()[:, 0].max()
                          - mesh.wisp_property('barycenter',
                                               0).values()[:, 0].min())
            mesh_thick = (mesh.wisp_property('barycenter',
                                             0).values()[:, 1].max()
                          - mesh.wisp_property('barycenter',
                                               0).values()[:, 1].min())

            property_2_plot.append(mesh_width / mesh_thick)

    elif property == 'surface_area':
        y_label = 'Relative surface area'

        for mesh in meshes:
            if not mesh.has_wisp_property('area', 2):
                compute_topomesh_property(mesh, 'area', 2)

            property_2_plot.append(sum(mesh.wisp_property('area', 2).values()))

        # -- Normalizing the surf. area list by its first value
        property_2_plot = np.array(property_2_plot)
        property_2_plot /= property_2_plot[0]
    elif property == 'cell_number':
        y_label = 'Cell number'
        for mesh in meshes:
            property_2_plot.append(mesh.nb_wisps(2))
    elif property == 'orientation':
        y_label = 'Main tissue axis orientation \n' +\
                  '(cos(angle(main axe vs Ox)))'
        for mesh in meshes:
            eig_vals, eig_vecs = lng.eig(compute_shape_descriptor(mesh))
            if eig_vals[0] > eig_vals[1]:
                dir_vec = eig_vecs[0]
            else:
                dir_vec = eig_vecs[1]

            unit_x_vec = np.array([1, 0])
            property_2_plot.append(np.abs(np.dot(dir_vec, unit_x_vec)))

    # -- Plotting the selected property
    plt.plot(property_2_plot,
             color=color,
             marker=marker,
             linestyle=linestyle,
             label=meshes_name)
    plt.legend(loc=legend_location, prop={'size': 6})
    plt.title(property + ' time evolution')
    plt.ylabel(y_label)

    # --*-- Setting optional custom vertical axis range
    if yrange != 'automatic':
        plt.ylim(yrange)

    plt.xlabel('Simulation time step')

    if property == 'orientation':
        plt.ylim([-0.1, 1.1])

    # -- Saving the figure
    if save_figure:
        figure_name = meshes_name + '_'+property+'_time_evolution.png'
        plt.savefig(save_path+figure_name, dpi=300)


def plot_tissue_properties_time_evolution(meshes,
                                          properties_2_plot=STANDARD_PROPERTIES,
                                          plots_style=None,
                                          legend_positions=None,
                                          save_figure=False,
                                          save_path=None,
                                          figure_name=None):
    """follow the time evolution of properties of a growing 2D mesh.

    Parameters
    ----------
    meshes : dict(PropertyTopomesh)
      - keys : names of the various time sequences
      - values : The time squences of meshes to follow.
    properties_2_plot : list(Str)
      Optional (default: STANDARD_PROPERTIES). List of property names to draw.
      STANDARD_PROPERTIES: aspect_ratio, surface_area, cell_number, orientation
    plots_style : dict(list(str))
        Optional (default: None).
        - keys : names of the various time sequences
        - values : list of two elements of the type
                   ['x', 'y'] with 'x' = color name, 'y' = marker type
    legend_positions: list(str)
        Optional (default: None). Position of the legend in the various graphs.
    save_figure : Bool
      Optional (default : False). If True a .png of the graph is recorded.
    save_path : Str
      Optional (default : None). The location where we want to record
      the graphs.

    Returns
    -------
    None.

    """
    # -- A Hack to restrain the Y-range of surface area plots
    yrange = [0, 10]

    if not plots_style:
        style = {sim_name: ['k', 'o', '-'] for sim_name in meshes.keys()}
    else:
        style = plots_style

    if not legend_positions:
        location = ['upper left', 'upper left', 'upper left', 'upper left']
    else:
        location = legend_positions

    # -- Defining a saving location
    if save_figure:
        if not save_path:
            save_path = '/Users/oali/Documents/Work/Research/' +\
                        'Devlp/tissue2D/data/results/'

        save_path += 'plots/'

        if not os.path.exists(save_path):
            os.makedirs(save_path)

    # -- Multiple plots
    fig = plt.figure(figsize=(8, 8))
    grid = gs.GridSpec(2, 2)
    fig.subplots_adjust(bottom=0.025, left=0.025, top=0.975, right=0.975)

    # --*-- First plot
    fig.add_subplot(grid[0, 0])
    for sim_name, meshes_list in meshes.items():
        plot_tissue_property_time_evolution(meshes_list,
                                            property=properties_2_plot[0],
                                            meshes_name=sim_name,
                                            color=style[sim_name][0],
                                            marker=style[sim_name][1],
                                            linestyle=style[sim_name][2],
                                            legend_location=location[0],
                                            fig=fig,
                                            save_figure=False)

    # --*-- Second plot
    fig.add_subplot(grid[1, 0])
    for sim_name, meshes_list in meshes.items():
        plot_tissue_property_time_evolution(meshes_list,
                                            property=properties_2_plot[1],
                                            meshes_name=sim_name,
                                            color=style[sim_name][0],
                                            marker=style[sim_name][1],
                                            linestyle=style[sim_name][2],
                                            legend_location=location[1],
                                            fig=fig,
                                            save_figure=False)

    # --*-- Third plot
    fig.add_subplot(grid[0, 1])
    for sim_name, meshes_list in meshes.items():
        plot_tissue_property_time_evolution(meshes_list,
                                            property=properties_2_plot[2],
                                            meshes_name=sim_name,
                                            yrange=yrange,
                                            color=style[sim_name][0],
                                            marker=style[sim_name][1],
                                            linestyle=style[sim_name][2],
                                            legend_location=location[2],
                                            fig=fig,
                                            save_figure=False)

    # --*-- Fourth plot
    fig.add_subplot(grid[1, 1])
    for sim_name, meshes_list in meshes.items():
        plot_tissue_property_time_evolution(meshes_list,
                                            property=properties_2_plot[3],
                                            meshes_name=sim_name,
                                            color=style[sim_name][0],
                                            marker=style[sim_name][1],
                                            linestyle=style[sim_name][2],
                                            legend_location=location[3],
                                            fig=fig,
                                            save_figure=False)

    plt.tight_layout()

    # --*-- Saving procedure
    if save_figure:
        if not figure_name:
            figure_name = 'tissue_geometrical_properties_time_evolution.png'
        plt.savefig(save_path+figure_name, dpi=300)

    fig.show()


def plot_single_elements_property_time_evolution(mesh_list,
                                                 property_name,
                                                 degree,
                                                 wids_2_consider='all',
                                                 mesh_name='mesh2D',
                                                 save_figure=True,
                                                 save_path=None,
                                                 colormap='viridis',
                                                 alpha=1,
                                                 yrange=None,
                                                 inner_outer=False):
    """Plots the time evolution of a scalar property defined on a list of mesh.

    Parameters
    ----------
    mesh_list : list(PropertyTopomesh)
        The meshes we want to work with.
    property_name : str
        The name of the property to look at.
    degree : int
        The degree at which the property in defined within the topomesh.
    wids_2_consider : list(int)
        Optional (default : 'all'). List of ids to consider in the plot.
    mesh_name : str
        Optional (default : 'mesh2D'). Name to use to record the plot.
    save_figure : bool
        Optional (default : True). Record a png image of the plot if True.
    save_path : str

        Optional (default : None). location where to record the image.
    colormap : str
        Optional (default : 'viridis'). Name of the colormap to use.
    alpha : float
        Optional (default : 1). Alpha value for the plot.
    yrange: list(float)
        Optional (default : None). The span of the y axis.
    inner_outer : bool
        Optional (default : False). If true the color code distingues
        between inner and outer edges.

    Returns
    -------
    None.

    """
    plt.clf()

    # -- Setting the list of wids to consider:
    if (wids_2_consider != 'all'
            and not isinstance(wids_2_consider, Iterable)):
        wids_2_consider = [wids_2_consider]

    for mesh in mesh_list:
        assert property_name in mesh.wisp_properties(degree).keys()

    step_number = len(mesh_list)
    all_steps = range(0, step_number)
    color = list(map(lambda x: cm.ScalarMappable(cmap=colormap,
                                                 norm=Normalize(0,
                                                                step_number)
                                                 ).to_rgba(x), all_steps))

    property_2_plot = {}
    for step in all_steps:
        mesh = mesh_list[step]

        # -- Separating the outermost wisps from the inner ones
        if degree == 1:
            epiderm = {eid: type == 'outer' for eid, type
                       in mesh.wisp_property('edge_type', 1).items()}
        elif degree == 2:
            epiderm = {}
            for cid in mesh.wisps(2):
                eids = list(mesh.borders(2, cid))

                border_types = mesh.wisp_property('edge_type', 1).values(eids)
                epiderm[cid] = (border_types == 'outer').any()

        # -- Constructing the data set
        for wid, property in mesh.wisp_property(property_name, degree).items():
            if wid not in property_2_plot.keys():
                property_2_plot[wid] = (step, [property], epiderm[wid])
            else:
                property_2_plot[wid][1].append(property)

    # -- Plotting the data set
    for wid, (starting_step, property, epiderm) in property_2_plot.items():
        if (wids_2_consider == 'all' or wid in wids_2_consider):
            if inner_outer:
                if epiderm:
                    plt_color = 'g'
                else:
                    plt_color = 'b'

            else:
                plt_color = color[starting_step]

            # --*-- Starting point
            plt.scatter(starting_step,
                        property[0],
                        color=plt_color,
                        # edgecolors='w',
                        alpha=alpha,
                        zorder=10)

            # --*-- Ending point
            ending_step = starting_step + len(property) - 1
            if ending_step < step_number:
                plt.scatter(ending_step,
                            property[-1],
                            color=plt_color,
                            marker='s',
                            # edgecolors='w',
                            alpha=alpha,
                            zorder=10)

            # --*-- Trajectory
            plt.plot(list(range(starting_step, step_number)),
                     property,
                     color=plt_color,
                     alpha=alpha)

    # -- Labelling and naming
    if inner_outer:
        plt.plot(0, 0, color='b', label='inner edges')
        plt.plot(0, 0, color='g', label='outer edges')
        plt.legend(loc='lower right')

    plt.title(property_name+' time evolution')
    plt.ylabel(property_name)
    plt.xlabel('Simulation time steps')
    plt.xticks(all_steps)
    if yrange:
        plt.ylim(yrange)

    # -- Saving the plot
    if save_figure:
        if not save_path:
            save_path = SAVE_PATH_DEFAULT

        save_path += 'plots/'
        if not os.path.exists(save_path):
            os.makedirs(save_path)

        figure_name = mesh_name + '_'+property_name+'_time_evolution.png'
        plt.savefig(save_path+figure_name, dpi=300)


def plot_property_histogram(mesh,
                            property_name,
                            degree,
                            color='b',
                            alpha=.5,
                            fig=None,
                            save_figure=False,
                            save_path=None):
    """Plots an histogram of a scalar property defined on a mesh.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we are working on.
    property_name : str
        The name of the property of interest.
    degree : int
        The wisp degree on which the property is defined
    color : matplotlib color code
        Optional (default : 'b'). The color we want for the histogram.
    alpha : Float
        Optional (default : .5). The alpha value we want for the histogram.
    fig : matplotlib figure
        Optional (default : None). To include the histogram in a bigger figure.
    save_figure : bool
        Optional (default : False). Save the figure if true.
    save_path : str
        Optional (default : None). A custom path where to save the figure.

    Returns
    -------
    none.

    """
    elements = {0: 'vertices', 1: 'edges', 2: 'cells'}
    if not fig:
        fig = plt.figure()

    if save_figure and not save_path:
        save_path = SAVE_PATH_DEFAULT

    property = list(mesh.wisp_property(property_name, degree).values())

    nmbr_cells_per_bin, bins, patches = plt.hist(property,
                                                 color=color,
                                                 alpha=alpha)

    property_mean = np.mean(property)
    property_std = np.std(property)

    xlim_min = property_mean - 3 * property_std
    xlim_max = property_mean + 3 * property_std
    ylim_min = 0
    ylim_max = 1.33 * np.max(nmbr_cells_per_bin)

    plt.plot([property_mean, property_mean], [ylim_min, ylim_max],
             color=color,
             ls='--')

    plt.plot([property_mean - property_std, property_mean - property_std],
             [ylim_min, ylim_max],
             color=color,
             ls=':')

    plt.plot([property_mean + property_std, property_mean + property_std],
             [ylim_min, ylim_max],
             color=color,
             ls=':')

    fig.gca().set_xlim(xlim_min, xlim_max)
    fig.gca().set_ylim(ylim_min, ylim_max)
    fig.gca().set_title(property_name+' distribution on '+elements[degree])
    plt.tight_layout()

    if save_figure:
        figure_name = property+'_histogram.png'
        plt.savefig(save_path+figure_name, dpi=300)

    if not fig:
        plt.show()


def plot_properties_correlation(mesh,
                                property_name_1,
                                property_name_2,
                                degree,
                                wids_2_consider='all',
                                mesh_name='mesh',
                                marker='+',
                                color='b',
                                alpha=1,
                                legend_pos='upper left',
                                xlim=None,
                                fig=None,
                                save_figure=False,
                                save_path=None):
    """Plots an point cloud in a 2D property space.

    Parameters
    ----------
    mesh : PropertyTopomesh
        The mesh we are working on.
    property_name_1 : str
        The name of the property to plot along the x axis.
    property_name_2 : str
        The name of the property to plot along the y axis.
    wids_2_consider : str / list(int)
        Optional (default : 'all'). The ids of the wisps to consider.
    mesh_name : str
        Optional (default : 'mesh'). Custom name for the simulation.
    degree : int
        The wisp degree on which the property is defined
    marker : str
        Optional (default : '+'). The marker glyph to use.
    color : matplotlib color code
        Optional (default : 'b'). The color we want for the histogram.
    alpha : Float
        Optional (default : .5). The alpha value we want for the histogram.
    legend_pos : str
        Optional (default : 'upper left'). Where to put the legend on the plot.
    xlim: tuple(float, float)
        Optional( default : None). The variable range along the axis.
    fig : matplotlib figure
        Optional (default : None). To include the histogram in a bigger figure.
    save_figure : bool
        Optional (default : False). Save the figure if true.
    save_path : str
        Optional (default : None). A custom path where to save the figure.

    Returns
    -------
    none.

    """

    if not fig:
        fig = plt.figure()

    if not save_path:
        save_path = SAVE_PATH_DEFAULT

    save_path += 'plots/stack_correlation/'
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    # -- Defining subclasses of interest
    localization = {'epithelium': [], 'inner tissue': []}
    if degree == 1:
        for wid, etype in mesh.wisp_property('edge_type', 1).items():
            if etype == 'outer':
                localization['epithelium'].append(wid)
            else:
                localization['inner tissue'].append(wid)

    if degree == 2:
        for cid in mesh.wisps(2):
            border_ids = list(mesh.borders(2, cid))
            border_types = mesh.wisp_property('edge_type',
                                              1).values(border_ids)

            if (border_types == 'inner').all():
                localization['inner tissue'].append(cid)
            else:
                localization['epithelium'].append(cid)

    # -- Getting the two properties to plot
    property = {}
    for loc_name, loc_wids in localization.items():
        loc_property = {}
        for property_name in [property_name_1, property_name_2]:
            try:
                loc_property[property_name] = list(mesh.wisp_property(
                                                   property_name,
                                                   degree).values(loc_wids))
            except PropertyError:
                print('WARNING: ' + property_name
                      + ' not defined on degree ' + str(degree)
                      + ' wisps of the considered mesh')
            except:
                print('Something else went wrong...')

        property[loc_name] = loc_property

    colors = {'inner tissue': 'b', 'epithelium': 'g'}
    # -- Plotting the properties
    for loc_name, loc_wids in localization.items():
        plt.scatter(property[loc_name][property_name_1],
                    property[loc_name][property_name_2],
                    marker=marker,
                    color=colors[loc_name],
                    alpha=alpha,
                    label=loc_name)

    plt.gca().set_xlabel(property_name_1)
    plt.gca().set_ylabel(property_name_2)
    plt.gca().set_title(mesh_name)

    if xlim:
        plt.gca().set_xlim(xlim)
        plt.gca().set_ylim(xlim)

        plt.gca().plot(xlim,
                       xlim, ls=':', color='k', alpha=.5, label='bisector')
    else:
        plt.gca().plot([0, 1],
                       [0, 1], ls=':', color='k', alpha=.5, label='bisector')

    plt.gca().legend(loc=legend_pos)
    plt.gca().set_aspect('equal')
    plt.tight_layout()

    # -- Saving the plot
    if save_figure:
        figure_name = 'correlation_' + property_name_1 + '_'\
                                     + property_name_1 + '_'\
                                     + mesh_name + '.png'
        plt.savefig(save_path+figure_name, dpi=300)

    if not fig:
        plt.show()


# ############################################################## #
# Functions using Pandas DataFrames as the input data structure. #
# ############################################################## #
def boxplot_time_series(time_series, color, label, yaxis_name, fig, display_legend=True):
    """Generates a boxplot for each step of a time series.

    Parameters
    ----------
    time_series : dict
        - keys : str. The names of the various time series to plot.
        - values : list(float) the array for each step for each time serie
                   to boxplot.
    color : dict
        - keys : str. The names of the various time series to plot.
        - values : matplotlib color values. Color associated with
                   each time series.
    label : dict
        - keys : str. The names of the various time series to plot.
        - values : str. Name of each time series for the legends.
    yaxis_name : str
        the name of the quantity that is plotted.
    fig : figure matplotlib
        Where to draw the time serie.
    display_legend : bool
        Optional (default : True). If true a legend is added to the graph.

    Returns
    -------
    None

    Notes
    -----
    This function is supposed to be used within another to generate
    grid plots of the time evolution of various properties.
    """

    # -- Style to apply to the median marker
    style = {name: {'color': color[name], 'linewidth': 2}
             for name in time_series.keys()}

    for name, cnbr in time_series.items():
        box2 = fig.gca().boxplot(cnbr,
                                 patch_artist=True,
                                 showfliers=False,
                                 medianprops=style[name])
        plt.setp(box2['boxes'],
                 color=color[name],
                 facecolor=color[name],
                 alpha=.33)
        plt.setp(box2['whiskers'],
                 color=color[name],
                 alpha=.33)
        plt.setp(box2['caps'],
                 color=color[name],
                 alpha=.33)

        plt.plot([1], [cnbr[0][0]], color=color[name], label=label[name])

    fig.gca().xaxis.set_major_locator(MultipleLocator(5))
    fig.gca().xaxis.set_major_formatter(FormatStrFormatter('%d'))
    fig.gca().xaxis.set_minor_locator(MultipleLocator(1))
    fig.gca().grid(axis='x', which='major', linestyle=':')

    fig.gca().set_xlabel('time steps', fontsize=16)
    fig.gca().set_ylabel(yaxis_name, fontsize=16)
    if display_legend:
        fig.gca().legend(loc='upper left', fontsize=20)


def versus_plot_time_series(x_series,
                            y_series,
                            style,
                            labels=['x data', 'y data'],
                            xlim=2.2,
                            data_to_consider='all',
                            axes_font_size='24',
                            show_legend=True,
                            legend_loc='upper center',
                            fig=None):
    """Plots one variable versus the other with errorbars in both directions.

    Parameters
    ----------
    x_series : dict(list(list))
        - keys : the names of the various simulations we want to compare.
        - values : a list of time steps each containing a list of replicates.
    y_series : dict(list(list))
        - keys : the names of the various simulations we want to compare.
        - values : a list of time steps each containing a list of replicates.
    style : dict()
        - keys : str in ['color', 'marker']
        - values : dict()
            - keys : the names of the various simulations we want to compare.
            - values : the color and marker value to allocate to each
                       simulation.
    labels : list(str)
        the x an y labels to put on the graph.
    xlim : float
        Optional (default : 2.2). A threshold value above which the points are
        not displayed.
    data_to_consider : str / list(str)
        Optional (default: 'all'). The name of the simulation to plot.
    axes_font_size : int
        Optional (default: 24). The size of the axes labels
    show_legend : Bool
        Optional (default : True). Display the name of the graphs if true.
    legend_loc : str
        Optional (default : 'upper center'). Where to put the legend.
    fig : matplolib figure
        The figure to plot on.

    Returns
    -------
    None

    """

    color = style['color']
    marker = style['marker']
    label = style['label']
    size = style['size']

    names = x_series.keys()
    if data_to_consider == 'all':
        authorized_names = names
    else:
        authorized_names = data_to_consider

    all_x_datas = list(x_series.values())
    all_y_datas = list(y_series.values())

    for name, x_datas, y_datas in zip(names, all_x_datas, all_y_datas):
        if name in authorized_names:
            x_mean = [np.median(time_step) for time_step in x_datas]
            x_std = [np.percentile(time_step, 75)
                     - np.percentile(time_step, 25)
                     for time_step in x_datas]
            y_mean = [np.median(time_step) for time_step in y_datas]
            y_std = [np.percentile(time_step, 75)
                     - np.percentile(time_step, 25)
                     for time_step in y_datas]

            # --*-- Defining the legend.
            fig.gca().scatter(x_mean[0], y_mean[0],
                              color=color[name],
                              marker=marker[name],
                              label=label[name],
                              s=10*size)

            # --*-- Plotting the data
            for x, y, dx, dy in zip(x_mean, y_mean, x_std, y_std):
                if x < xlim:
                    fig.gca().errorbar(x, y, yerr=dy/2, xerr=dx/2,
                                       color=color[name], marker=marker[name],
                                       markersize=size,
                                       capsize=3, capthick=1, elinewidth=1,
                                       ecolor=colors.to_rgba(color[name],
                                                             alpha=.5))

    # fig.gca().yaxis.set_major_locator(MultipleLocator(.1))
    # fig.gca().yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    # fig.gca().yaxis.set_minor_locator(MultipleLocator(.05))
    # fig.gca().xaxis.set_major_locator(MultipleLocator(.5))
    # fig.gca().xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    # fig.gca().xaxis.set_minor_locator(MultipleLocator(.25))
    fig.gca().grid(axis='y', which='both', linestyle=':')
    fig.gca().grid(axis='x', which='both', linestyle=':')

    fig.gca().set_xlabel(labels[0], fontsize=axes_font_size)
    fig.gca().set_ylabel(labels[1], fontsize=axes_font_size)
    if show_legend:
        fig.gca().legend(loc=legend_loc, fontsize=20)

# quantitative_visualization.py ends here.
