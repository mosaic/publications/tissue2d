"""
Package to simulate growth, division and stiffening of 2D plant tissues
"""
# {# pkglts, base

import logging
from . import version

__version__ = version.__version__

# #}

# -- Global varialbe
LOG_FMT = '%(levelname)s: %(message)s'

# -- Set the logging format and level:
logging.basicConfig(format=LOG_FMT, level=logging.INFO)
