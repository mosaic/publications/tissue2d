# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Email:  olivier.ali@inria.fr

"""
This scripts generates and plays a tissue2D simulation.
"""
import logging
import numpy as np

from cellcomplex.property_topomesh.io import read_ply_property_topomesh

from tissue2D.utils.mesh_generation import formatting_mesh
from tissue2D.models.master import Tissue2DModel

logging.getLogger().setLevel(logging.INFO)

# -- Initialization
mesh_name = 'ellipse_ratio2_mesh.ply'

path_2_mesh = '../data/'
initial_mesh = read_ply_property_topomesh(path_2_mesh + mesh_name)

# --*-- WARNING:
#       the parameter 'cwll_stiffness_ratio' should have the same value than
formatting_mesh(initial_mesh, cwll_stiffness_ratio=3.)

# --*-- Usefull to define the division threshold
mean_cell_surf = np.mean(initial_mesh.wisp_property('area', 2).values())


# -- Settings
EDGE_TYPES = ['all', 'inner', 'outer']
STF_METHODS = ['random', 'along_max_stress', 'radial', 'along_edge']
DIV_ORIENT_METHODS = ['random',
                      'along_max_stress',
                      'errera',
                      'radial',
                      'ortho_growth',
                      'out_radial_in_random',
                      'out_radial_in_errera',
                      'out_radial_in_along_max_stress',
                      'out_radial_in_ortho_growth']

models = {'mechanics': True,
          'growth': True,
          'division': True,
          'stiffening': True,
          'recording': True}

simu_params = {'step_number': 75,
               'pressure': .1,
               'stiffness_ratio': 3.,
               'gth_rate': .04,
               'gth_threshold': .1,
               'gth_exponent': 7,
               'stf_rate': 2,
               'stf_threshold': .15,
               'stf_exponent': 5,
               'stf_method': STF_METHODS[-1],
               'edges_2_stiffen': EDGE_TYPES[1],
               'div_threshold': 2 * mean_cell_surf,
               'div_orient_method': DIV_ORIENT_METHODS[-2],
               'recording_path': '../results/'}

visu_params = {'save_figure': True,
               'visu_each_step': True,
               'show_stress_orient': True,
               'show_recent_div': False,
               'back_ground_color': 'black',
               'edge_color': 'r',
               'tensor_color': 'g',
               'field_of_view': 'automatic',
               'image_aspect_ratio': 1,
               'cell_property': 'area',
               'cell_colormap': 'viridis',
               'cell_alpha': .0,
               'edge_property': 'cwll_stiffness',
               'edge_property_2': None,
               'edge_thickness': 2}


# -- Instantiation
def play_simulation():
    model = Tissue2DModel()

    model.set_mesh(initial_mesh, mesh_name=mesh_name[:-4])

    model.set_processes(mechanics=models['mechanics'],
                        growth=models['growth'],
                        division=models['division'],
                        stiffening=models['stiffening'],
                        recording=models['recording'],
                        recording_path=simu_params['recording_path'])

    model.set_mechanical_parameters(pressure=simu_params['pressure'],
                                    stiffness_ratio=simu_params['stiffness_ratio'])

    model.set_growth_parameters(rate=simu_params['gth_rate'],
                                threshold=simu_params['gth_threshold'],
                                exponent=simu_params['gth_exponent'])

    model.set_division_parameters(threshold=simu_params['div_threshold'],
                                  method=simu_params['div_orient_method'])

    model.set_stiffening_parameters(rate=simu_params['stf_rate'],
                                    threshold=simu_params['stf_threshold'],
                                    exponent=simu_params['stf_exponent'],
                                    method=simu_params['stf_method'],
                                    edges=simu_params['edges_2_stiffen'])

    model.set_visualization_parameters(visu_each_step=visu_params['visu_each_step'],
                                       show_stress_orient=visu_params['show_stress_orient'],
                                       save_fig=visu_params['save_figure'],
                                       visu_params=visu_params)

    model.run(t_min=0, t_max=simu_params['step_number']-1, dt=1)


if __name__ == '__main__':
    play_simulation()

# play_simulation.py ends here.
