# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Email:  olivier.ali@inria.fr

"""
This scripts generates a number of plots from simulation outputs.
"""
import os

import matplotlib.pyplot as plt

from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.io import read_ply_property_topomesh

# -- Loading the structures
mesh_names = next(os.walk('./meshes/'))[2]
mesh_names.sort()
step_nmber = len(mesh_names)
meshes = {int(mesh_name[-16:-13]):
          read_ply_property_topomesh('./meshes/' + mesh_name)
          for mesh_name in mesh_names}

simu_name = mesh_names[0][:-27] + '_' + str(step_nmber) + 'steps'

colors = {'relative width': 'r',
          'relative height': 'g',
          'relative surface area': 'b',
          'aspect ratio': 'y',
          'cell number': 'k'}


# -- Computing geometrical properties
def compute_geo_prop_time_evolution(meshes):
    """Computesthe time evolution of some geometrical properties.

    Parameters
    ----------
    meshes : dict
        - keys : Int. time steps
        - values : PropertyTopomesh. The structures to analyse.

    Returns
    -------
    type
        Description of returned object.

    """
    geo_prop = {'relative width': [],
                'relative height': [],
                'relative surface area': [],
                'aspect ratio': [],
                'cell number': []}

    for time_step, mesh in meshes.items():

        vtx_pos = mesh.wisp_property('barycenter', 0).values()

        if 'area' not in mesh.wisp_properties(2).keys():
            compute_topomesh_property(mesh, 'area', 2)

        wdth = vtx_pos[:, 0].max() - vtx_pos[:, 0].min()
        hght = vtx_pos[:, 1].max() - vtx_pos[:, 1].min()
        surf = mesh.wisp_property('area', 2).values().sum()
        cnbr = mesh.nb_wisps(2)

        if time_step == 1:
            init_wdth = wdth
            init_hght = hght
            init_surf = surf

        geo_prop['relative width'].append(wdth / init_wdth)
        geo_prop['relative height'].append(hght / init_hght)
        geo_prop['relative surface area'].append(surf / init_surf)
        geo_prop['cell number'].append(cnbr)
        geo_prop['aspect ratio'].append(hght / wdth)

    return geo_prop


# -- Plotting the properties time evolution
def plot_geo_prop_time_evolution(geo_prop,
                                 prop_name,
                                 color='r',
                                 simu_name=simu_name,
                                 recording_path='./'):
    """Plots and saves the time evolution of a geometrical property.

    Parameters
    ----------
    geo_prop : dict
        - keys : Str. Available geo. prop. names.
        - values : list(Float). The time series of the property values.
    prop_name : Str
        The name of the property to plot. Should be one of these:
            * 'relative width'
            * 'relative height'
            * 'relative surface area'
            * 'cell number'
            * 'aspect ratio'
    color: matplotlib color code
        Optional (default : 'r'). The color to plot with.
    simu_name : Str
        Optional (default : cf simu_name variable above). The name to use to
        write the files.
    recording_path : Str
        Optional (default : cf simu_folder variable above). The location where
        to write the files.

    Returns
    -------
    None

    """
    plt.clf()

    plt.plot(geo_prop[prop_name], label=prop_name, color=color, marker='o')
    plt.xlabel('Time steps', fontsize=14)
    plt.ylabel(prop_name.capitalize(), fontsize=14)
    plt.title('Structure ' + prop_name + ' as a function of time', fontsize=16)

    # -- Saving the graph
    recording_path += 'plots/'
    if not os.path.exists(recording_path):
        os.makedirs(recording_path)

    fig_name = simu_name + '_'+prop_name+'_time_evolution.png'
    plt.savefig(recording_path + fig_name, dpi=300)


# -- Plotting the aspect ratio versus the surface area
def asp_ratio_vs_surf_area(geo_prop,
                           simu_name=simu_name,
                           recording_path='./'):
    """Plots the aspect ratio vs surface area for a time series of tissues.

    Parameters
    ----------
    geo_prop : dict
        - keys : Str. Available geo. prop. names.
        - values : list(Float). The time series of the property values.
    simu_name : Str
        Optional (default : cf simu_name variable above). The name to use to
        write the files.
    recording_path : Str
        Optional (default : cf simu_folder variable above). The location where
        to write the files.

    Returns
    -------
    type
        Description of returned object.

    """
    plt.clf()

    prop_name_x = 'relative surface area'
    prop_name_y = 'aspect ratio'
    plt.scatter(geo_prop[prop_name_x],
                geo_prop[prop_name_y],
                color='c',
                marker='d')

    plt.xlabel(prop_name_x.capitalize(), fontsize=14)
    plt.ylabel(prop_name_y.capitalize(), fontsize=14)
    plt.title(prop_name_y.capitalize() + ' V.S. ' + prop_name_x.capitalize(),
              fontsize=16)
    # plt.show()

    # -- Saving the graph
    recording_path += 'plots/'
    if not os.path.exists(recording_path):
        os.makedirs(recording_path)

    fig_name = simu_name + '_' + prop_name_y + '_vs_' + prop_name_x + '.png'
    plt.savefig(recording_path + fig_name, dpi=300)


if __name__ == '__main__':
    geo_prop = compute_geo_prop_time_evolution(meshes)

    for prop_name in geo_prop.keys():
        plot_geo_prop_time_evolution(geo_prop,
                                     prop_name,
                                     color=colors[prop_name])

    asp_ratio_vs_surf_area(geo_prop)


# analyse_simulation.py ends here.
