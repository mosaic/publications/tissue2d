# tissue2D

## Welcome

* Coordinator(s): O. Ali
* Contributor(s): O. Ali
* Team(s): Mosaic
* Language(s): Python 3.7
* Supported OS: MacOS, Linux
* Licence: see licence file

Package to simulate growth, division and stiffening of 2D plant tissues

## Installation

Installation through conda environment.
```bash
conda create -n tissue2D -c mosaic tissue2d
```

## Requirements

* python 3.7
* numpy
* scipy
* imageio
* matplotlib
* cellcomplex
* artificial_tissue

## Documentation

See wiki.
