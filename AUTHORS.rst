=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* oali, <olivier.ali@inria.fr>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* Olli <olivier.ali@inria.fr>

.. #}
