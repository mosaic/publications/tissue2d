#!/usr/bin/env python
# -*- coding: utf-8 -*-

# {# pkglts, pysetup.kwds
# format setup arguments

from setuptools import setup, find_packages


short_descr = "Package to simulate growth, division and stiffening of 2D plant tissues"
readme = open('README.md').read()
history = open('HISTORY.rst').read()

# find packages
pkgs = find_packages('src')



setup_kwds = dict(
    name='tissue2D',
    version="0.9.0",
    description=short_descr,
    long_description=readme + '\n\n' + history,
    author="oali",
    author_email="olivier.ali@inria.fr",
    url='https://gitlab.inria.fr/mosaic/publications/tissue2D',
    license='cecill-c',
    zip_safe=False,

    packages=pkgs,
    package_dir={'': 'src'},
    setup_requires=[
        ],
    install_requires=[
        ],
    tests_require=[
        ],
    entry_points={},
    keywords='',

    test_suite='nose.collector',
    )
# #}
# change setup_kwds below before the next pkglts tag

# do not change things below
# {# pkglts, pysetup.call
setup(**setup_kwds)
# #}
