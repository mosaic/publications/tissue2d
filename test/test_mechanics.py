# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:

import os
import logging
import sys
import copy
from time import time
import numpy as np
import numpy.linalg as lng
from scipy import optimize as opt
import matplotlib.pyplot as plt

from cellcomplex.utils.array_dict import array_dict
from cellcomplex.property_topomesh.analysis import compute_topomesh_property

from tissue2D.utils.tools import compute_tensor_from_vectors
from tissue2D.utils.mesh_generation import formatting_mesh
from tissue2D.utils.mesh_generation import generate_some_tissues, square_topomesh
from tissue2D.models.mechanics import compute_pressure_force, compute_cellwall_force, compute_mechanical_equilibrium, compute_cell_response_force, MechanicalModel
from tissue2D.visualization.tissue_visualization import draw_vector_field, draw_tissue, draw_tensor_main_directions
from tissue2D.utils.tools import orienting_vertices, compute_intersection_multiple_lines_cell, inflate_structure, compute_position_array_from_mesh, compute_position_array_from_arraydict, compute_tensor_from_vectors, intensity, anisotropy

# -- Tissues generation
square_cell_mesh, hexa_cell_mesh, hexa_grid_mesh, circle_mesh, ellipse_mesh = generate_some_tissues()

meshes = {'square cell': square_cell_mesh, 'hexa cell': hexa_cell_mesh, 'hexa grid': hexa_grid_mesh, 'random disk': circle_mesh}

# # -- Stress computation test
# mesh = square_cell_mesh
#
# force_field = {0: np.array([-1, 0, 0]), # lower left
#                1: np.array([1, 0, 0]),  # lower right
#                2: np.array([1, 0, 0]),  # upper right
#                3: np.array([-1, 0, 0])}  # upper left
#
# mesh.update_wisp_property('force_field', 0, force_field)
#
# stress_tensor = compute_tensor_from_vectors(mesh, force_field)
#
# mesh.update_wisp_property('stress_tensor', 2, {0: stress_tensor})
#
# fig= plt.figure()
# draw_vector_field(mesh, 'force_field', fig=fig)
# draw_tensor_main_directions(mesh, tensor_name='stress_tensor', fig=fig)
# fig.show()
#
# # -- Checking the meshes characteristics:
#
# for mesh_name, mesh in meshes.items():
#     positions  = mesh.wisp_property('barycenter', 0).values()
#     print('----------------------------')
#     print(mesh_name, 'characteristics: ')
#     print('    +-> Number of cells: ...... ', mesh.nb_wisps(2))
#     print('    +-> Number of edges: ...... ', mesh.nb_wisps(1))
#     print('    +-> Number of vertices: ... ', mesh.nb_wisps(0))
#     print('    +-> x - width: ', np.max(positions[:, 0]) - np.min(positions[:,0 ]))
#     print('    +-> y - width: ', np.max(positions[:, 1]) - np.min(positions[:, 1]))
#
#
# # -- Pressure forces visualization
# for mesh_name, mesh in meshes.items():
#     vertex_positions, vid_2_idx, _ = compute_position_array_from_mesh(mesh)
#
#     pressure_force_field = compute_pressure_force(vertex_positions, mesh, vid_2_idx)
#
#     mesh.update_wisp_property('pressure_force_field', 0, pressure_force_field)
#
#     draw_vector_field(mesh, 'pressure_force_field')
#
# # -- Cell wall response forces visualization
# for mesh_name, mesh in meshes.items():#{'hexa grid': hexa_grid_mesh}.items(): #
#
#     vertex_positions, vid_2_idx, _ = compute_position_array_from_mesh(mesh)
#
#     cw_resting_length = {eid: .95 * length for eid, length in mesh.wisp_property('length', 1).items()}
#     cw_stiffness = {eid: 1 for eid in mesh.wisps(1)}
#
#     cell_wall_response_force_field = compute_cellwall_force(vertex_positions, mesh, vid_2_idx, cwll_stiffness_value=cw_stiffness, cw_resting_length_value=cw_resting_length)
#
#     if mesh_name=='hexa grid':
#         print('l.65 - test_mecha - cellwall response forces: ', list(cell_wall_response_force_field.values())[:5])
#
#     mesh.update_wisp_property('cell_wall_response_force_field', 0, cell_wall_response_force_field)
#
#     draw_vector_field(mesh, 'cell_wall_response_force_field')
#
#
#
# # -- Microfibrils response forces visualization
# def test_cmf_response_forces():
#     for mesh_name, mesh in meshes.items():
#         print('working on mesh', mesh_name)
#         vertex_positions, vid_2_idx, _ = compute_position_array_from_mesh(mesh)
#
#         fibers_response_force_field = compute_mcrfbrls_force(vertex_positions, mesh, vid_2_idx)
#         print(fibers_response_force_field[0])
#
#         mesh.update_wisp_property('fibers_response_force_field', 0, fibers_response_force_field)
#
#         draw_vector_field(mesh, 'fibers_response_force_field')
#
# test_cmf_response_forces()
#
# # -- Checking the cellwall_force expression.
# def test_cellwall_force_on_square():
#     """A test function to assess the elastic force expression for cell walls.
#
#     Parameters
#     ----------
#     None.
#
#     Returns
#     -------
#     None.
#     """
#     cwll_stiffness = 1
#     vtx_pos, vid_2_idx, _ = compute_position_array_from_mesh(square_cell_mesh)
#
#
#     # -- Assessing the squareness of the square....
#     edge_lengths = square_cell_mesh.wisp_property('length', 1)
#     mn_edge_lngth = np.mean(edge_lengths)
#     for edge_lngth in edge_lengths.values():
#         assert edge_lngth== mn_edge_lngth
#
#
#     # -- Computing the vertices relative positions
#     cell_center = np.mean(list(square_cell_mesh.wisp_property('barycenter', 0).values()), axis=0)
#     vtx_relat_pos = {vid: vtx_pos - cell_center for vid, vtx_pos in square_cell_mesh.wisp_property('barycenter', 0).items()}
#
#
#     # -- Making the test
#     for shrink_coef in range(10):
#         shrink_coef = 1 - .1*shrink_coef
#
#         cw_rest_lngth = {eid: shrink_coef * length for eid, length
#                                                    in square_cell_mesh.wisp_property('length', 1).items()}
#
#         force_intens_theo_expect = np.sqrt(2)*cwll_stiffness*(1-shrink_coef)*mn_edge_lngth
#
#         cell_wall_response_force_field = compute_cellwall_force(vtx_pos,
#                                                                 square_cell_mesh, vid_2_idx,
#                                                                 cwll_stiffness_value=cwll_stiffness,
#                                                                 cw_resting_length_value=cw_rest_lngth)
#
#         for vid, force in cell_wall_response_force_field.items():
#             # --*-- Asserting the orientation
#             assert np.dot(force, vtx_relat_pos[vid])<=0
#
#             # --*-- Asserting the intensity
#             assert np.abs(lng.norm(force) - force_intens_theo_expect)<1e-3
#
#
#
# # -- Checking the cmfbrls_force expression.
# def test_mcrfbrls_force_on_square():
#     """A test function to assess the elastic force expression for cellulose microfibrils.
#
#     Parameters
#     ----------
#     None.
#
#     Returns
#     -------
#     None.
#     """
#     cmf_density     = 1/5
#     cmf_relative_stiffness   = 1
#     cmf_orientation = np.array([1., 0., 0.])
#
#
#     # -- Assessing the squareness of the square....
#     edge_lengths = square_cell_mesh.wisp_property('length', 1)
#     mn_edge_lngth = np.mean(edge_lengths)
#     for edge_lngth in edge_lengths.values():
#         assert edge_lngth== mn_edge_lngth
#
#
#     # -- Computing the vertices relative positions and cell geometrical characteristics
#     vtx_pos_dict  = np.array(square_cell_mesh.wisp_property('barycenter', 0).values())
#     cell_center   = np.mean(vtx_pos_dict, axis=0)
#     cell_width    = np.max(vtx_pos_dict[:, 0]) - np.min(vtx_pos_dict[:, 0])
#     vtx_relat_pos = {vid: vtx_pos - cell_center for vid, vtx_pos in square_cell_mesh.wisp_property('barycenter', 0).items()}
#
#     vtx_pos, vid_2_idx, _ = compute_position_array_from_mesh(square_cell_mesh)
#
#     # --*-- Checking the number of fibers
#     cell_radius = max(map(lambda x: lng.norm(x), vtx_pos_dict - cell_center))
#     lines_nmber = int(cell_radius * cmf_density) + 1
#     assert lines_nmber==1
#
#
#     # -- Making the test
#     for shrink_coef in range(10):
#         shrink_coef = 1 - .1*shrink_coef
#
#         cmf_rest_lngth = shrink_coef * cell_width
#
#         force_intens_theo_expect = 1/2 * cmf_relative_stiffness * (1 - shrink_coef) * cell_width
#
#         cmf_force_field = compute_mcrfbrls_force(vtx_pos, square_cell_mesh, vid_2_idx,
#                                                  cmf_relative_stiffness     =cmf_relative_stiffness,
#                                                  cmf_resting_length=cmf_rest_lngth,
#                                                  cmf_orientation   =cmf_orientation,
#                                                  cmf_density       =cmf_density)
#
#         for vid, force in cmf_force_field.items():
#             # --*-- Asserting the orientation
#             assert np.dot(force, vtx_relat_pos[vid])<=0
#
#             # --*-- Asserting the intensity
#             assert np.abs(lng.norm(force) - force_intens_theo_expect)<1e-3
#
# test_mcrfbrls_force_on_square()


# # -- Testing all the forces together on a Voronoi tissues
# mechanical_parameters = {'pressure'      : .1,
#                          'cwll_stiffness_ratio':  1.,
#                          'cmf_relative_stiffness': 1e-3,
#                          'cmf_orientation_method': 0}
#
# initial_mesh = hexa_cell_mesh #circle_mesh

# vtx_pos_2nD, vid_2_idx, idx_2_vid = compute_position_array_from_mesh(initial_mesh)
# cmf_density, cmf_direction, cmf_resting_length, cmf_position_interpolation = preprocess_mcrfbrls_forces(vtx_pos_2nD, initial_mesh, vid_2_idx, cmf_orientation_method=mechanical_parameters['cmf_orientation_method'])
#
# mcrfbrls_forces   = compute_mcrfbrls_force(vtx_pos_2nD, initial_mesh,
#                                                         vid_2_idx,
#                                                         cmf_density,
#                                                         cmf_direction,
#                                                         cmf_resting_length,
#                                                         cmf_position_interpolation,
#                                                         cmf_relative_stiffness=mechanical_parameters['cmf_relative_stiffness'])
#
#
# initial_mesh.update_wisp_property('fibers_response_force_field', 0, mcrfbrls_forces)
#
# draw_vector_field(initial_mesh, 'fibers_response_force_field')
#
#
# for cmf_forces in [False]: #, True
#     final_mesh, stress_field, vtx_displacement_field = find_mechanical_equilibrium(initial_mesh, include_cmf_forces=cmf_forces,
#                                                                                    factor=0.1,
#                                                                                    mechanical_parameters=mechanical_parameters)


# -- Testing the MechanicalModel Class.
# --*-- Setting the mechanical parameters

cwll_stiffness_ratio = 3.

mechanical_parameters = {'pressure'      : .1,
                         'cwll_stiffness_ratio':  cwll_stiffness_ratio}


# --*-- Instancing the model
mecha_model = MechanicalModel()

circle_mesh = hexa_grid_mesh

new_resting_lengths = {eid: .8*rest_lngth for eid, rest_lngth in circle_mesh.wisp_property('resting_length', 1).items()}

circle_mesh.update_wisp_property('resting_length', 1, new_resting_lengths)

formatting_mesh(circle_mesh, cwll_stiffness_ratio=cwll_stiffness_ratio)

mecha_model.set_mesh(circle_mesh)

mecha_model.set_parameters(mechanical_parameters)

mecha_model.run(t_max=2)

mesh_ini = mecha_model._mesh
stress_fld = mesh_ini.wisp_property('stress_tensor', 2)
stress_int = {eid: intensity(strs)  for eid, strs in stress_fld.items()}
stress_ani = {eid: anisotropy(strs) for eid, strs in stress_fld.items()}

mesh_ini.update_wisp_property('stress_intensity' , 2, stress_int)
mesh_ini.update_wisp_property('stress_anisotropy', 2, stress_ani)

elastic_force   = {eid: mesh_ini.wisp_property('cwll_stiffness', 1)[eid]
                      * mesh_ini.wisp_property('edge_stretch', 1)[eid]
                    for eid in mesh_ini.wisps(1)}
mesh_ini.update_wisp_property('elastic_force', 1, elastic_force)

visu_param = {'back_ground_color': 'white',
              'field_of_view'    : 'automatic',
              'cell_property'    : 'stress_anisotropy',
              'cell_colormap'    : 'viridis',
              'cell_alpha'       : .5,
              'edge_property'    : 'cwll_stiffness',
              'edge_property_2'  : 'elastic_force',
              'edge_thickness'   : 2,
              'edge_color'       : 'r'}


fig  = plt.figure()
draw_tissue(mesh_ini,
            visualization_parameters=visu_param,
            fig=fig,
            show_stress_orientation=False)

draw_tensor_main_directions(mesh_ini, tensor_name='stress_tensor',
                                      fig=fig,
                                      color='g')

plt.gca().set_aspect('equal', adjustable='box')

plt.show()

#
# # -- Computing mechanical equilibrium
# # --*-- For the square cell
# square_cell_mesh_final, _, _ = find_mechanical_equilibrium(square_cell_mesh, factor=.1)
#
# # --*-- For the hexagonal cell
# custom_cwll_stiffness = {eid: 2 for eid in hexa_cell_mesh.wisps(1)}
# mechanical_parameters = {'pressure'      : 1,
#                          'cwll_stiffness': custom_cwll_stiffness}
#
# hexa_cell_mesh_final, _, _ = find_mechanical_equilibrium(hexa_cell_mesh,
#                                                          factor=.9,
#                                                          mechanical_parameters=mechanical_parameters)
#
# draw_tissue([hexa_cell_mesh_final, hexa_cell_mesh ])
#
# # --*-- For a tissue composed by hexagonal cells
# # --*--*-- Setting the mechanical parameters
# custom_cwll_stiffness = {}
# for eid, edge_type in hexa_grid_mesh.wisp_property('edge_type', 1).items():
#     if edge_type=='outer':
#         custom_cwll_stiffness[eid] = 3
#     else:
#         custom_cwll_stiffness[eid] = 1
#
# mechanical_parameters = {'pressure'      : .1,
#                          'cwll_stiffness':  custom_cwll_stiffness}
#
# # --*--*-- Solving the mechanical equilibrium
# hexa_grid_mesh_final, stress_field, vtx_displacement_field = find_mechanical_equilibrium(hexa_grid_mesh,
#                                                                                          factor=.1,
#                                                                                          mechanical_parameters=mechanical_parameters)
#
# draw_tissue([hexa_grid_mesh_final, hexa_grid_mesh])
#
#
#
#


#
## test_mechanics.py ends here.
