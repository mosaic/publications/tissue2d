# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:

import copy
from time import time
import numpy as np
import random

from cellcomplex.property_topomesh.io import read_ply_property_topomesh, save_ply_property_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property

import tissue2D
from tissue2D.utils.mesh_generation import generate_some_tissues, formatting_mesh
from tissue2D.utils.tools import compute_current_edge_lengths
from tissue2D.models.division import CellDivisionModel
from tissue2D.models.growth import GrowthModel
from tissue2D.models.mechanics import MechanicalModel
from tissue2D.models.master import Tissue2DModel
from tissue2D.visualization.tissue_visualization import draw_vector_field, draw_tissue, draw_tensor_main_directions



# -- Tissues generation
square_cell_mesh, hexa_cell_mesh, hexa_grid_mesh, circle_mesh, ellipse_mesh = generate_some_tissues()


# -- Tissue reading from saved files
path_2_mesh   = tissue2D.__path__[0]+"/../../"+'/data/mesh_examples/ellipse_ratio2_mesh.ply'


computable_topomesh_properties = {0: ['barycenter'],
                                  1: ['length', 'borders', 'vertices'],
                                  2: ['barycenter', 'vertices', 'oriented_borders', 'oriented_vertices', 'area']}

properties_2_save  = {degree: [] for degree in range(4)}
for degree in range(4):
    for mesh_property in ellipse_mesh.wisp_properties(degree).keys():
        if mesh_property not in computable_topomesh_properties[degree]:
            properties_2_save[degree].append(mesh_property)


save_ply_property_topomesh(ellipse_mesh, path_2_mesh, properties_2_save)





# -- Testing the master model with mechanics, growth, division and stiffening
default_processes = {'mechanics': True,
                     'growth': True,
                     'cell division': True,
                     'cell wall stiffening': True}

def test_master_model_with_mechanics_growth_division_and_stiffening(input_mesh,
                                                                    step_number=5,
                                                                    processes=default_processes,
                                                                    orientation_method='along_max_stress'):
    # --*-- Setting the visualization parameters
    visualization_parameters = {'back_ground_color': 'black',
                                'field_of_view'    : 10,
                                'cell_property'    : 'area',
                                'cell_colormap'    : 'viridis',
                                'cell_alpha'       : .0,
                                'edge_property'    : 'cwll_stiffness',
                                'edge_property_2'  : None, #'edge_stretch',
                                'edge_thickness'   : 1,
                                'edge_color'       : 'r'}

    # --*-- Setting the mechanical parameters
    mechanical_parameters = {'pressure'      : .1,
                             'cwll_stiffness_ratio':  3.,
                             'cmf_relative_stiffness': 1e-3,
                             'cmf_orientation_method': 0}

    include_cmf_forces = False

    # --*-- Setting the growth parameters
    growth_rate        = .2
    vtx_dsplcmnt_field = {vid: np.zeros(3) for vid in input_mesh.wisps(0)}

    # --*-- Setting the division parameters
    division_threshold = 2 * np.mean(input_mesh.wisp_property('area',2).values())
    # orientation_method = 'along_max_stress' #'random' #'along_max_stress'

    # --*-- Setting the cell wall stiffening Parameters
    stiffening_rate = .5

    # draw_tissue(input_mesh, visualization_parameters=visualization_parameters)

    # --*-- Instancing the model
    tissue2D_model = Tissue2DModel()

    tissue2D_model.set_mesh(input_mesh, mesh_name='ellipse_1.2_voronoi')

    tissue2D_model.set_processes(mechanics =processes['mechanics'],
                                 growth    =processes['growth'],
                                 division  =processes['cell division'],
                                 stiffening=processes['cell wall stiffening'])

    tissue2D_model.set_visualization_parameters(visualization_each_step=True,
                                                visualization_parameters=visualization_parameters,
                                                show_stress_orientation=True,
                                                save_figure=True,
                                                save_simulation=True)

    tissue2D_model.set_mechanical_parameters(mechanical_parameters,
                                             include_cmf_forces=False,
                                             factor=.1)

    tissue2D_model.set_growth_parameters(vtx_dsplcmnt_field, growth_rate)

    tissue2D_model.set_division_parameters(division_threshold, orientation_method)

    tissue2D_model.set_stiffening_parameters(stiffening_rate, include_cmf_forces)

    tissue2D_model.run(t_min=0, t_max=step_number-1, dt=1)




# -- Calling the test
path_2_mesh   = '/Users/oali/Documents/Work/Research/Devlp/tissue2D/data/mesh_examples/ellipse_mesh.ply'
recorded_ellipse_mesh = read_ply_property_topomesh(path_2_mesh)
for prop_name in ['oriented_vertices', 'oriented_borders']:
    if prop_name not in recorded_ellipse_mesh.wisp_properties(2).keys():
        compute_topomesh_property(recorded_ellipse_mesh, prop_name, 2)

compute_topomesh_property(recorded_ellipse_mesh, 'length', 1)
compute_topomesh_property(recorded_ellipse_mesh, 'vertices', 1)
compute_topomesh_property(recorded_ellipse_mesh, 'area', 2)
compute_topomesh_property(recorded_ellipse_mesh, 'barycenter', 2)
formatting_mesh(recorded_ellipse_mesh)

# processes = {'mechanics'           : True,
#              'growth'              : True,
#              'cell division'       : False,
#              'cell wall stiffening': True}
#
# cell_div_orientation_methods = ['random', 'along_max_stress']
# number_of_steps = 30
# test_master_model(recorded_ellipse_mesh,
#                   number_of_steps,
#                   processes,
#                   orientation_method=cell_div_orientation_methods[1])
#




# -- Systematic analysis
for condition in [(False, 'random'),
                  (True, 'random'),
                  (True, 'along_max_stress')]:

    # --*-- Starting from  the same tissue each time
    path_2_mesh   = '/Users/oali/Documents/Work/Research/Devlp/tissue2D/data/mesh_examples/ellipse_mesh.ply'
    recorded_ellipse_mesh = read_ply_property_topomesh(path_2_mesh)
    for prop_name in ['oriented_vertices', 'oriented_borders']:
        if prop_name not in recorded_ellipse_mesh.wisp_properties(2).keys():
            compute_topomesh_property(recorded_ellipse_mesh, prop_name, 2)

    compute_topomesh_property(recorded_ellipse_mesh, 'length', 1)
    compute_topomesh_property(recorded_ellipse_mesh, 'vertices', 1)
    compute_topomesh_property(recorded_ellipse_mesh, 'area', 2)
    compute_topomesh_property(recorded_ellipse_mesh, 'barycenter', 2)
    formatting_mesh(recorded_ellipse_mesh)

    # --*-- Simulation begins here
    print('=============================')
    print('=============================')
    if condition:
        print('Simulation with cell division')
        print('... with orientation ', condition[1])
    else:
        print('Simulation without cell division')
    print('=============================')
    print('=============================')

    number_of_steps = 30
    processes = {'mechanics'           : True,
                 'growth'              : True,
                 'cell division'       : condition[0],
                 'cell wall stiffening': True}

# -- Calling the test
path_2_mesh   = '/Users/oali/Documents/Work/Research/Devlp/tissue2D/data/mesh_examples/ellipse_mesh.ply'
recorded_ellipse_mesh = read_ply_property_topomesh(path_2_mesh)
for prop_name in ['oriented_vertices', 'oriented_borders']:
    if prop_name not in recorded_ellipse_mesh.wisp_properties(2).keys():
        compute_topomesh_property(recorded_ellipse_mesh, prop_name, 2)

compute_topomesh_property(recorded_ellipse_mesh, 'length', 1)
compute_topomesh_property(recorded_ellipse_mesh, 'vertices', 1)
compute_topomesh_property(recorded_ellipse_mesh, 'area', 2)
compute_topomesh_property(recorded_ellipse_mesh, 'barycenter', 2)
formatting_mesh(recorded_ellipse_mesh)

processes = {'mechanics'           : True,
             'growth'              : False,
             'cell division'       : False,
             'cell wall stiffening': False}

orientation_methods = ['random', 'along_max_stress']
number_of_steps = 1

test_master_model_with_mechanics_growth_division_and_stiffening(recorded_ellipse_mesh,
                                                                number_of_steps,
                                                                processes,
                                                                orientation_method=orientation_methods[1])



#
## test_master.py ends here.
