# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:

import copy
from time import time
import numpy as np
import random

from tissue2D.utils.mesh_generation import generate_some_tissues
from tissue2D.utils.tools import compute_current_edge_lengths
from tissue2D.models.division import divide_one_cell, update_lineage, divide_several_cells, CellDivisionModel
from tissue2D.visualization.tissue_visualization import draw_vector_field, draw_tissue




# -- Tissues generation
square_cell_mesh, hexa_cell_mesh, hexa_grid_mesh, circle_mesh = generate_some_tissues()



# -- Testing the division of a square cell
def test_division_on_square_cell():
    old_square = square_cell_mesh
    new_square = copy.deepcopy(old_square)

    cid = list(old_square.wisps(2))[0]
    threshold = 0
    direction = np.array([1., 0., 0.])

    divide_one_cell(old_square, new_square,
                                cid=cid,
                                threshold=threshold,
                                direction=direction)


    expected_new_wisp_ids ={0: [0, 1, 2, 3, 4, 5],
                            1: [0, 3, 4, 5, 6, 7, 8],
                            2: [1, 2]}
    for degree in range(3):
        # -- Checking the wisps of the new mesh.
        assert set(new_square.wisps(degree))==set(expected_new_wisp_ids[degree])

        # -- Checking the properties of the new mesh.
        if degree in [1, 2]:
            assert 'lineage_mother_id' in new_square.wisp_properties(degree).keys()

        expected_properties = np.concatenate([list(old_square.wisp_properties(degree).keys()),
                                              ['lineage_mother_id']])
        set(new_square.wisp_properties(degree).keys())==set(expected_properties)

    # -- Checking the lineage
    expected_edge_lineage = {4: -1, 5: 1, 6: 1, 7: 2, 8: 2}


    for degree in range(1, 3):
        new_wids = set(new_square.wisps(degree)) - set(old_square.wisps(degree))

        if degree==1:
            assert set(new_wids)==set(expected_edge_lineage.keys())

            for new_wid in new_wids:
                assert new_square.wisp_property('lineage_mother_id', degree)[new_wid]==expected_edge_lineage[new_wid]

        if degree==2:
            assert set(new_wids)==set([1, 2])
            assert list(new_square.wisp_property('lineage_mother_id', degree).values())[0]==0

    draw_tissue(new_square)

test_division_on_square_cell()

# -- Test on an hexa cell
def test_division_one_hexa_cell_within_hexa_tissue():
    old_hexa = copy.deepcopy(hexa_grid_mesh)

    new_hexa = copy.deepcopy(old_hexa)

    cid = list(old_hexa.wisps(2))[3]
    threshold = 0
    angle = np.pi/180 * 54
    direction = np.array([ np.cos(angle), np.sin(angle),0.])

    divide_one_cell(old_hexa, new_hexa, cid, threshold, direction=direction)

    new_eids = set(new_hexa.wisps(1)) - set(old_hexa.wisps(1))

    draw_tissue(new_hexa)

test_division_one_hexa_cell_within_hexa_tissue()

# -- Test one cell division on an voronoi tissue
def test_division_one_cell_within_voronoi_tissue():
    old_voronoi = copy.deepcopy(circle_mesh)

    new_voronoi = copy.deepcopy(circle_mesh)

    cid = 11 #random.choice(list(old_voronoi.wisps(2)))
    threshold = 0
    angle = np.pi/180*90#random.uniform(0, np.pi)
    direction = np.array([ np.cos(angle), np.sin(angle),0.])

    divide_one_cell(old_voronoi, new_voronoi, cid, threshold, direction=direction)

    new_eids = set(new_voronoi.wisps(1)) - set(old_voronoi.wisps(1))

    draw_tissue(old_voronoi)
    draw_tissue(new_voronoi)

    for eid, cwll_stiff in new_voronoi.wisp_property('cwll_stiffness', 1).items():
        if eid not in old_voronoi.wisps(1):
            print(eid, cwll_stiff, new_voronoi.wisp_property('edge_type', 1)[eid])
            print(eid, list(new_voronoi.borders(1,eid)), new_voronoi.wisp_property('lineage_mother_id', 1)[eid])

test_division_one_cell_within_voronoi_tissue()

# -- Test multiple cell divisions
def test_multiple_divisions_within_voronoi_tissue():
    old_mesh  = copy.deepcopy(circle_mesh)
    threshold = 0

    new_mesh = divide_several_cells(old_mesh, threshold, orientation_method='random')

    draw_tissue(old_mesh)
    draw_tissue(new_mesh)

    for degree in range(3):
        print(new_mesh.wisp_properties(degree).keys())
        for prop_name in new_mesh.wisp_properties(degree).keys():
            print(' Number of wisps: ', new_mesh.nb_wisps(degree), ' | Number of keys for ', prop_name, ' : ', len(new_mesh.wisp_property(prop_name, degree)))

    for eid, cwll_stiff in new_mesh.wisp_property('cwll_stiffness', 1).items():
        if eid not in old_mesh.wisps(1):
            print(eid, cwll_stiff, new_mesh.wisp_property('edge_type', 1)[eid])
test_multiple_divisions_within_voronoi_tissue()



# -- Test the CellDivisionModel:
# --*-- Setting the division parameters
division_threshold = 1
orientation_method = 'random'


draw_tissue(circle_mesh)
# --*-- Instancing the model
division_model = CellDivisionModel()

division_model.set_mesh(circle_mesh)
division_model.set_parameters(division_threshold, orientation_method)

division_model.run()

circle_mesh_final = division_model._mesh

draw_tissue(circle_mesh_final)


#
## test_division.py ends here.
