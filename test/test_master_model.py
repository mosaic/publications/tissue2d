# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:

import unittest
import copy
from time import time
import numpy as np
import random

from cellcomplex.property_topomesh.io import read_ply_property_topomesh, save_ply_property_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property

import tissue2D

from tissue2D.utils.mesh_generation import generate_some_tissues, formatting_mesh
from tissue2D.models.master import Tissue2DModel, MECHA_PARAM_DEFAULT, VISU_PARAM_DEFAULT


class TestTissue2DModel(unittest.TestCase):

    def setUp(self):
        # -- Tissues generation

        path_2_mesh = tissue2D.__path__[0] + "/../../" + '/data/mesh_examples/ellipse_ratio2_mesh.ply'
        self.mesh = read_ply_property_topomesh(path_2_mesh)

        computable_topomesh_properties = {0: ['barycenter'],
                                          1: ['length', 'borders', 'vertices'],
                                          2: ['barycenter', 'vertices', 'oriented_borders', 'oriented_vertices', 'area']}

        properties_2_save  = {degree: [] for degree in range(4)}
        for degree in range(4):
            for mesh_property in self.mesh.wisp_properties(degree).keys():
                if mesh_property not in computable_topomesh_properties[degree]:
                    properties_2_save[degree].append(mesh_property)

        for prop_name in ['oriented_vertices', 'oriented_borders']:
            if prop_name not in self.mesh.wisp_properties(2).keys():
                compute_topomesh_property(self.mesh, prop_name, 2)

        compute_topomesh_property(self.mesh, 'length', 1)
        compute_topomesh_property(self.mesh, 'vertices', 1)
        compute_topomesh_property(self.mesh, 'area', 2)
        compute_topomesh_property(self.mesh, 'barycenter', 2)
        formatting_mesh(self.mesh)


        # -- Testing the master model with mechanics, growth, division and stiffening
        self.default_processes = {'mechanics': True,
                             'growth': True,
                             'cell division': True,
                             'cell wall stiffening': True}

        # --*-- Setting the visualization parameters
        self.visualization_parameters = VISU_PARAM_DEFAULT

        # --*-- Setting the mechanical parameters
        self.mechanical_parameters = MECHA_PARAM_DEFAULT

        self.include_cmf_forces = False

        # --*-- Setting the growth parameters
        self.growth_rate = .2
        self.growth_threshold = 0

        # --*-- Setting the division parameters
        self.division_threshold = 2 * np.mean(self.mesh.wisp_property('area', 2).values())
        # orientation_method = 'along_max_stress' #'random' #'along_max_stress'

        # --*-- Setting the cell wall stiffening Parameters
        self.stiffening_rate = .5
        self.orientation_method = 'along_max_stress'

        self.step_number = 1
        self.processes = self.default_processes

        # --*-- Instancing the model
        self.tissue2D_model = Tissue2DModel()

    def tearDown(self):
        pass

    def test_model_set_mesh(self):
        self.tissue2D_model.set_mesh(self.mesh, mesh_name='circle')

    def test_model_run(self):

        self.tissue2D_model.set_mesh(self.mesh, mesh_name='circle')

        self.tissue2D_model.set_processes(mechanics =self.processes['mechanics'],
                                          growth    =self.processes['growth'],
                                          division  =self.processes['cell division'],
                                          stiffening=self.processes['cell wall stiffening'])

        self.tissue2D_model.set_mechanical_parameters(self.mechanical_parameters,
                                                      factor=.1)

        self.tissue2D_model.set_growth_parameters(self.growth_rate,self.growth_threshold)

        self.tissue2D_model.set_division_parameters(self.division_threshold, self.orientation_method)

        self.tissue2D_model.set_stiffening_parameters(self.stiffening_rate, self.include_cmf_forces)

        self.tissue2D_model.run(t_min=0, t_max=self.step_number-1, dt=1)


#
## test_master.py ends here.