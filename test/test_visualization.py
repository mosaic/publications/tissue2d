# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:
from time import time
import numpy as np
import numpy.linalg as lng
import matplotlib as mpl
import matplotlib.gridspec as gs
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
from cellcomplex.property_topomesh.utils.matplotlib_tools import mpl_draw_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.io import save_ply_property_topomesh, read_ply_property_topomesh

from tissue2D.utils.tools import max_eigvect, min_eigvect, intensity, anisotropy
from tissue2D.visualization.quantitative_visualization import plot_simulation_characteristics, plot_tissue_property_time_evolution, plot_tissue_properties_time_evolution


# -- Open one mesh
data_fld_path = '/Users/oali/Documents/Work/Research/Devlp/tissue2D/data/results/'
sim_spec_path = '190417_triplet_30steps/190417_ellipse_1.2_voronoi_randomDiv/meshes/'
rec_data_path = data_fld_path + sim_spec_path[:-7]

step_number   = 20

for step in range(0, step_number):
    ply_mesh_name = 'ellipse_1.2_voronoi_step'+str(step_number+1)+'_over_'+str(step_number)+'.ply'
    ply_mesh_path = data_fld_path + sim_spec_path + ply_mesh_name
    mesh = read_ply_property_topomesh(ply_mesh_path)



    # -- Adding useful properties to the mesh
    edge_ids       = list(mesh.wisps(1))
    cwll_stiffness = mesh.wisp_property('cwll_stiffness', 1).values(edge_ids)
    if 'length' not in mesh.wisp_properties(1).keys():
        compute_topomesh_property(mesh, 'length', 1)
    edge_stretch   = {eid: mesh.wisp_property('length', 1)[eid]
                         - mesh.wisp_property('resting_length', 1)[eid]
                        for eid in mesh.wisps(1)}
    mesh.update_wisp_property('edge_stretch', 1, edge_stretch)

    cwll_force_intensity = {eid: stiffness * stretch
                            for eid, stiffness, stretch in zip(edge_ids,
                                                               cwll_stiffness,
                                                               edge_stretch)}
    mesh.update_wisp_property('cwll_force_intensity', 1, cwll_force_intensity)

    grwth_tsr = mesh.wisp_property('growth_tensor', 2)
    grwth_int = {cid: intensity( g_tsr) for cid, g_tsr in grwth_tsr.items()}
    grwth_ani = {cid: anisotropy(g_tsr) for cid, g_tsr in grwth_tsr.items()}

    for prop_name, prop_dic in {'growth_intensity' : grwth_int,
                                'growth_anisotropy': grwth_ani}.items():
        mesh.update_wisp_property(prop_name, 2, prop_dic)



    # -- Plotting begins
    visu_param = {'back_ground_color': 'black',
                  'field_of_view'    : 'automatic',
                  'cell_property'    : 'stress_intensity',
                  'cell_property_2'  : 'growth_intensity',
                  'cell_colormap'    : 'viridis',
                  'cell_colormap_2'  : 'inferno',
                  'cell_alpha'       : .75,
                  'cell_alpha_2'     : .75,
                  'edge_property'    : 'cwll_stiffness',
                  '2nd_edge_property': 'edge_stretch',
                  'edge_thickness'   : 2,
                  'edge_color'       : 'w'}

    plot_simulation_characteristics(mesh, visu_param=visu_param,
                                      mesh_name=ply_mesh_name[:-4],
                                      save_figure=True, save_path=rec_data_path)



# -- Geometry properties tracking
data_fld_path = '/Users/oali/Documents/Work/Research/Devlp/tissue2D/data/results/190417_triplet_30steps/'


sim_spec_paths = ['190417_ellipse_1.2_voronoi_stressBasedDiv/meshes/',
                  '190417_ellipse_1.2_voronoi_noCellDiv/meshes/',
                  '190417_ellipse_1.2_voronoi_randomDiv/meshes/']

# --*-- Opening all the meshes
meshes = {}
for sim_spec_path in sim_spec_paths:
    sim_name = sim_spec_path[34:-8]
    print(sim_name)
    meshes[sim_name] = []
    for step in range(1, step_number):
        ply_mesh_name = 'ellipse_1.2_voronoi_step' + str(step + 1) + '_over_' + str(step_number) + '.ply'
        ply_mesh_path = data_fld_path + sim_spec_path + ply_mesh_name
        meshes[sim_name].append(read_ply_property_topomesh(ply_mesh_path))

# --*-- Plotting all the properties together
plot_tissue_properties_time_evolution(meshes, save_figure=True,
                                              save_path=data_fld_path)


# # --*-- Plotting only one property
# properties = ['aspect_ratio', 'surface_area', 'cell_number', 'orientation']
# property_2_plot = properties[3]
# fig=plt.figure()
# style = {'stressBasedDiv': ['b', 'o'],
#          'randomDiv': ['r', '^'],
#          'noCellDiv': ['g', 's']}
# save_condition = False
# for sim_name, meshes_list in meshes.items():
#     if sim_name=='noCellDiv':
#         save_condition = True
#     plot_tissue_property_time_evolution(meshes_list,
#                                         property=property_2_plot,
#                                         meshes_name=sim_name,
#                                         color=style[sim_name][0],
#                                         marker=style[sim_name][1],
#                                         fig=fig,
#                                         save_figure=save_condition)
# plt.show()


#
## test_visualization.py ends here.
