# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:

import os
import logging
import sys
import copy
from time import time
import numpy as np
import numpy.linalg as lng

from cellcomplex.utils.array_dict import array_dict
from cellcomplex.property_topomesh.analysis import compute_topomesh_property

from tissue2D.utils.tools import compute_tensor_from_vectors
from tissue2D.utils.mesh_generation import generate_some_tissues, square_topomesh
from tissue2D.models.stiffening import compute_cell_wall_stiffening, StiffeningModel
from tissue2D.models.mechanics import find_mechanical_equilibrium, MechanicalModel
from tissue2D.visualization.tissue_visualization import draw_vector_field, draw_tissue
from tissue2D.utils.tools import orienting_vertices, compute_intersection_multiple_lines_cell, inflate_structure, compute_position_array_from_mesh, compute_position_array_from_arraydict

# -- Tissues generation
square_cell_mesh, hexa_cell_mesh, hexa_grid_mesh, circle_mesh = generate_some_tissues()

meshes = {'square cell': square_cell_mesh, 'hexa cell': hexa_cell_mesh, 'hexa grid': hexa_grid_mesh, 'random disk': circle_mesh}

# -- Checking the meshes characteristics:

for mesh_name, mesh in meshes.items():
    positions  = mesh.wisp_property('barycenter', 0).values()
    print('----------------------------')
    print(mesh_name, 'characteristics: ')
    print('    +-> Number of cells: ...... ', mesh.nb_wisps(2))
    print('    +-> Number of edges: ...... ', mesh.nb_wisps(1))
    print('    +-> Number of vertices: ... ', mesh.nb_wisps(0))
    print('    +-> x - width: ', np.max(positions[:, 0]) - np.min(positions[:,0 ]))
    print('    +-> y - width: ', np.max(positions[:, 1]) - np.min(positions[:, 1]))



# -- Testing the stiffening law.
def test_cwll_stiffening():
    initial_mesh = circle_mesh

    # --*-- Computing a stress field first
    mechanical_parameters = {'pressure'      : .1,
                             'cwll_stiffness_ratio':  3.,
                             'cmf_relative_stiffness': 1e-3,
                             'cmf_orientation_method': 0}

    final_mesh, stress_field, vtx_displacement_field = find_mechanical_equilibrium(initial_mesh,
                                                                                   mechanical_parameters=mechanical_parameters)

    final_mesh.update_wisp_property('stress_tensor', 2, stress_field)


    new_cwll_stiffness = compute_cell_wall_stiffening(final_mesh)

    for eid, old_stiffness in final_mesh.wisp_property('cwll_stiffness', 1).items():
        print(eid, old_stiffness, new_cwll_stiffness[eid])

test_cwll_stiffening()


# -- Testing the StiffnenigModel class
# --*-- First computing the mechanical equilibrium
# --*--*-- Setting the mechanical parameters
mechanical_parameters = {'pressure'      : .1,
                         'cwll_stiffness_ratio':  3.}

# --*--*-- Instancing the model
mecha_model = MechanicalModel()

mecha_model.set_mesh(circle_mesh)
mecha_model.set_parameters(mechanical_parameters, include_cmf_forces=False,
                                                  factor=.1)

mecha_model.run()

first_mesh = mecha_model._mesh

# --*-- Then the stiffening model per se
stiffen_model = StiffeningModel()

stiffen_model.set_mesh(first_mesh)
stiffen_model.set_parameters(stiffening_rate=.5, include_cmf_forces=False)

stiffen_model.run(dt=1)

final_mesh = stiffen_model._mesh






#
## test_stiffening.py ends here.
