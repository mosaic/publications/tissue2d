# --*-- coding: utf-8 --*--
# --*-- python --*--
#
# @Author: Olivier Ali  <oali>
# @Date:   2019-02-25T07:16:42+01:00
# @Email:  olivier.ali@inria.fr
# @Last modified by:
# @Last modified time:

import copy
from time import time
import numpy as np
import numpy.linalg as lng

from tissue2D.utils.mesh_generation import generate_some_tissues
from tissue2D.utils.tools import compute_current_edge_lengths
from tissue2D.models.growth import compute_new_resting_lengths, compute_grown_positions, update_grown_property_topomesh, grow_property_topomesh, GrowthModel
from tissue2D.models.mechanics import find_mechanical_equilibrium
from tissue2D.visualization.tissue_visualization import draw_vector_field, draw_tissue




# -- Tissues generation
square_cell_mesh, hexa_cell_mesh, hexa_grid_mesh, circle_mesh = generate_some_tissues()




# -- new edge resting lengths computation
meshes = [hexa_cell_mesh, hexa_grid_mesh, circle_mesh]

for growth_factor in [0., .5, 1.]: # growth_factor should be within [0, 1]
    for mesh in meshes:
        old_rest_lngth = mesh.wisp_property('resting_length', 1)
        cur_edge_lngth = compute_current_edge_lengths(mesh)
        new_rest_lngth = compute_new_resting_lengths(mesh, growth_factor)

        for eid in mesh.wisps(1):
            assert new_rest_lngth[eid]==(old_rest_lngth[eid] + growth_factor * (cur_edge_lngth[eid] - old_rest_lngth[eid]))




# -- New resting positions computation after a mechanical equilibrium estimation
# --*--*-- Setting the mechanical parameters
custom_cwll_stiffness = {}
for eid, edge_type in hexa_grid_mesh.wisp_property('edge_type', 1).items():
    if edge_type=='outer':
        custom_cwll_stiffness[eid] = 3
    else:
        custom_cwll_stiffness[eid] = 1

mechanical_parameters = {'pressure'      : .1,
                         'cwll_stiffness':  custom_cwll_stiffness}

# --*--*-- Solving the mechanical equilibrium
hexa_grid_mesh_final, stress_field, vtx_displacement_field = find_mechanical_equilibrium(hexa_grid_mesh,
                                                                                         factor=.1,
                                                                                         mechanical_parameters=mechanical_parameters)

# --*--*-- Making the structure grow
growth_factor = .5
grown_hexa_grid_mesh = grow_property_topomesh(hexa_grid_mesh, vtx_displacement_field, growth_factor= growth_factor)

# --*--*-- Visualizing the results
draw_tissue([grown_hexa_grid_mesh, hexa_grid_mesh])




# -- Test several growth steps in a row
# mesh = copy.deepcopy(hexa_grid_mesh)
mesh = copy.deepcopy(circle_mesh)
nber_growth_steps = 5

# --*--*-- Setting the mechanical parameters
custom_cwll_stiffness = {}
for eid, edge_type in mesh.wisp_property('edge_type', 1).items():
    if edge_type=='outer':
        custom_cwll_stiffness[eid] = 3
    else:
        custom_cwll_stiffness[eid] = 1

mechanical_parameters = {'pressure'      : .1,
                         'cwll_stiffness':  custom_cwll_stiffness}

growth_factor = .5

# --*-- Simulation loop
start_time = time()

for time_step in range(nber_growth_steps):
    print('time step #', time_step, ' over ', nber_growth_steps)
    draw_tissue(mesh, save_figure=True)

    mesh_equi_mecha, stress_field, vtx_dsplcmnt_field = find_mechanical_equilibrium(mesh,
                                                                                    factor=.1,
                                                                                    mechanical_parameters=mechanical_parameters)

    mesh = grow_property_topomesh(mesh, vtx_dsplcmnt_field, growth_factor= growth_factor)

end_time = time()

print(nber_growth_steps, ' simulation steps, performed in: ', end_time - start_time, " sec.")




# -- Testing the GrowthModel
# --*-- Setting the growth parameters
growth_rate = .5
tissue_cntr = np.mean(circle_mesh.wisp_property('barycenter', 0).values(), axis=0)

vtx_dsplcmnt_field = {vid: (pos - tissue_cntr)  #/ lng.norm(pos - tissue_cntr)
                           for vid, pos in circle_mesh.wisp_property('barycenter', 0).items()}

draw_tissue(circle_mesh)
# --*-- Instancing the model
growth_model = GrowthModel()

growth_model.set_mesh(circle_mesh)
growth_model.set_parameters(vtx_dsplcmnt_field, growth_rate)

growth_model.run(t_max=10)

circle_mesh_final = growth_model._mesh

draw_tissue(circle_mesh_final)



#
## test_mechanics.py ends here.
