
Welcome to the Tissue2D package documentation
=============================================

Description
-----------

This package provide python 3 methods and classes to simulate growth,
cell division and cell stiffening in 2D Plant tissues.

Contents:

.. toctree::
   :maxdepth: 1

   autodoc
   citing
   contributing
