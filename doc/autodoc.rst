
Example of 'autodoc' usage
==========================

see `autodoc extension page`_ for more documentation.

Doc for all members of example module

.. automodule:: tissue2D.models
   :members:

.. _`autodoc extension page`: http://www.sphinx-doc.org/en/stable/ext/autodoc.html
