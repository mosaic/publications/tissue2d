tissue2D.models.stiffening module
=================================

.. automodule:: tissue2D.models.stiffening
    :members:
    :undoc-members:
    :show-inheritance:
