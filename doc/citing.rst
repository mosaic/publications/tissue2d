.. toctree::
   :maxdepth: 2

.. include:: substitutions.txt


.. _ref_citing:

******
Citing
******

To cite |sname| please use the following publication:
